# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-31 11:03+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: actions/showmain.php:18
msgid ""
"UpdraftCentral is a JavaScript application. You will need to have JavaScript "
"enabled in order to use it."
msgstr ""

#: actions/showmain.php:26
msgid "Loading..."
msgstr ""

#: classes/user.php:62 classes/user.php:71 classes/user.php:283
#: classes/user.php:332 classes/user.php:400
msgid "Missing information"
msgstr ""

#: classes/user.php:83
#, php-format
msgid "This site (%d / %d) was not found"
msgstr ""

#: classes/user.php:92
#, php-format
msgid "The key for this site (%d / %d) was not found"
msgstr ""

#: classes/user.php:99 dashboard-translations.php:96
msgid ""
"You have more sites in your dashboard than licences. As a result, you cannot "
"perform actions on this site."
msgstr ""

#: classes/user.php:99 dashboard-translations.php:96
msgid "You will need to obtain more licences, or remove some sites."
msgstr ""

#: classes/user.php:107
msgid ""
"You cannot contact a website hosted on a site-local network (e.g. localhost) "
"from this dashboard - it cannot be reached."
msgstr ""

#: classes/user.php:124
#, php-format
msgid ""
"To use HTTP digest authentication, your server running the UpdraftCentral "
"dashboard needs at least PHP %s (your version is %s)"
msgstr ""

#: classes/user.php:132
#, php-format
msgid ""
"The loaded UDRPC library (%s) is too old - you probably need to update your "
"installed UpdraftPlus on the server"
msgstr ""

#: classes/user.php:171
msgid "The connection to the remote site returned an error"
msgstr ""

#: classes/user.php:176 dashboard-translations.php:32
#: dashboard-translations.php:34 dashboard-translations.php:35
#: dashboard-translations.php:36
msgid "There was an error in contacting the remote site."
msgstr ""

#: classes/user.php:176 dashboard-translations.php:32
msgid ""
"You should check that the remote site is online, is not firewalled, has "
"remote control enabled, and that no security module is blocking the access. "
"Then, check the logs on the remote site and your browser's JavaScript "
"console."
msgstr ""

#: classes/user.php:176 dashboard-translations.php:32
#: dashboard-translations.php:33
msgid ""
"If none of that helps, then you should try re-adding the site with a fresh "
"key."
msgstr ""

#: classes/user.php:181
msgid "The site was connected to, and returned a response"
msgstr ""

#: classes/user.php:259
msgid "The site list has been refreshed."
msgstr ""

#: classes/user.php:277
msgid "The site was successfully deleted from your dashboard."
msgstr ""

#: classes/user.php:324 classes/user.php:391
msgid "The site configuration was successfully edited."
msgstr ""

#: classes/user.php:410
msgid "Please enter the site key."
msgstr ""

#: classes/user.php:429 classes/user.php:440 classes/user.php:448
#: classes/user.php:453 classes/user.php:471 classes/user.php:477
#: classes/user.php:550 site-management.php:186 site-management.php:194
#: site-management.php:212 site-management.php:217 site-management.php:597
msgid "Error:"
msgstr ""

#: classes/user.php:432
msgid "The entered key was the wrong length - please try again."
msgstr ""

#: classes/user.php:434 classes/user.php:436 classes/user.php:440
msgid "The entered key was corrupt - please try again."
msgstr ""

#: classes/user.php:448
msgid ""
"The entered key does not belong to a remote site (it belongs to this one)."
msgstr ""

#: classes/user.php:453
msgid ""
"The entered key belongs to a local development website - these cannot be "
"controlled from this dashboard because it is not reachable from an external "
"network."
msgstr ""

#: classes/user.php:471
msgid ""
"This key could not be added, as it appears to be corrupt - please try again."
msgstr ""

#: classes/user.php:477
msgid ""
"This key could not be added - it may be too long since you generated it; "
"please try again."
msgstr ""

#: classes/user.php:536
msgid "The key was successfully added."
msgstr ""

#: classes/user.php:536
msgid "It is for interacting with the following site: "
msgstr ""

#: classes/user.php:565
#, php-format
msgid ""
"You have more sites being managed (%d) than active licences (%d) - you will "
"need to obtain more licences in order to manage all of your managed sites."
msgstr ""

#: classes/user.php:674
msgid "You do not have the permission to do this."
msgstr ""

#: classes/user.php:677
msgid ""
"You have no licences available - to add a site, you will need to obtain some "
"more."
msgstr ""

#: dashboard-translations.php:16
msgid "Error"
msgstr ""

#: dashboard-translations.php:17
msgid "Communications error"
msgstr ""

#: dashboard-translations.php:18 templates/sites/management-actions.php:6
msgid "Add Site"
msgstr ""

#: dashboard-translations.php:19
msgid "Advanced options"
msgstr ""

#: dashboard-translations.php:20
msgid "The response was not understood."
msgstr ""

#: dashboard-translations.php:20 dashboard-translations.php:21
msgid "You can check the browser JavaScript console for more information."
msgstr ""

#: dashboard-translations.php:21
msgid "The response was not understood (no message included)."
msgstr ""

#: dashboard-translations.php:22 templates/sites/site-menu.php:73
msgid "Remove site"
msgstr ""

#: dashboard-translations.php:23
msgid "Do you really wish to delete this site from your dashboard?"
msgstr ""

#: dashboard-translations.php:24
#, php-format
msgid ""
"This site is running WordPress version %s (PHP %s, MySQL %s) and UpdraftPlus "
"version %s (UDRPC version %s)"
msgstr ""

#: dashboard-translations.php:25
msgid "UpdraftCentral version"
msgstr ""

#: dashboard-translations.php:26
msgid ""
"An error occurred - you may find more information in your web browser's "
"JavaScript console."
msgstr ""

#: dashboard-translations.php:27
msgid "Follow this link to test alternative connection methods"
msgstr ""

#: dashboard-translations.php:28
msgid "Test connection methods"
msgstr ""

#: dashboard-translations.php:29
#: modules/updraftplus/translations-dashboard.php:78
msgid "Testing..."
msgstr ""

#: dashboard-translations.php:30
msgid "Failed"
msgstr ""

#: dashboard-translations.php:31
msgid "Succeeded."
msgstr ""

#: dashboard-translations.php:33
msgid ""
"There was an error in contacting either the mothership or the remote site."
msgstr ""

#: dashboard-translations.php:33
msgid ""
"You should check that the mothership is online, and that the remote site is "
"online, is not firewalled, has remote control enabled, and that no security "
"module is blocking the access. Then, check the logs on the remote site and "
"your browser's JavaScript console."
msgstr ""

#: dashboard-translations.php:34
msgid "The request did not complete in a reasonable amount of time."
msgstr ""

#: dashboard-translations.php:34
msgid ""
"You should check that the remote site is online, is not firewalled, has "
"remote control enabled, and that no security module is blocking the access. "
"Then, check the logs on the remote site."
msgstr ""

#: dashboard-translations.php:35
msgid ""
"The request to the remote site was blocked by the site's webserver as "
"unauthorized."
msgstr ""

#: dashboard-translations.php:36
msgid ""
"The request to the remote site was blocked by your web browser as "
"unauthorized."
msgstr ""

#: dashboard-translations.php:37
msgid "HTTP response status code"
msgstr ""

#: dashboard-translations.php:38
msgid "Error code"
msgstr ""

#: dashboard-translations.php:39
msgid "Site"
msgstr ""

#: dashboard-translations.php:40
msgid "Open site configuration"
msgstr ""

#: dashboard-translations.php:41
msgid "Description"
msgstr ""

#: dashboard-translations.php:42
msgid "Go"
msgstr ""

#: dashboard-translations.php:43 templates/dashboard/modal.php:13
msgid "Close"
msgstr ""

#: dashboard-translations.php:44
msgid "Edit"
msgstr ""

#: dashboard-translations.php:45
msgid "Edit site configuration"
msgstr ""

#: dashboard-translations.php:46
msgid "PHP installation information"
msgstr ""

#: dashboard-translations.php:47
msgid "To connect to a remote site, paste its key here:"
msgstr ""

#: dashboard-translations.php:48
msgid ""
"The remote site did not recognise the communications key - you are "
"recommended to delete and re-create it."
msgstr ""

#: dashboard-translations.php:49
msgid "The remote site has already received the communications key"
msgstr ""

#: dashboard-translations.php:50
msgid "Follow this link for help, including a video and screenshots"
msgstr ""

#: dashboard-translations.php:51
msgid "Go here for help in trouble-shooting connection issues"
msgstr ""

#: dashboard-translations.php:52
msgid "HTTP authentication (for sites protected by a password)"
msgstr ""

#: dashboard-translations.php:53
msgid "Authentication method"
msgstr ""

#: dashboard-translations.php:54
msgid "HTTP username"
msgstr ""

#: dashboard-translations.php:55
msgid "HTTP password"
msgstr ""

#: dashboard-translations.php:56
msgid "Basic"
msgstr ""

#: dashboard-translations.php:57
msgid "Digest"
msgstr ""

#: dashboard-translations.php:58
msgid "Send CORS headers"
msgstr ""

#: dashboard-translations.php:59
msgid ""
"CORS headers should normally be turned on, so that UpdraftCentral on the "
"remote site sends these necessary headers. You would only turn it off if you "
"have set something else up to manually add them (which may be necessary for "
"you to do if you are using HTTP authentication)."
msgstr ""

#: dashboard-translations.php:60
msgid ""
"The remote site is protected by HTTP digest authentication, which "
"UpdraftCentral does not support in this connection mode (via mothership)."
msgstr ""

#: dashboard-translations.php:61
msgid "Connection method"
msgstr ""

#: dashboard-translations.php:62
msgid ""
"These tests cannot currently be run. You must first change and save the site "
"configuration to take it out of the \"Via mothership, encryption performed "
"at the mothership\" mode, in order to be able to perform the connection "
"tests. This is because in the current mode, some of the information for "
"testing direct connections has not been shared with the browser, to enhance "
"security."
msgstr ""

#: dashboard-translations.php:63
msgid "Run a connection method test by following this link"
msgstr ""

#: dashboard-translations.php:64
msgid "Get help on complicated setups and connection methods at this link."
msgstr ""

#: dashboard-translations.php:65
#, php-format
msgid ""
"Because of %s, any connections to http sites must go via the mothership - "
"and the option chosen here will make no difference"
msgstr ""

#: dashboard-translations.php:65
msgid "web browser security restrictions"
msgstr ""

#: dashboard-translations.php:66
msgid "Direct"
msgstr ""

#: dashboard-translations.php:67
msgid "Direct when possible (authentication method chosen automatically)"
msgstr ""

#: dashboard-translations.php:68
msgid "Direct when possible (authentication via jQuery)"
msgstr ""

#: dashboard-translations.php:69
msgid "Direct when possible (manually constructed authentication)"
msgstr ""

#: dashboard-translations.php:70
msgid "Not yet begun"
msgstr ""

#: dashboard-translations.php:71
msgid "Via mothership (encrypted from the browser)"
msgstr ""

#: dashboard-translations.php:72
msgid "Via mothership (encrypted at the mothership)"
msgstr ""

#: dashboard-translations.php:73
msgid ""
"Via mothership (slower, but better compatibility with some awkward setups)"
msgstr ""

#: dashboard-translations.php:74
msgid ""
"Via mothership, encryption performed at the mothership (useful for slow "
"browsers, but less secure)"
msgstr ""

#: dashboard-translations.php:75
msgid ""
"Browser security restrictions preventing https dashboards contacting non-"
"https sites may be involved here. You could try setting up https (i.e. SSL) "
"for your site-local website, visiting its dashboard over https, and getting "
"a new key for it."
msgstr ""

#: dashboard-translations.php:76
msgid "Method (in recommended order)"
msgstr ""

#: dashboard-translations.php:77
msgid "Result"
msgstr ""

#: dashboard-translations.php:78
msgid "Not possible (due to browser security restrictions on non-https sites)"
msgstr ""

#: dashboard-translations.php:79
msgid "This is the best connection method."
msgstr ""

#: dashboard-translations.php:80
msgid "You are recommended to continue using it."
msgstr ""

#: dashboard-translations.php:81
msgid "You are recommended to use it."
msgstr ""

#: dashboard-translations.php:82
msgid "This is the current method."
msgstr ""

#: dashboard-translations.php:83
msgid "Switch to using this method"
msgstr ""

#: dashboard-translations.php:84 site-management.php:579
#: templates/updraftplus/management-actions-oldcopy-keep.php:16
msgid "Settings"
msgstr ""

#: dashboard-translations.php:85
msgid "Save settings"
msgstr ""

#: dashboard-translations.php:86
msgid "Debugging level"
msgstr ""

#: dashboard-translations.php:87
msgid ""
"for this session only - controls the amount of output in the web browser's "
"console"
msgstr ""

#: dashboard-translations.php:88
msgid "UpdraftCentral Help"
msgstr ""

#: dashboard-translations.php:89
msgid "Follow one of these links to get help with UpdraftCentral"
msgstr ""

#: dashboard-translations.php:90
msgid "How to install UpdraftCentral"
msgstr ""

#: dashboard-translations.php:91
msgid "How to add a site to UpdraftCentral"
msgstr ""

#: dashboard-translations.php:92
msgid "Frequently asked questions"
msgstr ""

#: dashboard-translations.php:93
msgid "Make a suggestion / feature request"
msgstr ""

#: dashboard-translations.php:94
msgid "Paid support requests"
msgstr ""

#: dashboard-translations.php:95 templates/wp-admin/dashboard-page.php:6
msgid "Support forum"
msgstr ""

#: dashboard-translations.php:99
msgid ""
"The site could not return the requested information - perhaps you need to "
"update it to a more recent UpdraftPlus version?"
msgstr ""

#: dashboard-translations.php:100
msgid "The remote site did not recognise the command."
msgstr ""

#: dashboard-translations.php:100
#, php-format
msgid ""
"Possibly you need to update the remote site to a more recent %s version."
msgstr ""

#: dashboard-translations.php:102
msgid ""
"Your web browser lacks capabilities that UpdraftCentral requires, and thus "
"is not supported. Please update your browser to the latest version."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:22
msgid ""
"You have chosen to backup files, but no file entities have been selected"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:23
msgid ""
"If you exclude both the database and the files, then you have excluded "
"everything!"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:24
msgid "The backup appears not to have started."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:24
msgid "You may find more information in your web browser's JavaScript console."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:25
msgid "The backup has begun"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:26
msgid "Last log message"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:27
#: templates/updraftplus/site-row-buttons.php:3
#: templates/sites/site-menu.php:21 templates/sites/site-menu.php:40
msgid "Backup now"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:28
msgid "Log file"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:29
msgid "Download log"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:30
msgid "Delete"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:31
msgid "none"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:32
msgid "Deleted"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:33
msgid "This item has been deleted."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:34
msgid ""
"This item needs downloading from the remote storage (which will be "
"attempted)."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:35
msgid "Delete backup set"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:36
msgid "Could not find that job - perhaps it has already finished?"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:37
msgid "Job deleted"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:38
msgid "Existing Backups"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:39
msgid ""
"Rescanning (looking for backups that you have uploaded manually into the "
"internal backup store)..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:40
msgid "Rescanning remote and local storage for backup sets..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:41
msgid "Raw backup history"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:42
#: modules/updraftplus/translations-dashboard.php:43
#, php-format
msgid "Are you sure that you wish to remove %s from UpdraftPlus?"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:42
msgid "this backup set"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:43
msgid "these backup sets"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:44
msgid "Also delete from remote storage"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:45
msgid ""
"Deleting... please allow time for any communications with the remote storage "
"to complete."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:46
#: templates/updraftplus/management-actions-oldcopy-keep.php:13
msgid "Download"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:47
msgid "Begun looking for this entity"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:48
msgid "The download failed to start"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:49
msgid "File ready."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:50
msgid "You should:"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:51
msgid "Delete from your web server"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:52
msgid "Download to your computer"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:53
msgid "and then, if you wish,"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:54
msgid "The download request did not succeed."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:55
msgid "Restore backup"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:56
msgid ""
"You will now be taken to the site's dashboard, to allow you to choose "
"restoration options. Note that depending upon what you restore, and the "
"state of the site when the backup was taken, restoring may cause your "
"UpdraftCentral connection to be broken. In that case, you will need to re-"
"create it after restoring."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:59
msgid "day"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:60
msgid "in the month"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:61
msgid "day(s)"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:62
msgid "hour(s)"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:63
msgid "week(s)"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:64
msgid "For backups older than"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:65
msgid "Enter the directory:"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:66
msgid "Remove"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:67
msgid "Send a report only when there are warnings/errors"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:68
msgid "When the Email storage method is enabled, also send the entire backup"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:69
#, php-format
msgid ""
"Be aware that mail servers tend to have size limits; typically around %s Mb; "
"backups larger than any limits will likely not arrive."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:70
msgid "Host"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:71
msgid "Username"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:72
msgid "Password"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:73
msgid "Database"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:74
msgid "Table prefix"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:75
msgid ""
"If you enter a table prefix, then only tables that begin with this prefix "
"will be backed up."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:76
msgid "Test connection..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:77
msgid "Backup external database"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:79
#, php-format
msgid "Test %s Settings"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:80
#, php-format
msgid "Testing %s Settings..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:81
#, php-format
msgid "%s settings test result:"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:82
msgid "Counting..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:83
msgid "Update quota count"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:84
msgid "Disconnecting..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:85
msgid "Connect"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:86
msgid "Connecting..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:87
msgid "Disconnect"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:88
msgid "Create new IAM user and S3 bucket"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:89
msgid "Create"
msgstr ""

#: modules/updraftplus/translations-dashboard.php:90
msgid "Trying..."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:91
msgid "You are now using a IAM user account to access your bucket."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:91
msgid "Do remember to save your settings."
msgstr ""

#: modules/updraftplus/translations-dashboard.php:92
msgid "Settings saved"
msgstr ""

#: modules/updraftplus/loader.php:33 templates/sites/site-menu.php:12
msgid "Backups"
msgstr ""

#: pages/dashboard.php:55
msgid "Sites"
msgstr ""

#: pages/dashboard.php:56
msgid "The Future"
msgstr ""

#: site-management.php:99
msgid "Higher PHP version required"
msgstr ""

#: site-management.php:99 site-management.php:104 site-management.php:236
#: site-management.php:240
#, php-format
msgid ""
"The %s plugin requires %s version %s or higher - your current version is "
"only %s."
msgstr ""

#: site-management.php:104
msgid "Higher WordPress version required"
msgstr ""

#: site-management.php:121
msgid "Home"
msgstr ""

#: site-management.php:186
msgid "failed to initialise"
msgstr ""

#: site-management.php:194
msgid "you are not authorized to access this site"
msgstr ""

#: site-management.php:212
#, php-format
msgid "This action (%s) could not be handled"
msgstr ""

#: site-management.php:582
msgid "UpdraftCentral website"
msgstr ""

#: site-management.php:597
msgid "template not found"
msgstr ""

#: templates/wp-admin/dashboard-page.php:1
msgid "Remote Control for WordPress"
msgstr ""

#: templates/wp-admin/dashboard-page.php:4
msgid "News"
msgstr ""

#: templates/wp-admin/dashboard-page.php:5
msgid "Twitter"
msgstr ""

#: templates/wp-admin/dashboard-page.php:7
msgid "Suggest idea"
msgstr ""

#: templates/wp-admin/dashboard-page.php:8
msgid "Newsletter sign-up"
msgstr ""

#: templates/wp-admin/dashboard-page.php:9
msgid "Lead developer's homepage"
msgstr ""

#: templates/wp-admin/dashboard-page.php:10
msgid "More plugins"
msgstr ""

#: templates/wp-admin/dashboard-page.php:10
msgid "Version"
msgstr ""

#: templates/wp-admin/dashboard-page.php:15
msgid "Getting started"
msgstr ""

#: templates/wp-admin/dashboard-page.php:17
msgid ""
"Welcome to UpdraftCentral! This is the dashboard plugin, which you install "
"on the site where you want to have your dashboard for controlling other "
"sites. (On the controlled sites, you install UpdraftPlus)."
msgstr ""

#: templates/wp-admin/dashboard-page.php:19
#, php-format
msgid ""
"UpdraftCentral runs on the front-end of your site. To get started, you must "
"create a front-end page for your site, to contain the dashboard. i.e. Go to "
"%s."
msgstr ""

#: templates/wp-admin/dashboard-page.php:19
msgid "Pages -> Add New"
msgstr ""

#: templates/wp-admin/dashboard-page.php:21
msgid ""
"In your new front-end page, put this shortcode: [updraft_central] . This "
"will allow logged-in site administrators, who visit that page, to use "
"UpdraftCentral."
msgstr ""

#: templates/wp-admin/dashboard-page.php:23
msgid ""
"If you want users with roles to also be able to use UpdraftCentral (note "
"that every user has their own list of sites - giving users access to "
"UpdraftCentral does not give them access to your sites, only to their own "
"list of sites), then you can do this with the parameter \"require_role\". e."
"g. [updraft_central require_role=\"administrator,editor,subscriber\"]"
msgstr ""

#: templates/wp-admin/dashboard-page.php:25
msgid ""
"Then, to start using UpdraftCentral, simply visit the page, and you can "
"begin adding sites."
msgstr ""

#: templates/updraftplus/management-actions.php:5 templates/future.php:6
msgid "Go back to sites management"
msgstr ""

#: templates/updraftplus/management-actions.php:10
msgid "Choose another site to manage backups on"
msgstr ""

#: templates/updraftplus/management-actions-oldcopy-keep.php:7
msgid "Backup"
msgstr ""

#: templates/updraftplus/management-actions-oldcopy-keep.php:10
msgid "Restore"
msgstr ""

#: templates/updraftplus/site-row-buttons.php:8
#: templates/sites/site-menu.php:29
msgid "Existing backups"
msgstr ""

#: templates/updraftplus/site-row-buttons.php:13
msgid "Backup settings"
msgstr ""

#: templates/dashboard/not-authorised.php:5
msgid ""
"You are logged in, but your user account is not authorised to see this page."
msgstr ""

#: templates/dashboard/not-authorised.php:21
msgid "Go here to log out and log in again."
msgstr ""

#: templates/dashboard/navigation.php:28
msgid "Follow the link to purchase more licences."
msgstr ""

#: templates/dashboard/not-logged-in.php:5
msgid "You need to be logged in to see this page."
msgstr ""

#: templates/dashboard/not-logged-in.php:22
msgid "Go here to log in."
msgstr ""

#: templates/future.php:14
msgid "UpdraftCentral - The future"
msgstr ""

#: templates/future.php:16
msgid ""
"Thank you for using this early release of UpdraftCentral! This is just a "
"beginning... we have lots of future plans."
msgstr ""

#: templates/future.php:18
msgid ""
"The main feature that we are working on next is updates management - i.e. "
"managing updates of WordPress, plugins and themes across all your managed "
"sites. We plan to include this feature in all versions of UpdraftCentral."
msgstr ""

#: templates/future.php:20
msgid "Feature ideas and other suggestions"
msgstr ""

#: templates/future.php:22
msgid ""
"If you want to have a say in the future of UpdraftCentral, then please <a "
"href=\"https://updraftplus.com/make-a-suggestion\">do post in our feature "
"requests / suggestions forum, here.</a> You will need a login to post - <a "
"href=\"https://updraftplus.com/my-account/\">you can sign up for one here.</"
"a> We may not reply directly to a feature suggestion or every comment, but "
"you can be assured that we are reading them all."
msgstr ""

#: templates/future.php:24
msgid "Other feedback"
msgstr ""

#: templates/future.php:28
msgid ""
"If you already want to give UpdraftCentral a 5-star rating, then... thank "
"you!"
msgstr ""

#: templates/future.php:28 templates/future.php:30
msgid "You can do so at this link."
msgstr ""

#: templates/future.php:30
msgid ""
"If you think UpdraftCentral has problems and isn't yet worth five stars, "
"then please post in the support forum instead."
msgstr ""

#: templates/future.php:30
msgid ""
"Please don't post negative reviews (less than 5 stars) yet - it makes it "
"hard for a plugin to get off the ground at all if it begins with bad "
"reviews, which deter any other users from trying it out (whereas, 5-star "
"reviews bring in new users, and help the plugin to get better)."
msgstr ""

#: templates/sites/site-menu.php:51 templates/sites/site-menu.php:62
msgid "Site configuration"
msgstr ""

#: templates/sites/site-row.php:15 templates/sites/site-row.php:29
msgid "A licence is required to manage this site"
msgstr ""

#: templates/sites/site-row.php:39
msgid "Site information"
msgstr ""

#: templates/sites/site-row.php:45
msgid "WP Dashboard"
msgstr ""

#: templates/sites/none-set-up.php:3
msgid "No sites have yet been set up."
msgstr ""

#: templates/sites/none-set-up.php:3
msgid "Go here to learn how to add your first site."
msgstr ""
