<button class="btn btn-success-outline btn-sm updraftcentral_site_backup_now updraftcentral-hide-in-other-tabs updraftcentral-show-in-tab-backups" title="<?php esc_attr_e('Backup now', 'updraftcentral');?>">
	<span class="dashicons dashicons-cloud">	</span>
	<?php _e('Backup now', 'updraftcentral'); ?>
</button>

<button class="btn btn-updraftplus-outline btn-sm updraftcentral_site_backups_manage updraftcentral-hide-in-other-tabs updraftcentral-show-in-tab-backups" title="<?php esc_attr_e('Existing backups', 'updraftcentral');?>">
	<span class="dashicons dashicons-upload">	</span>
	<?php _e('Existing backups', 'updraftcentral'); ?>
</button>

<button class="btn btn-settings-outline btn-sm updraftcentral_site_backup_settings updraftcentral-hide-in-other-tabs updraftcentral-show-in-tab-backups" title="<?php esc_attr_e('Backup settings', 'updraftcentral');?>">
	<span class="dashicons dashicons-admin-generic">	</span>
	<?php _e('Backup settings', 'updraftcentral'); ?>
</button>
