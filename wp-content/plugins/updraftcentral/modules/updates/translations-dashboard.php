<?php

if (!defined('UD_CENTRAL_DIR')) die('Security check');

// Dashboard translations for the Updates module
return array(
	'type' => __('Type', 'updraftcentral'),
	'name' => __('Name', 'updraftcentral'),
	'version' => __('Version', 'updraftcentral'),
	'action' => __('Action', 'updraftcentral'),
	'actions' => __('Actions', 'updraftcentral'),
	'connection_information' => __('Connection information', 'updraftcentral'),
	'updating' => __('Updating...', 'updraftcentral'),
	'update' => __('Update', 'updraftcentral'),
	'show_messages' => __('Show messages...', 'updraftcentral'),
	'update_to' => __('Update to', 'updraftcentral'),
	'update_info' => __('Update information', 'updraftcentral'),
	'by' => __('by', 'updraftcentral'),
	'new_version_available' => __('New version available', 'updraftcentral'),
	'plugin' => __('Plugin', 'updraftcentral'),
	'theme' => __('Theme', 'updraftcentral'),
	'wordpress_core' => __('WordPress core', 'updraftcentral'),
	'wordpress' => __('WordPress', 'updraftcentral'),
	'current' => __('current', 'updraftcentral'),
	'changelog' => __('Changelog', 'updraftcentral'),
	'description' => __('Description', 'updraftcentral'),
	'update_info' => __('Update information', 'updraftcentral'),
	// This is in this module, because UC calls a backup if UD is configured for automatic backups
	'automatic_backup' => __('Automatic backup before update', 'updraftcentral'),
	'cannot_update' => __('Cannot update', 'updraftcentral'),
	'download' => __('download', 'updraftcentral'),
	'save_creds_in_browser' => __('Save these credentials locally in your web browser', 'updraftcentral'),
	'no_updates' => __('Your WordPress site is fully up to date.', 'updraftcentral'),
	'php_version_required' => sprintf(__('%s version required', 'updraftcentral'), 'PHP'),
	'mysql_version_required' => sprintf(__('%s version required', 'updraftcentral'), 'MySQL'),
	'insufficient_php' => 'You cannot update because <a href="https://codex.wordpress.org/Version_%1$s">WordPress %1$s</a> requires PHP version %2$s or higher. You are running version %3$s.',
	'insufficient_mysql' => 'You cannot update because <a href="https://codex.wordpress.org/Version_%1$s">WordPress %1$s</a> requires MySQL version %2$s or higher. You are running version %3$s.',
	'insufficient_php_and_mysql' => 'You cannot update because <a href="https://codex.wordpress.org/Version_%1$s">WordPress %1$s</a> requires PHP version %2$s or higher and MySQL version %3$s or higher. You are running PHP version %4$s and MySQL version %5$s.',
	'update_error_messages' => array(
		'update_failed' => __('Update failed', 'updraftcentral'),
		'not_found' => __('The item to update was not found (has it been removed?)', 'updraftcentral'),
		'updates_permission_denied' => __('You do not have permission to update this item', 'updraftcentral'),
		'unable_to_connect_to_filesystem' => __('WordPress was unable to connect to the filesystem to perform this update. This indicates that the supplied filesystem credentials were incorrect.', 'updraftcentral'),
	),
);
