jQuery(document).ready(function($) {
	
	var settings_css_sub_prefix = '.updraftcentral_row_extracontents';
	var settings_css_prefix = '#updraftcentral_dashboard_existingsites '+settings_css_sub_prefix;
// 	var wp_org_plugin_json_api = 'https://api.wordpress.org/plugins/info/1.0/'; // Suffixed by <slug>.json . Doesn't work: no CORS header set on the response.
	var wp_org_plugin_json_api = 'https://api.wordpress.org/plugins/info/1.1/'; // this is POSTed to
	var wp_org_theme_json_api = 'https://api.wordpress.org/themes/info/1.1/'; // this is POSTed to
	
	// In future, if we have multiple sites being updated at once, the queue will probably need to be an array with one entry per site
	var queue = new UpdraftCentral_Queue();
	
	var updates_meta = [];
	
	var site_credentials = [];
	
	var credentials_modal_opened = false;
	var modal_is_for_item;
	
	/**
	 * Highlights the first tab
	 * @returns {void}
	 */
	var post_modal_open = function() {
		$('#updraftcentral_modal ul#updates-sections-list li:first').addClass('selected');
	}
	
	/**
	 * @callable metadata_callback
	 * @param {Object} - the retrieved metadata
	 */
	
	/**
	 * Fetches metadata from the wordpress.org API (https://codex.wordpress.org/WordPress.org_API), or via our local cache
	 * 
	 * @param {string} type - either 'plugin' or 'theme' (otherwise, results are undefined)
	 * @param {string} slug - the plugin or theme slug (i.e. not the file path)
	 * @param {*} passback - this gets passed back to the callback function
	 * @param {metadata_callback} callback - in the event of a successful retrieval, this function is called with the results
	 * @returns {void}
	 */
	function get_wporg_metadata(type, slug, passback, callback) {
		
		// Cache for 10 minutes
		var from_storage = UpdraftCentral.storage_get('wporg_api_'+type+'_'+slug, 600);
		
		if (from_storage && from_storage.hasOwnProperty('name')) {
			callback.call(this, from_storage, passback);
			return;
		}
		
		var api_url = wp_org_plugin_json_api;
		if ('theme' == type) {
			api_url = wp_org_theme_json_api;
		}
		
		$.getJSON(api_url, {
			action: type+'_information',
			request: {
				slug: slug
			}
		}, function(data, status) {
			if ('success' == status) {
				UpdraftCentral.storage_set('wporg_api_'+type+'_'+slug, data, true);
				callback.call(this, data, passback);
			}
		});
		
	}
	
	/**
	 * For any plugins or themes in the passed array of objects, if they are from wordpress.org, use the wordpress.org API to fill in information about them.
	 * 
	 * @param {Object} $site_row - the jQuery object for the site
	 * @param {array} objects - an array of plugin or theme update objects
	 * @param {string} type - either 'plugin' or 'theme', according to which type of objects has been passed
	 * @returns {void}
	 */
	function fill_wporg_metadata($site_row, objects, type) {
		
		for (var i=0, j=objects.length; i<j; i++) {
			var object = objects[i];
			if (object && object.hasOwnProperty('update') && object.update.hasOwnProperty('package') && object.update.package) {
				var url = object.update.package;
				if ('string' != typeof url) { continue; }
				var is_wp_org = url.match(/^https?:\/\/((downloads|www)\.)?wordpress\.org\//);
				if (is_wp_org) {
					
					if ('plugin' == type) {
						get_wporg_metadata(type, object.update.slug, object, function(data, object) {
							if (data && data.hasOwnProperty('sections')) {
								
								var $the_update = $site_row.find('.updraftcentral_row_extracontents .updates-plugin-update.updates-update[data-plugin-file="'+object.update.plugin+'"]');
								
								var existing_data = $the_update.data('plugin-info');
								
								if (!existing_data) { return; }
								
								if (!existing_data.update.hasOwnProperty('sections') || !existing_data.update.sections) existing_data.update.sections = {};
								
								if (data.sections.hasOwnProperty('changelog')) {
									existing_data.update.sections.changelog = data.sections.changelog;
								}
								if (data.sections.hasOwnProperty('description')) {
									existing_data.update.sections.description = data.sections.description;
								}
								
								$the_update.data('plugin-info', existing_data);
							}
						});
					} else if ('theme' == type) {
						get_wporg_metadata(type, object.update.theme, object, function(data, object) {
							if (data && data.hasOwnProperty('sections')) {
								
								var $the_update = $site_row.find('.updraftcentral_row_extracontents .updates-theme-update.updates-update[data-theme="'+object.update.theme+'"]');
								
								var existing_data = $the_update.data('theme-info');
								
								if (!existing_data) { return; }
								
										   
								if (data.sections.hasOwnProperty('changelog')) {
									if (!existing_data.update.hasOwnProperty('sections') || !existing_data.update.sections) existing_data.update.sections = {};
									existing_data.update.sections.changelog = data.sections.changelog;
								}
								
								// Redundant - this is always the same as what the remote site parsed from the style.css
								//if (data.sections.hasOwnProperty('description')) {
									//existing_data.update.sections.description = data.sections.description;
								//}
								
								$the_update.data('theme-info', existing_data);
							}
						});
					}
					
				}
			}
		}
		
	}
	
	$('#updraftcentral_dashboard_existingsites').on('updraftcentral_listener_finished_updraftplus_backup', function(event, data) {
		if (!queue.is_empty()) {
			var site_id = data.site_id;
			
			// Was something queueing for *this* site?
			var item = queue.peek();
			
			if (queue.is_locked() && item && item.hasOwnProperty('autobackup_requested') && item.autobackup_requested && !item.autobackup_complete) {
			
				var $queuefirst_site_row = item.site_row;
				var queuefirst_site_id = $queuefirst_site_row.data('site_id');
				
				if (queuefirst_site_id == site_id) {
					console.log("UpdraftCentral: automatic backup is complete; calling updates queue");
					item.autobackup_complete = true;
					queue.replace_front(item);
					queue.unlock();
					process_queue();
				}
				
			}
		}
		
	});
	
	$('#updraftcentral_modal_dialog').on('hide.bs.modal', function() {
		if (credentials_modal_opened) {
			credentials_modal_opened = false;
			console.log("UpdraftCentral/updates: modal closed: unlocking queue and removing item (cancelled)");
			queue.dequeue();
			queue.unlock();
			if (modal_is_for_item && modal_is_for_item.hasOwnProperty('ui_rows')) {
				var $ui_rows = modal_is_for_item.ui_rows;
				$ui_rows.find('.update-go').show();
				$ui_rows.find('.update-in-progress').hide();
			}
		}
	});
	
	// Register the row clickers for this tab
	$('#updraftcentral_dashboard_existingsites').on('updraftcentral_dashboard_mode_set_updates', function() {

		UpdraftCentral.register_modal_listener('ul#updates-sections-list a.updates-section-link', function(e) {
			e.preventDefault();
			var section = $(this).data('section');
			$('#updraftcentral_modal #updates-sections-list li.selected').removeClass('selected');
			$(this).closest('li').addClass('selected');
			$('#updraftcentral_modal .updates-section').hide();
			$('#updraftcentral_modal #updates-section-'+section).show();
		});
		
		UpdraftCentral.register_modal_listener('.request-filesystem-credentials-dialog-content #ssh', function () {
			$("#updraftcentral_modal .request-filesystem-credentials-dialog-content #ssh_keys").show();
		});
		
		UpdraftCentral.register_modal_listener('.request-filesystem-credentials-dialog-content #ftp, .request-filesystem-credentials-dialog-content #ftps', function () {
			$("#updraftcentral_modal .request-filesystem-credentials-dialog-content #ssh_keys").hide();
		});
		
		UpdraftCentral.register_row_clicker('.updraftcentral_row_extracontents .updates-plugin-update .update-info', function($site_row) {
			var plugin_info = $(this).closest('.updates-plugin-update');
			plugin_info = plugin_info.data('plugin-info');
			if (!plugin_info) { console.log("Plugin info not found"); return; }
						
			UpdraftCentral.open_modal(udclion.updates.update_info, UpdraftCentral.template_replace('updates-plugin-update-info', plugin_info), true, false, post_modal_open);
			
		});
		
		UpdraftCentral.register_row_clicker('.updraftcentral_row_extracontents .updates-theme-update .update-info', function($site_row) {
			var theme_info = $(this).closest('.updates-theme-update');
			theme_info = theme_info.data('theme-info');
			if (!theme_info) { console.log("Theme info not found"); return; }
			
			UpdraftCentral.open_modal(udclion.updates.update_info, UpdraftCentral.template_replace('updates-theme-update-info', theme_info), true, false, post_modal_open);
			
		});
		
		UpdraftCentral.register_row_clicker('.updraftcentral_row_extracontents .updates-core-update .update-info', function($site_row) {
			var core_info = $(this).closest('.updates-core-update');
			core_info = core_info.data('core-info');
			if (!core_info) { console.log("WP core info not found"); return; }
			UpdraftCentral.open_modal(udclion.updates.update_info, UpdraftCentral.template_replace('updates-core-update-info', core_info), true, false, post_modal_open);
		});
		
		$('#updraftcentral_notice_container').on('click', '.updraftcentral_notice .update-failure-messages-show', function(e) {
			e.preventDefault();
			$(this).hide().siblings('.update-failure-messages-text').slideDown();
		});
		
		UpdraftCentral.register_row_clicker('.updraftcentral_row_extracontents .updates-plugin-update .update-go', function($site_row) {
			
			// Prevent them clicking again on the same button
			var width = $(this).css('width');
			$(this).hide();

			var $plugin_row = $(this).closest('.updates-plugin-update');
			
			$plugin_row.find('.update-in-progress').css('width', width).show();
			
			plugin_info = $plugin_row.data('plugin-info');
			if (!plugin_info || !plugin_info.hasOwnProperty('update')) { console.log("Plugin info not found"); console.log(plugin_info); return; }
			
			var updates = {
				plugins: [
					{
						plugin: plugin_info.update.plugin,
						slug: plugin_info.update.slug
					}
				]
			}
			
			var update_info = {
				site_row: $site_row,
				updates: updates,
				ui_rows: $plugin_row,
				spinner_where: false
			}
			
			queue.enqueue(update_info);
			
			process_queue();
			
		});
		
		UpdraftCentral.register_row_clicker('.updraftcentral_row_extracontents .updates-theme-update .update-go', function($site_row) {
			
			// Prevent them clicking again on the same button
			var width = $(this).css('width');
			$(this).hide();
			
			var $theme_row = $(this).closest('.updates-theme-update');
			
			$theme_row.find('.update-in-progress').css('width', width).show();
			
			theme_info = $theme_row.data('theme-info');
			if (!theme_info || !theme_info.hasOwnProperty('update')) { console.log("Theme info not found"); console.log(theme_info); return; }
			
			var updates = {
				themes: [ { theme: theme_info.update.theme } ]
			}
			
			var update_info = {
				site_row: $site_row,
				updates: updates,
				ui_rows: $theme_row,
				spinner_where: $theme_row.find('button:first')
			}
			
			queue.enqueue(update_info);
			
			process_queue();
			
			
		});
		
		UpdraftCentral.register_row_clicker('.updraftcentral_row_extracontents .updates-core-update .update-go', function($site_row) {
			
			// Prevent them clicking again on the same button
			var width = $(this).css('width');

			var $core_row = $(this).closest('.updates-core-update');
			
			core_info = $core_row.data('core-info');
			if (!core_info || !core_info.hasOwnProperty('version')) { console.log("Core update info not found"); console.log(core_info); return; }
			
			var is_mysql = true;
			if (core_info.hasOwnProperty('installed') && core_info.installed.hasOwnProperty('is_mysql') && !core_info.installed.is_mysql) {
				is_mysql = false;
			}
			
			if (core_info.hasOwnProperty('sufficient')) {
				var mysql_sufficient = (!is_mysql || core_info.sufficient.mysql);
				var php_sufficient = core_info.sufficient.php;
				if (!mysql_sufficient) {
					if (!php_sufficient) {
						UpdraftCentral.dialog.alert('<h2>'+udclion.updates.cannot_update+'</h2>'+sprintf(udclion.updates.insufficient_php_and_mysql, core_info.version, core_info.php_version, core_info.mysql_version, core_info.installed.php, core_info.installed.mysql), null, false);
						return;
					} else {
						UpdraftCentral.dialog.alert('<h2>'+udclion.updates.cannot_update+'</h2>'+sprintf(udclion.updates.insufficient_mysql, core_info.version, core_info.mysql_version, core_info.installed.mysql), null, false);
						return;
					}
				} else if (!php_sufficient) {
					UpdraftCentral.dialog.alert('<h2>'+udclion.updates.cannot_update+'</h2>'+sprintf(udclion.updates.insufficient_php, core_info.version, core_info.php_version, core_info.installed.php), null, false);
					return;
				}
			}
			
			$(this).hide();
			$core_row.find('.update-in-progress').css('width', width).show();
			
			var updates = {
				core: [ { core: core_info.version } ]
			}
			
			var update_info = {
				site_row: $site_row,
				updates: updates,
				ui_rows: $core_row,
				spinner_where: $core_row.find('button:first')
			}
			
			queue.enqueue(update_info);
			
			process_queue();
			
		});
		
		
		UpdraftCentral.register_row_clicker('.updraftcentral_action_show_updates', function($site_row) {
			
			var update_options = {
				force_refresh: false
			}
			
			if ($(this).hasClass('updraftcentral_updates_force_check')) {
				update_options.force_refresh = true;
			}
			
			var site_id = $site_row.data('site_id');
			
			UpdraftCentral.send_site_rpc('updates.get_updates', update_options , $site_row, function(response, code, error_code) {
				if ('ok' == code && response && 'updates' == UpdraftCentral.get_dashboard_mode()) {
					
					var $row_extra_contents = $site_row.find('.updraftcentral_row_extracontents');
					var output = '';
					
					var plugins;
					var themes;
					var core;
					
					if (response.data) {
						
						if (response.data.hasOwnProperty('plugins') && response.data.plugins.length) {
							plugins = UpdraftCentral.template_replace('updates-plugin-updates', { plugins: response.data.plugins});
						}
						
						if (response.data.hasOwnProperty('themes') && response.data.themes.length) {
							themes = UpdraftCentral.template_replace('updates-theme-updates', { themes: response.data.themes});
						}
						
						if (response.data.hasOwnProperty('core') && response.data.core.length) {
							core = UpdraftCentral.template_replace('updates-core-updates', { core: response.data.core});
						}
						
						if (response.data.hasOwnProperty('meta')) {
							updates_meta[site_id] = response.data.meta;
						}
					}
					
					if (plugins || themes || core) {
						output = UpdraftCentral.template_replace('updates-table-header', {
							"plugins": plugins,
							"themes": themes,
							"core": core
						});
						
					} else {
						output = "<h5>" + udclion.updates.no_updates + "</h5>";
					}
					
					$row_extra_contents.html('<div class="updraft_updates_output updraft_module_output">' +
					'<div class="dashicons dashicons-image-rotate updraftcentral_action_show_updates updraftcentral_updates_force_check"></div>' 
					+ UpdraftCentral.sanitize_html(output) + '</div>');
					
					if (response.data.plugins.length) {
						fill_wporg_metadata($site_row, response.data.plugins, 'plugin');
					}
					if (response.data.themes.length) {
						fill_wporg_metadata($site_row, response.data.themes, 'theme');
					}
				}
			});
		}, true);
		
		// Put clicked links within the settings sections into their own tab
		$('#updraftcentral_dashboard_existingsites_container').on('click', '.updraftcentral_site_row '+settings_css_sub_prefix+' .updates-update a', function(e) {
			var href = $(this).attr('href');
			if ('undefined' === typeof href) { return; }
			if (href.substring(0, 4) == 'http') {
				e.preventDefault();
				var win = window.open(href, '_blank');
				win.focus();
			}
		});
		
	});
	
	/**
	 * Report an error when updating something
	 * 
	 * @param {Object} entity - the update object returned from the remote call
	 * @param {Object} $site_row - the jQuery object for the affected site
	 * @param {Object} $update_row - the jQuery object for the relevant update
	 * @param {Object} entity_info - the object with information about the updatable entity
	 * @returns {void}
	 * 
	 */
	function update_error(entity, $site_row, $update_row, entity_info) {
		
		var site_id = $site_row.data('site_id');
		
		console.log("UpdraftCentral: update error (follows)");
		console.log(entity);
		
		$update_row.find('.update-go').show();
		$update_row.find('.update-in-progress').hide();
		
		var site_description = $site_row.data('site_description');
		var site_url = $site_row.data('site_url');
		
		if (site_description == site_url) { site_description = ''; }
		
		var messages = '';
		if (entity.hasOwnProperty('messages')) {
			$(entity.messages).each(function(ind, message) {
				messages += message+"\n";
			});
		}
		
		// If an existing container for this site exists, then we add the information in there, instead of creating a new one.
		
		var $existing_notice = $('#updraftcentral_notice_container > .updraftcentral_notice[data-site_id="'+site_id+'"]:first');
		if ($existing_notice.length < 1) {
			$existing_notice = UpdraftCentral.add_dashboard_notice(
				UpdraftCentral.template_replace('updates-update-error-container', {
					site_url: site_url,
					site_description: site_description
				}),
				'error',
				false,
				{ site_id: site_id }
			);
		}
		
		if ('unable_to_connect_to_filesystem' == entity.error) {
			console.log("UpdraftCentral: unable_to_connect_to_filesystem: filesystem credentials apparently wrong; will clear");
		} else {
			console.log("UpdraftCentral: update failed: will clear filesystem credentials in case they were wrong");
		}
		site_credentials[site_id] = false;
		
		var error_message;
		if (!entity.hasOwnProperty('error_message') || '' == entity.error_message) {
			
			if (udclion.updates.update_error_messages.hasOwnProperty(entity.error)) {
				error_message = udclion.updates.update_error_messages[entity.error];
			} else {
				error_message = udclion.updates.update_error_messages.update_failed;
				if (entity.error != 'update_failed') { error_message += ' ('+entity.error+')'; }
			}
		} else {
			error_message = entity.error_message;
		}
		
		$existing_notice.find('.update-errors').append(
			UpdraftCentral.template_replace('updates-update-error', {
				error_message: error_message,
				error: entity.error,
				messages: messages,
				entity_info: entity_info,
				entity: entity
			})
		);
		
	}
	
	/**
	 * Process the updates queue
	 * @returns {void}
	 */
	function process_queue() {
		
		var debug_level = UpdraftCentral.get_debug_level();
		
		if (!queue.get_lock()) {
			if (debug_level > 0) {
				console.log("UpdraftCentral/updates: process_queue(): queue is currently locked - exiting");
			}
			return;
		}
		if (debug_level > 0) {
			console.log("UpdraftCentral/updates: process_queue(): got queue lock");
		}
		
		var item = queue.peek();
		
		if ('undefined' === typeof item) {
			console.log("UpdraftCentral/updates: process_queue(): queue is apparently empty - exiting");
			queue.unlock();
			return;
		}
		
		var $site_row = item.site_row;
		var updates = item.updates;
		var spinner_where = item.spinner_where;
		var site_id = $site_row.data('site_id');
		var site_url = $site_row.data('site_url');
		// Used for cacheing remote storage credential results
		var site_hash = UpdraftCentral.md5(site_id+'_'+site_url);
		
		var save_credentials_in_browser = (item.hasOwnProperty('save_credentials_in_browser') && item.save_credentials_in_browser) ? true : false;

		var $row_updates_info = $site_row.find('.updraftcentral_row_extracontents .updraft_updates_output:first');
		
		var need_filesystem_credentials = false;
		
		var site_updates_meta = updates_meta[site_id];
		
		if ($row_updates_info.length) {
			
			if (site_updates_meta && site_updates_meta.hasOwnProperty('request_filesystem_credentials')) {
				
				var request_filesystem_credentials = site_updates_meta.request_filesystem_credentials;
				
				if (updates.plugins && updates.plugins.length > 0 && request_filesystem_credentials.plugins) {
					need_filesystem_credentials = true;
				}
				
				if (updates.themes && updates.themes.length > 0 && request_filesystem_credentials.themes) {
					need_filesystem_credentials = true;
				}
				
				if (updates.core && updates.core.length > 0 && request_filesystem_credentials.core) {
					need_filesystem_credentials = true;
				}
			}
			
		}
		
		if (debug_level > 0) {
			console.log("UpdraftCentral/updates: process_queue(): processing update queue item (need_filesystem_credentials="+need_filesystem_credentials+") (follows)");
			console.log(updates);
		}
		
		// Used only for testing
		// need_filesystem_credentials = true;
		
		if (need_filesystem_credentials) {
			
			if ('undefined' === typeof site_credentials[site_id] || false === site_credentials[site_id]) {
				
				// Can we get them from the cache
				
				var possible_credentials = UpdraftCentral.storage_get('filesystem_credentials_'+site_hash);
				
				var site_heading = UpdraftCentral.get_site_heading($site_row);
				
				// Have we already got them? If not, then pop up a box to ask for them
				
				UpdraftCentral.open_modal(udclion.updates.connection_information, UpdraftCentral.template_replace('updates-request-credentials', { credentials_form: site_updates_meta.filesystem_form, site_heading: site_heading } ), function() {
					
					var save_credentials_in_browser = $('#updraftcentral_modal #filesystem-credentials-save-in-browser').is(':checked');
					
					// This needs to be set, to prevent the actions that trigger when the modal is closed another way from taking place
					credentials_modal_opened = false;
					
					site_credentials[site_id] = $('#updraftcentral_modal .request-filesystem-credentials-dialog-content input').serialize();
					
					UpdraftCentral.close_modal();
					
					if (save_credentials_in_browser) {
						item.save_credentials_in_browser = true;
						queue.replace_front(item);
					}
					
					// This is done via a near-immediate timer just to try to prevent the call stack getting hideously long
					setTimeout(function() { queue.unlock(); process_queue(); }, 10);
					
				}, udclion.updates.update, function() {
					modal_is_for_item = item;
					credentials_modal_opened = true;
					$('#updraftcentral_modal .request-filesystem-credentials-dialog-content input[value=""]:first').focus();
					
					if (possible_credentials) {
						
						possible_credentials = UpdraftCentral.unserialize(possible_credentials);
						
						// We pre-fill the form, rather than just trying them. This allows credentials to change.
						console.log(possible_credentials);
						if (possible_credentials) {
							$.each(possible_credentials, function(index, value) {
								var type = $('#updraftcentral_modal .request-filesystem-credentials-dialog-content input[name="'+index+'"]').attr('type');
								if ('text' == type || 'number' == type || 'password' == type) {
									$('#updraftcentral_modal .request-filesystem-credentials-dialog-content input[name="'+index+'"]').val(value);
								} else if ('checkbox' == type) {
									if (value) {
										$('#updraftcentral_modal .request-filesystem-credentials-dialog-content input[name="'+index+'"]').prop('checked', true);
									} else {
										$('#updraftcentral_modal .request-filesystem-credentials-dialog-content input[name="'+index+'"]').prop('false', true);
									}
								} else if ('radio' == type) {
									$('#updraftcentral_modal .request-filesystem-credentials-dialog-content input[name="'+index+'"][value="'+value+'"]').prop('checked', true);
								} else if (type) {
									console.log("UpdraftCentral: unrecognised field type in credential form: type="+type+", field index="+index);
								}
							});
							$('#updraftcentral_modal #filesystem-credentials-save-in-browser').prop('checked', true);
						}
						
					}
					
				});
				
				return;
				
			} else {
				if (debug_level > 0) {
					console.log("UpdraftCentral/updates: process_queue(): filesystem credentials are needed, and available");
				}
			}
			
		}
		
		if (site_updates_meta && site_updates_meta.hasOwnProperty('automatic_backups') && site_updates_meta.automatic_backups) {
			
			console.log(site_updates_meta);
			console.log(site_updates_meta.automatic_backups);
			
			var autobackup_requested = item.hasOwnProperty('autobackup_requested') && item.autobackup_requested;
			
			if (autobackup_requested) {
				
				var autobackup_complete = item.hasOwnProperty('autobackup_complete') && item.autobackup_complete;
				
				if (!autobackup_complete) {
					console.log("UpdraftCentral: automatic pre-upgrade backup was already requested, but is not yet complete");
					return;
				} else {
					console.log("UpdraftCentral: automatic pre-upgrade backup was already requested, and is complete");
				}
				
			} else {
				console.log("UpdraftCentral: automatic pre-upgrade backup indicated - will request");
				
				item.autobackup_requested = true;
				item.autobackup_complete = false;
				
				queue.replace_front(item);
				
				var only_these_file_entities = '';
				
				if (updates.plugins && updates.plugins.length > 0) {
					only_these_file_entities = 'plugins';
				}
				
				if (updates.themes && updates.themes.length > 0) {
					only_these_file_entities = (only_these_file_entities) ? only_these_file_entities+',themes' : 'themes';
				}
				
				if (updates.core && updates.core.length > 0) {
					only_these_file_entities = (only_these_file_entities) ? only_these_file_entities+',wpcore' : 'wpcore';
				}
				
				extra_data = {
					_listener_title: '<h2>'+$site_row.data('site_description')+' - '+udclion.updates.automatic_backup+'</h2>'
				}
				
				// Q. How does this method know which site to start the backup on? A. It uses UpdraftCentral.$site_row. If we ever change to start allowing working on updates on multiple sites, this may need looking at.
				
				if (debug_level > 0) {
					console.log("UpdraftCentral/updates: process_queue(): running automatic backup; entities: "+only_these_file_entities);
				}
				
				UpdraftCentral_Module_UpdraftPlus.backupnow_go(false, false, false, only_these_file_entities, extra_data, udclion.updates.automatic_backup);
				
				return;
			}
			
		}
		
		// Now that we have some creds to try, we remove the item from the queue (if the update fails, it'll only be retried via the user manually re-attempting)
		queue.dequeue();
		
		if ('undefined' !== typeof site_credentials[site_id] && false !== site_credentials[site_id]) {
			updates.meta = {
				filesystem_credentials: site_credentials[site_id]
			}
		}
		
		UpdraftCentral.send_site_rpc('updates.do_updates', updates, $site_row, function(response, code, error_code) {
			if ('ok' == code && response) {
				
				var any_successes = false;
				var any_errors = false;
				var $tbody = $site_row.find(".updates_table_container tbody");
				
				$(response.data.plugins).each(function(index, plugin) {
					
					var plugin_file = plugin.plugin;
					var $update_row = $site_row.find('.updraft_updates_output .updates-plugin-update[data-plugin-file="'+plugin_file+'"]');
					var entity_info = $update_row.data('plugin-info');
					
					if (plugin.hasOwnProperty('error')) {
						any_errors = true;
						update_error(plugin, $site_row, $update_row, entity_info);
					} else {
						any_successes = true;
						if (debug_level > 0 && plugin.hasOwnProperty('messages')) {
							console.log(plugin.messages)
						}
						$update_row.slideUp('slow', function() {
							$update_row.remove();
							if ($tbody.children().length < 1) {
								$tbody.closest('.updates_table_container').remove();
								$site_row.find('.updraft_updates_output').append("<h5>" + udclion.updates.no_updates  + "</h5>");
							}
						});
					}
					
				});
				
				$(response.data.themes).each(function(index, theme) {
					var theme_slug = theme.theme;
					var $update_row = $site_row.find('.updraft_updates_output .updates-theme-update[data-theme="'+theme_slug+'"]');
					var entity_info = $update_row.data('theme-info');
					
					if (theme.hasOwnProperty('error')) {
						any_errors = true;
						update_error(theme, $site_row, $update_row, entity_info);
					} else {
						any_successes = true;
						if (debug_level > 0 && theme.hasOwnProperty('messages')) {
							console.log(theme.messages)
						}
						$update_row.slideUp('slow', function() {
							$update_row.remove();
							if ($tbody.children().length < 1) {
								$tbody.closest('.updates_table_container').remove();
								$site_row.find('.updraft_updates_output').append("<h5>" + udclion.updates.no_updates  + "</h5>");
							}
						});
					}
				});
				
				$(response.data.core).each(function(index, core) {
					var $update_row = $site_row.find('.updraft_updates_output .updates-core-update');
					var entity_info = $update_row.data('core-info');
					entity_info.name = udclion.updates.wordpress;
					
					if (core.hasOwnProperty('error')) {
						any_errors = true;
						update_error(core, $site_row, $update_row, entity_info);
					} else {
						any_successes = true;
						if (debug_level > 0 && core.hasOwnProperty('messages')) {
							console.log(core.messages)
						}
						$update_row.slideUp('slow', function() {
							$update_row.remove();
							if ($tbody.children().length < 1) {
								$tbody.closest('.updates_table_container').remove();
								$site_row.find('.updraft_updates_output').append("<h5>" + udclion.updates.no_updates  + "</h5>");
							}
						});
					}
				});
				
				if (any_successes && !any_errors && save_credentials_in_browser) {
					UpdraftCentral.storage_set('filesystem_credentials_'+site_hash, site_credentials[site_id], true);
				}
				
			} else {
				// Error: still want to reset UI state
				if (updates.hasOwnProperty('plugins')) {
					$(updates.plugins).each(function(index, plugin) {
						var plugin_file = plugin.plugin;
						var $update_row = $site_row.find('.updraft_updates_output .updates-plugin-update[data-plugin-file="'+plugin_file+'"]');
						var entity_info = $update_row.data('plugin-info');
						plugin.error = 'update_failed';
						update_error(plugin, $site_row, $update_row, entity_info);
					});
				}
				if (updates.hasOwnProperty('themes')) {
					$(updates.themes).each(function(index, theme) {
						var theme_slug = theme.theme;
						var $update_row = $site_row.find('.updraft_updates_output .updates-theme-update[data-theme="'+theme_slug+'"]');
						var entity_info = $update_row.data('theme-info');
						theme.error = 'update_failed';
						update_error(theme, $site_row, $update_row, entity_info);
					});
				}
				if (updates.hasOwnProperty('core')) {
					$(updates.core).each(function(index, core) {
						var $update_row = $site_row.find('.updraft_updates_output .updates-core-update');
						var entity_info = $update_row.data('core-info');
						entity_info.name = udclion.updates.wordpress;
						core.error = 'update_failed';
						update_error(core, $site_row, $update_row, entity_info);
					});
				}
			}
			
			if (debug_level > 0) {
				console.log("UpdraftCentral/updates: process_queue(): unlocking queue");
			}
			
			queue.unlock();
			
			process_queue();
			
		}, spinner_where, 180);
		
	}
	
});
