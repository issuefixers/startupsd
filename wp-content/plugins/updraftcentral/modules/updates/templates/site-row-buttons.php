<button class="btn btn-success-outline btn-sm updraftcentral-hide-in-other-tabs updraftcentral-show-in-tab-updates updraftcentral_action_show_updates" title="<?php esc_attr_e('Show updates', 'updraftcentral');?>">
	<span class="dashicons dashicons-cloud">	</span>
	<?php _e('Show updates', 'updraftcentral'); ?>
</button>
