<div id="updraftcentral_updraftplus_actions" class="updraftcentral_mode_actions updraftcentral_action_box updraftcentral-show-in-tab-updates updraftcentral-hide-in-other-tabs">

	<button class="updraftcentral_action_manage_sites btn btn-info-outline btn-sm">
		<span class="dashicons dashicons-arrow-left-alt2"></span>
		<?php _e('Go back to sites management', 'updraftcentral'); ?>
	</button>

	<button class="updraftcentral_action_choose_another_site btn btn-success-outline btn-sm updraftcentral-hide-in-tab-initially">
		<span class="dashicons dashicons-arrow-left-alt2"></span>
		<?php printf(__('Choose another site to manage %s on', 'updraftcentral'), 'updates'); ?>
	</button>

</div>
