<?php if (empty($updraftcentral_hide_action_box)) { ?>
<div id="updraftcentral_updraftplus_actions" class="updraftcentral_mode_actions updraftcentral_action_box updraftcentral-show-in-tab-future updraftcentral-hide-in-other-tabs">

	<button class="updraftcentral_action_manage_sites btn btn-info-outline btn-sm">
		<span class="dashicons dashicons-arrow-left-alt2"></span>
		<?php _e('Go back to sites management', 'updraftcentral'); ?>
	</button>

</div>
<?php } ?>

<div id="updraftcentral_panel_thefuture" class="updraftcentral-show-in-tab-future updraftcentral-hide-in-other-tabs">

	<h2><?php _e('UpdraftCentral - The future', 'updraftcentral');?></h2>

	<p><?php _e('Thank you for using this early release of UpdraftCentral! This is just a beginning... we have lots of future plans.', 'updraftcentral');?></p>

	<p><?php _e('The main feature that we are working on next is adding further features to the updates management - i.e. managing updates of WordPress, plugins and themes across all your managed sites. We plan to include these enhancements in all versions of UpdraftCentral.', 'updraftcentral');?></p>
	
	<h3><?php _e('Feature ideas and other suggestions', 'updraftcentral');?></h3>
	
	<p><?php _e('If you want to have a say in the future of UpdraftCentral, then please <a href="https://updraftplus.com/make-a-suggestion">do post in our feature requests / suggestions forum, here.</a> You will need a login to post - <a href="https://updraftplus.com/my-account/">you can sign up for one here.</a> We may not reply directly to a feature suggestion or every comment, but you can be assured that we are reading them all.', 'updraftcentral');?></p>
	
	<h3><?php _e('Other feedback', 'updraftcentral');?></h3>
	
	<p>
	
		<?php _e('If you already want to give UpdraftCentral a 5-star rating, then... thank you!', 'updraftcentral');?> <a href="https://wordpress.org/support/view/plugin-reviews/updraftcentral?rate=5#postform"><?php _e('You can do so at this link.', 'updraftcentral');?></a>
	
		<?php _e("If you think UpdraftCentral has problems and isn't yet worth five stars, then please post in the support forum instead.", 'updraftcentral');?> <a href="https://wordpress.org/support/plugin/updraftcentral"><?php _e('You can do so at this link.', 'updraftcentral');?></a> <?php _e("Please don't post negative reviews (less than 5 stars) yet - it makes it hard for a plugin to get off the ground at all if it begins with bad reviews, which deter any other users from trying it out (whereas, 5-star reviews bring in new users, and help the plugin to get better).", 'updraftcentral');?></a>
	
	</p>
	
</div>
