<div id="updraftcentral_notice_container">
<?php
	foreach ($this->notices as $notice) {
		if (!is_array($notice)) continue;
		?>
		<div class="updraftcentral_notice updraftcentral_notice_level_<?php
			echo $notice['level'];
			if (!empty($notice['extra_classes'])) echo ' '.$notice['extra_classes'];
		?>">
			<button type="button" class="updraftcentral_notice_dismiss"></button>
			<?php echo $notice['content']; ?>
		</div>
<?php } ?>
</div>
