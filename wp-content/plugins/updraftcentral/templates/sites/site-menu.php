<div class="dropdown updraft-dropdown updraftcentral_rowmenu" data-site_id="<?php echo $site->site_id; ?>">
	<a href="#" data-toggle="dropdown">
			<span class="dashicons dashicons-menu"></span>
		</a>
	<ul class="dropdown-menu updraft-dropdown-menu pull-middle" data-site_id="<?php echo $site->site_id; ?>">
<!--		
		Markup for a sub-menu - sub-menu would be under the menu item 'backups'
		<li class="dropdown-submenu">
			<a href="#" tabindex="-1" data-toggle="dropdown">
				<span class="updraft-dropdown-item">
					<span class="dashicons dashicons-upload">	</span>
					<?php _e('Backups', 'updraftcentral'); ?>	
					<span class="dashicons dashicons-arrow-right"></span>
				</span>
			</a>
			<ul class="dropdown-menu">
				<li>
					<a href="#" tabindex="-1" data-toggle="dropdown" class="row_backupnow">
						<span class="updraft-dropdown-item">
							<span class="dashicons dashicons-upload">	</span>
							<?php _e('Backup now', 'updraftcentral'); ?>
						</span>
				</a>
			</li>
				<li>
					<a href="#" tabindex="-1">
						<span class="updraft-dropdown-item">
					<span class="dashicons dashicons-backup">	</span>
					<?php _e('Existing backups', 'updraftcentral');?>
						</span>
					</a>
				</li>
			</ul>
		</li>
-->
		<li>
			<a href="#" tabindex="-1" data-toggle="dropdown" class="row_backupnow">
				<span class="updraft-dropdown-item">
					<span class="dashicons dashicons-upload">	</span>
					<?php _e('Backup now', 'updraftcentral'); ?>
				</span>
				</a>
			</li>
			<?php
				if(empty($site->description)) {
			?>
			<li>
				<a href="#" tabindex="-1">
				<span class="updraft-dropdown-item updraftcentral_site_adddescription">
					<span class="dashicons dashicons-edit">	</span>
					<span><?php _e('Site configuration', 'updraftcentral'); ?></span>
				</span>
				</a>
			</li>
			<?php
				} else {
			?>
			<li>
				<a href="#" tabindex="-1">
				<span class="updraft-dropdown-item updraftcentral_site_adddescription updraftcentral_site_editdescription">
					<span class="dashicons dashicons-edit">	</span>
					<span><?php _e('Site configuration', 'updraftcentral'); ?></span>
				</span>
				</a>
			</li>
			<?php
				}
			?>
			<li>
			<a href="#" tabindex="-1">
				<span class="updraft-dropdown-item updraftcentral_site_delete">
					<span class="dashicons dashicons-no-alt">	</span>
					<span><?php _e('Remove site', 'updraftcentral'); ?></span>
				</span>
			</a>
			</li>
		</ul>
</div>
