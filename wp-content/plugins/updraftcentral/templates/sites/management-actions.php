<div id="updraftcentral_sites_actions" class="updraftcentral_mode_actions updraftcentral_action_box updraftcentral-show-in-tab-sites updraftcentral-hide-in-other-tabs">

	<div class="col-sm-12 text-center updraftcentral_dashboard_newsite">
		<button type="button" class="btn btn-sm btn-primary-outline" id="updraftcentral_dashboard_newsite">
			<span class="dashicons dashicons-plus dashicon-fix">	</span>
				<?php _e('Add Site', 'updraftcentral'); ?>
		</button>
	</div>

</div>
