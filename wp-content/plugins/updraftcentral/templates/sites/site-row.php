<div class="row updraftcentral_site_row<?php if (!empty($site->unlicensed)) echo ' site_unlicensed';?>" <?php echo $site_data_attributes;?>>

	<div class="col-sm-6 updraftcentral_row_sitelabel">
<!--		<label class="c-input c-checkbox">
			<input type="checkbox" class="updraft-site-checkbox" data-site_id="<?php echo $site->site_id; ?>" >
			<span class="c-indicator"></span>
		</label>
-->
		<?php
			if (empty($site->description)) {
				?>
				<div class="updraft_site_title">
					<a href="<?php esc_attr_e($site->url); ?>"><?php esc_attr_e($site->url); ?></a>
					<?php if (!empty($site->unlicensed)) { ?>
						<br><span class="updraft_site_unlicensed"><?php _e('A licence is required to manage this site', 'updraftcentral');?></span>
					<?php } ?>
				</div>
				<br>
				<?php
			} else {
				?>
				<div class="updraft_site_title">
					<span title="<?php esc_attr_e($site->url); ?>"><?php echo htmlspecialchars($site->description);?></span>
				</div>
				<br class="updraft-full-hidden">
				<br>
				<a href="<?php esc_attr_e($site->url); ?>" class="updraftcentral_site_url_after_description"><?php esc_attr_e($site->url); ?></a>
				<?php if (!empty($site->unlicensed)) { ?>
					<br><span class="updraft_site_unlicensed"><?php _e('A licence is required to manage this site', 'updraftcentral');?></span>
				<?php } ?>
				<br>
				<?php
			}
		?>
	</div>
	<div class="col-sm-6 updraftcentral_row_site_buttons">
		<div class="updraftcentral_row_container">
			<div class="updraft_site_actions updraftcentral-hide-in-other-tabs updraftcentral-show-in-tab-sites">
				<button type="button" class="btn btn-black-outline btn-sm row_siteinfo" title="<?php _e('Site information', 'updraftcentral'); ?>">
					<span class="dashicons dashicons-info">	</span>
				</button>
			</div>
			<button class="btn btn-updraftplus-outline btn-sm updraftcentral_site_dashboard updraftcentral-hide-in-other-tabs updraftcentral-show-in-tab-sites">
				<span class="dashicons dashicons-dashboard">	</span>
				<?php _e('WP Dashboard', 'updraftcentral'); ?>
			</button>

			<?php do_action('updraftcentral_site_row_after_buttons'); ?>

			<?php include 'site-menu.php'; ?>
		</div>
	</div>
	
	<div class="col-sm-12 updraftcentral_row_extracontents"></div>
	
</div>

<hr class="updraftcentral_row_divider">
