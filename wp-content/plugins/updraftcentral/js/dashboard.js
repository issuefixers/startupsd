jQuery(document).ready(function($) {
	UpdraftCentral = UpdraftCentral();
});

// Only needed for IE9 support - http://caniuse.com/#feat=console-basic
// Console-polyfill. MIT license.
// https://github.com/paulmillr/console-polyfill
// Make it safe to do console.log() always.
(function(global) {
	'use strict';
	global.console = global.console || {};
	var con = global.console;
	var prop, method;
	var empty = {};
	var dummy = function() {};
	var properties = 'memory'.split(',');
	var methods = ('assert,clear,count,debug,dir,dirxml,error,exception,group,' +
	'groupCollapsed,groupEnd,info,log,markTimeline,profile,profiles,profileEnd,' +
	'show,table,time,timeEnd,timeline,timelineEnd,timeStamp,trace,warn').split(',');
	while (prop = properties.pop()) if (!con[prop]) con[prop] = empty;
	while (method = methods.pop()) if (typeof con[method] !== 'function') con[method] = dummy;
	// Using `this` for web workers & supports Browserify / Webpack.
})(typeof window === 'undefined' ? this : window);

/**
 * This is the callback on the Paginator class
 * @callback paginatorCallback
 * @param {int} current_page
 */
/**
 * create paginator markup and manages the active page.
 *
 * @constructor
 * @param {Object} location - A jQuery DOM object used for placing the paginator
 * @param {Object} page_info - an object describing the paginator
 * @param {int} page_info.current_page - the current active page on the paginator
 * @param {int} page_info.total_pages - the amount of pages the paginator should show
 * @param {paginatorCallback} page_change - what should be done after there's a page change
 * @returns {void}
 */
function UpdraftCentral_Paginator(location, page_info, page_change){
	var self = this;
	
	var current = page_info.current_page;
	var total = page_info.total_pages;
	var callback = page_change;
	
	/**
	 * appends the paginator to the DOM and sets the active page
	 * @returns {void}
	 */
	function init(){
		append(location);
		set_active(current);
	}
	init();
	
	/**
	 * sets the active page
	 * @param {int} page_number - the page number that is to be active
	 * @returns {void}
	 */
	function set_active(page_number){
		self.element.find('.page').each(function(index, element){
			
			jQuery(this).removeClass('page_active');
			jQuery(this).attr('aria-selected', false);
			
			if(jQuery(this).data('page') === page_number){
				jQuery(this).addClass('page_active');
				jQuery(this).attr('aria-selected', true);
			}
		})
		
		self.element.find('.page_prev').removeClass('disabled');
		self.element.find('.page_next').removeClass('disabled');
		if(page_number === 1){
			self.element.find('.page_prev').addClass('disabled');
		}else if(page_number === total){
			self.element.find('.page_next').addClass('disabled');
		}
		trigger();
	}
	
	/**
	 * sets the next page as active
	 * @returns {void}
	 */
	function next(){
		if(current < total){
			current++;
			set_active(current);
		}
	}
	
	/**
	 * sets the previous page as active
	 * @returns {void}
	 */
	function prev(){
		if(current > 1){
			current--;
			set_active(current);
		}
	}
	
	/**
	 * go to a page and set it as active
	 * @param {int} page_number - the page number that is to be active
	 * @returns {void}
	 */
	function go_to(page_number){
		if(page_number !== current){
			current = page_number;
			set_active(page_number);
		}
	}
	
	/**
	 * inserts the paginator markup to the DOM
	 * @param {string} _location - a selector for where the paginator should be
	 * @returns {void}
	 */
	function append(_location){
		var pages = [];
		for(var i = 1; i <= total; i++){
			pages.push(i);
		}
		
		self.element = jQuery(UpdraftCentral.template_replace('dashboard-paginator', { pages:pages }));
		self.element.appendTo(_location);
		
		self.element.on('click', 'a', function(e){
			e.preventDefault();	
			
			if(jQuery(this).hasClass('active')){
				return;
			}
			
			if(jQuery(this).hasClass('page_prev')){
				prev();
			}else if(jQuery(this).hasClass('page_next')){
				next();
			}else if (jQuery(this).hasClass('page')){
				var page_number = jQuery(this).data('page');
				go_to(page_number);
			}
			
		});
	}
	
	/**
	 * set what should happen on a page change
	 * @param {paginatorCallback} _callback - a call back that triggers after a page change
	 * @returns {void}
	 */
	this.page_change = function(_callback){
		callback = _callback;
	}
	/**
	 * triggers a jquery event on the paginator element and executes the page_change callback
	 * @fires page_change
	 * @returns {void}
	 */
	function trigger(){
		self.element.trigger("page_change", current);
		if(jQuery.isFunction(callback)){
			callback(current);
		}
	}
}

var UpdraftCentral = function() {
	
	// This is just used internally to log more things. Set to 0 to turn it off (which won't necessarily prevent all console logging).
	// It's not completely systematic/consistent. Logging has been added in an ad hoc manner during development/testing to help with debugging.
	// This gets passed through from the PHP constant UPDRAFTCENTRAL_DEBUG_LEVEL
	var updraftcentral_debug_level = ('undefined' === typeof udclion || !udclion.hasOwnProperty('debug_level')) ? 0 : udclion.debug_level;
	var listener_poll_interval = (updraftcentral_debug_level > 0) ? 5000 : 10000;
	
	var mobile_width = 583
	
	var $ = jQuery;
	
	var modal_action_callback;
	
	// Use to hold the current site being operated on (e.g. state for modals) (N.B. - you may need to explicitly set this, depending on whether you're using a convenience function that already sets it, or not)
	var $site_row;

	/**
	 * Set the debugging level
	 * 
	 * @param {number} debug_level - debugging level, controlling how much console output there will be. The higher the value, the more output. Generally only 0 (minimal), 1 (some), 2 (very much) and 3 (even more) are useful levels
	 * @returns {void}
	 */
	this.set_debug_level = function(debug_level) {
		updraftcentral_debug_level = debug_level;
	}
	
	/**
	 * Get the current debugging level
	 * 
	 * @returns {number} - the debugging level (@see set_debug_level)
	 */
	this.get_debug_level = function() {
		return updraftcentral_debug_level;
	}
	
	// Dialog methods - this is just an abstraction layer (currently onto Bootbox, http://bootboxjs.com), allowing us to easily swap to a different provider if we ever need to
	this.dialog = {};
	
	/**
	 * Function to be called whenever a bootbox dialog is opened
	 * We use it simply to move the bootbox within the DOM if in fullscreen mode (because otherwise it won't be seen).
	 * @returns {void}
	 */
	var bootbox_opened = function() {
		// It only needs moving if in full-screen mode; so, we're conservative and otherwise leave it alone
		if ($.fullscreen.isFullScreen()) {
			$('.bootbox.modal').prependTo('#updraftcentral_dashboard');
		}
		// Use a new browser portal for any clicks to updraftplus.com
		$('.bootbox.modal').on('click', 'a', function(e) {
			var href = $(this).attr('href');
			if (href.substring(0, 23) == 'https://updraftplus.com' || href.substring(0, 23) == 'http://updraftplus.com') {
				e.preventDefault();
				var win = window.open(href, '_blank');
				win.focus();
			}
		});
		$('.bootbox.modal .updraftcentral_site_editdescription').click(function(e) {
			e.preventDefault();
			$(this).closest('.modal').modal('hide');
			open_site_configuration(UpdraftCentral.$site_row);
		});
		$('.bootbox.modal .updraftcentral_test_other_connection_methods').click(function(e) {
			e.preventDefault();
			$(this).closest('.modal').modal('hide');
			open_connection_test(UpdraftCentral.$site_row);
		});
	}
	
	/**
	 * @callable dialogresultCallback
	 * @param {null|boolean|String} [result=null] - the button clicked (for a confirm call) or the value entered (for a prompt); or, undefined for an alert
	 */
	
	/**
	 * Open an alert box (as a more aesthetic alternative to the traditional browser-provided alert()).
	 * @param {string} message - the message to display in the alert box
	 * @param {dialogresultCallback} result_callback - callback function that is invoked when the alert box is closed
	 * @param {boolean} [sanitize_message=true] - whether or not to put the message through sanitize_html()
	 * @returns {void}
	 * 
	 * @uses sanitize_html
	 */
	this.dialog.alert = function(message, result_callback, sanitize_message) {
		sanitize_message = ('undefined' == sanitize_message) ? true : sanitize_message;
		if (sanitize_message) {
			message = UpdraftCentral.sanitize_html(message);
		}
		bootbox.alert(message, result_callback);
		bootbox_opened();
	}

	/**
	 * Open a confirmation box (as a more aesthetic alternative to the traditional browser-provided confirm()).
	 * @param {string} question - the message to display in the alert box
	 * @param {dialogresultCallback} result_callback - callback function that is invoked when the alert box is closed
	 * @returns {void}
	 */
	this.dialog.confirm = function(question, result_callback) {
		bootbox.confirm(question, result_callback);
		bootbox_opened();
	}

	/**
	 * Open a prompt box (as a more aesthetic alternative to the traditional browser-provided prompt()).
	 * @param {string} title - the message to display in the alert box
	 * @param {string} default_value - the default value for the user response field
	 * @param {dialogresultCallback} result_callback - callback function that is invoked when the alert box is closed
	 * @returns {void}
	 */
	this.dialog.prompt = function(title, default_value, result_callback) {
		bootbox.prompt({ title: title, value: default_value, callback: result_callback});
		bootbox_opened();
	}
	
	/**
	 * Encode to base64 encoding
	 * This (or its callers) can be swapped for atob() once IE 10 support is not desired - http://caniuse.com/#feat=atob-btoa
	 * 
	 * @param {string} data - the data to base64-encode
	 * 
	 * @returns {string} - the encoded data
	 */
	this.base64_encode = function(data) {
		return forge.util.encode64(data);
	}
	
	/**
	 * Calculate an MD5 hash
	 * 
	 * @param {string} data - the data to hash
	 * 
	 * @returns {string} - the encoded data, in hex format
	 */
	this.md5 = function(data) {
		var md = forge.md.md5.create();
		md.update(data);
		return md.digest().toHex();
	}
	
	/**
	 * Open a modal window with the specified contents. Modals are separate to dialogues - that is, you can have both open at once without them interfering.
	 * @param {string} title - the title to use for the modal window
	 * @param {string} body - the HTML contents to place in the modal window
	 * @param {callback|true} action_button_callback - a callback to call when the main action button is pressed; or just true to close the modal
	 * @param {string|false} [action_button_text="Go"] - text for the action button; or, if false, an indication that there should be no action button
	 * @param {callback|null} [pre_open_callback=null] - an optional callback to call immediately before opening the modal
	 * @param {boolean} [sanitize_body=true] - whether or not to call the sanitize_html() method the passed body, or not, before placing it in the modal
	 * @param {string} [extra_classes=''] - extra CSS classes for the modal dialog (e.g. modal-lg)
	 * @returns {void}
	 */
	this.open_modal = function(title, body, action_button_callback, action_button_text, pre_open_callback, sanitize_body, extra_classes) {
		action_button_text = typeof action_button_text !== 'undefined' ? action_button_text : udclion.go;
		// By default, we assume that the input is potentially evil, and sanitize it
		sanitize_body = typeof sanitize_body !== 'undefined' ? sanitize_body : true;
		extra_classes = typeof extra_classes !== 'undefined' ? extra_classes : '';
		
		// Reset the modal's CSS classes
		$('#updraftcentral_modal_dialog .modal-dialog').removeClass().addClass('modal-dialog '+extra_classes);
		
		$('#updraftcentral_modal_dialog .modal-title').html(title);
		if (sanitize_body) body = this.sanitize_html(body);
		$('#updraftcentral_modal_dialog .modal-body').html(body);
		if (false === action_button_text) {
			$('#updraftcentral_modal_dialog button.updraft_modal_button_goahead').hide();
		} else {
			$('#updraftcentral_modal_dialog button.updraft_modal_button_goahead').html(action_button_text).show();
		}
		modal_action_callback = action_button_callback;
		if (typeof pre_open_callback !== 'undefined' && null !== pre_open_callback) pre_open_callback.call(this);
		
		$('#updraftcentral_modal_dialog').modal();
	}
	
	/**
	 * Given a site row, send back a suitable HTML site description
	 * 
	 * @param {Object} $site_row - the jQuery object for the row of the site
	 * 
	 * @returns {string} - an HTML string describing the site
	 */
	this.get_site_heading = function($site_row) {
	
		var site_description = $site_row.data('site_description');
		var site_url = $site_row.data('site_url');
		if (site_description == site_url) { site_description = ''; }
		
		var site_heading;
		if (site_description) {
			site_heading = '<a href="'+site_url+'">'+site_description+'</a>';
		} else {
			site_heading = '<a href="'+site_url+'">'+site_url+'</a>';
		}
		
		return site_heading;
	}
	
	
	
	/**
	 * Allow the user to download/save a file, with contents supplied from the inner HTML of a specified element
	 * 
	 * @param {string} filename - the filename that will be suggested to the user to save as
	 * @param {string} element_id - the DOM id of the element whose inner HTML is to be used as content
	 * @param {string} [mime_type='text/plain'] - the MIME type to indicate in the header sent to the browser
	 * @returns {void}
	 */
	this.download_inner_html = function(filename, element_id, mime_type) {
		mime_type = mime_type || 'text/plain';
		var element_html = document.getElementById(element_id).innerHTML;
		var link = document.body.appendChild(document.createElement('a'));
		link.setAttribute('download', filename);
		link.setAttribute('style', "display:none;");
		link.setAttribute('href', 'data:' + mime_type  +  ';charset=utf-8,' + encodeURIComponent(element_html));
		link.click(); 
	}
	
	/**
	 * Close the modal dialog
	 * @returns {void}
	 */
	this.close_modal = function() {
		$('#updraftcentral_modal_dialog').modal('hide');
	}
	
	/**
	 * Sanitizes passed HTML, so that it is safe for display. Uses Google's Caja parser.
	 * 
	 * @param {string} html - the potentially suspicious HTML
	 * @returns {string} The sanitized HTML
	 */
	this.sanitize_html = function(html) {
		var web_only = function(url) { if(/^https?:\/\//.test(url)) { return url; }}
		var same_id = function(id) { return id; }
		// The html_sanitize object comes from Google's Caja
		// This version retains data- attributes. It removes style attributes (but not CSS classes)
		return html_sanitize.sanitize(html, web_only, same_id);
	}
	
	/**
	 * jQuery callback for row click events
	 * 
	 * @callable rowclickerCallback
	 * @param {string} $site_row - the jQuery row object for the site that the click was for
	 * @param {Number} site_id - the site ID for the site that the click was for
	 * @param {Object} event - the event received from jQuery
	 * 
	 * @return {*} prevent_default - if anything other than (boolean)true, then event.preventDefault() is called
	 */
	
	/**
	 * De-register all row-clickers. The normal use of this is when switching tabs.
	 * @returns {void}
	 */
	function deregister_row_clickers() {
		$('#updraftcentral_dashboard_existingsites_container').off();
	}
	
	/**
	 * De-register all events on the modal. The normal use of this is when switching tabs.
	 * @returns {void}
	 */
	function deregister_modal_listeners() {
		$('#updraftcentral_modal').off();
	}
	
	/**
	 * Register click events for specified items in the UpdraftCentral site list (prevents repeating lots of jQuery boilerplate).
	 * Note that all row clickers are always deregistered upon a mode change (i.e. tab change). So, the correct place to call this function is when updraftcentral_dashboard_mode_set_(your mode) is triggered (or updraftcentral_dashboard_mode_set for all tabs).
	 * 
	 * @param {string} selector - the selector to use
	 * @param {rowclickerCallback} callback - callback function that will be called upon the click event
	 * @param {boolean} [hide_other_sites=false] - if set, then the click will cause other sites in the tab to be hidden
	 * @param {string} [on_event='click'] - the event type to listen for. In the special case of 'keypress', the default event will not be prevented
	 * @returns {void}
	 */
	this.register_row_clicker = function(selector, callback, hide_other_sites, on_event) {
		on_event = typeof on_event !== 'undefined' ? on_event : 'click';
		hide_other_sites = typeof hide_other_sites !== 'undefined' ? hide_other_sites : false;
		params = {};
		if ('tripleclick' == on_event) { params.threshold = 500; }
		$('#updraftcentral_dashboard_existingsites_container').on(on_event, '.updraftcentral_site_row '+selector, params, function(event) {
			if (on_event != 'keypress') { event.preventDefault(); }
			UpdraftCentral.$site_row = $(this).closest('.updraftcentral_site_row');
			var site_id = UpdraftCentral.$site_row.data('site_id');
			if (hide_other_sites) {
				$('#updraftcentral_dashboard_existingsites > .updraftcentral_site_row:not([data-site_id="'+site_id+'"]), #updraftcentral_dashboard_existingsites > .updraftcentral_row_divider').slideUp();
				$('.updraftcentral_mode_actions .updraftcentral_action_choose_another_site').show();
			}
			callback.call(this, UpdraftCentral.$site_row, site_id, event);
		});
	}
	var register_row_clicker = this.register_row_clicker;
	
	/**
	 * Register click events for specified items in the UpdraftCentral modal (prevents repeating lots of jQuery boilerplate).
	 * Note that all modal clickers are always deregistered upon a mode change (i.e. tab change). So, the correct place to call this function is when updraftcentral_dashboard_mode_set_(your mode) is triggered (or updraftcentral_dashboard_mode_set for all tabs).
	 * 
	 * @param {string} selector - the selector to use
	 * @param {rowclickerCallback} callback - callback function that will be called upon the click event
	 * @param {string} [on_event='click'] - the event type to listen for. In the special case of 'keypress', the default event will not be prevented
	 * @returns {void}
	 */
	this.register_modal_listener = function(selector, callback, on_event) {
		on_event = typeof on_event !== 'undefined' ? on_event : 'click';
		params = {};
		if ('tripleclick' == on_event) { params.threshold = 500; }
		$('#updraftcentral_modal').on(on_event, selector, params, function(event) {
			callback.call(this, event);
		});
	}
	
	$('#updraftcentral_modal_dialog button.updraft_modal_button_goahead').click(function() {
		if (true === modal_action_callback) {
			this.close_modal();
		} else {
			modal_action_callback.call(this);
		}
	});
	
	/**
	 * jQuery callback for row click events
	 * 
	 * @callable listenerCallback
	 * 
	 * @param {Object} $listener_row - the jQuery object of the listener itself
	 * @param {Object} $site_row - the jQuery row object for the site that the click was for
	 * @param {Number} site_id - the site ID for the site that this is a listener for
	 * @param {*} [data] - the returned data from the polling operation (if it is that sort of listener)
	 * @returns {*} - if 0 is returned, then the listener will be closed. If 1 is returned, then no more polling will be done, but the listener will not be closed. If an object with a property 'call' is returned, then this will be called. Otherwise, nothing will be done.
	 */
	
	var listener_processors = {};
	/**
	 * Register a listener callback - a callback function to be used in association with dashboard notices which poll and update
	 * 
	 * @see create_dashboard_listener
	 * 
	 * @param {string} listener_type - an identifying string, indicating the listener type
	 * @param {listenerCallback} callback - a listener callback function
	 * @returns {void}
	 */
	this.register_listener_processor = function(listener_type, callback) {
		listener_processors[listener_type] = callback;
	}
	
	/**
	 * Poll all listener rows on the dashboard for activity
	 * @returns {void}
	 */
	function poll_listeners() {
		
		//var listener_calls = {};
		
		$('#updraftcentral_notice_container .updraftcentral_listener').each(function(ind) {
			var site_id = $(this).data('site_id');
			var listener_type = $(this).data('type');
			var $listener_row = this;
			var $site_row = $('#updraftcentral_dashboard_existingsites .updraftcentral_site_row[data-site_id="'+site_id+'"');
			var finished = $(this).data('finished');

			if (finished) { return; }
			
			if (updraftcentral_debug_level > 1) {
				console.log("poll_listeners(): site_id="+site_id+", listener_type="+listener_type);
			}
			
			if ($site_row.length > 0 && listener_processors.hasOwnProperty(listener_type)) {
// 				if (typeof listener_calls[site_id] === 'undefined') listener_calls[site_id] = [];
				var call_this = listener_processors[listener_type].call(this, $listener_row, $site_row, site_id);
				// We could multiplex all the calls to the same site. That would involve plenty of work, but would be worth if for the efficiency - if it weren't the case that HTTP/2 takes care of this.
				if (0 === call_this) {
					$(this).data('finished', true);
					$('#updraftcentral_dashboard_existingsites').trigger('updraftcentral_listener_finished_'+listener_type, {
						site_id: site_id,
						site_row: $site_row,
						listener_row: $listener_row,
						listener_type: listener_type
					});
					$($listener_row).clearQueue().delay(10000).slideUp('slow', function() { $(this).remove(); });
				} else if (1 === call_this) {
					$(this).data('finished', true);
					$('#updraftcentral_dashboard_existingsites').trigger('updraftcentral_listener_finished_'+listener_type, {
						site_id: site_id,
						site_row: $site_row,
						listener_row: $listener_row,
						listener_type: listener_type
					});
				} else if (null != call_this && call_this.hasOwnProperty('call')) {
					var call_type = call_this.call;
					UpdraftCentral.send_site_rpc(call_this.call, call_this.data, $site_row, function(response, code, error_code) {
						if ('ok' == code && false !== response && response.hasOwnProperty('data')) {
							if (listener_processors.hasOwnProperty(call_type)) {
								listener_processors[call_type].call(this, $listener_row, $site_row, site_id, response.data);
							} else {
								console.log("UpdraftCentral: listener type "+call_type+" has no registered processor (dump of all registered processors follows)");
								console.log(listener_processors);
							}
						}
					});
				}
			} else if ($site_row.length > 0) {
				console.log("UpdraftCentral: listener type "+listener_type+" has no registered processor (dump of all registered processors follows)");
				console.log(listener_processors);
			} else {
				console.log("UpdraftCentral: listener for site_id="+site_id+" with type "+listener_type+": site row not found");
			}
		});
		
	}
	
	setInterval(function() { poll_listeners(); }, listener_poll_interval);
	
	// A separate ud_rpc object for each site
	var ud_rpcs = [];
	
	/**
	 * Given a site (identified by its row), get the URL to send HTTP requests to. This is abstracted for convenience and maintainability if there need to be future changes.
	 *
	 * @param {Object} $site_row - the jQuery object for the site row
	 * 
	 * @returns {string} - the URL
	 */
	this.get_contact_url = function($site_row) {
		// Used to be site_url; we changed to using the admin_url because when checking updates (e.g.), some sites are only registering their hooks on the back-end. Since UC is typically providing wp-admin-like functions, it makes sense to go for the back-end.
		var admin_url = $site_row.data('admin_url').replace(/\/+$/, '');
		return admin_url+'/admin-ajax.php';
	}
	
	/**
	 * Given a site (identified by its row), get a UpdraftPlus_Remote_Communications (remote communications) object for remote communications. This function does the heavy lifting of getting all the connection configuration for the site, and then passing it along to get_udrpc()
	 * 
	 * @param {Object} $site_row - the jQuery object for the site row
	 * @param {string} [connection_method_config="direct"] - either 'direct_default_auth'|'direct_jquery_auth'|'direct_manual_auth' (which means to send directly to the destination) or 'via_mothership' which also sends via the PHP-back-end, but does the RSA encryption in the browser. This is not necessarily the same value as inferred from $site_row - we provide it as an extra parameter to make it possible to over-ride - e.g. for diagnostics, or where the browser mixed-content model restricts the choices.
	 * 
	 * @returns {Object} - the UpdraftPlus_Remote_Communications object
	 * 
	 * @uses get_udrpc
	 */
	function get_site_udrpc($site_row, connection_method_config) {
		
		var site_remote_public_key = $site_row.data('site_remote_public_key');
		var site_local_private_key = $site_row.data('site_local_private_key');
		var site_url = this.get_contact_url($site_row);
		var site_id = $site_row.data('site_id');
		var key_name_indicator = $site_row.data('key_name_indicator');
		var remote_user_id = $site_row.data('remote_user_id');
		
		if ('undefined' === typeof connection_method_config || ('direct_manual_auth' != connection_method_config && 'via_mothership' != connection_method_config && 'via_mothership_encrypting' != connection_method_config && 'direct_jquery_auth' != connection_method_config)) { connection_method_config = 'direct_default_auth'; }
		
		// The connection method ought not to be via_mothership - such sites shouldn't be being routed into here (but via_mothership_encrypting is allowed)
		if ('via_mothership_encrypting' == connection_method_config) {
			console.warn("UpdraftCentral: A site ("+site_id+", "+site_url+") routed via_mothership_encrypting was passed into get_site_udrpc");
			console.log($site_row);
		}
		
		var message_wrapper = false;
		
		if ('direct_default_auth' == connection_method_config) {
			// Normally, of course, feature detection should be used. But, it really is the case that we need to do browser-detection here, as they're working differently.
			var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
			// N.B. If they're set to use Digest authentication, this should not use manual - should switch back
// 			connection_method = (is_firefox) ? 'direct_manual_auth' : 'direct_jquery_auth';
			// Actually, 'jQuery method' also works in Firefox 
			connection_method = 'direct_jquery_auth';
		} else {
			connection_method = connection_method_config;
			
			// We're relying here on the fact that UDRPC object store includes the connection method in its unique ID - i.e. that this can be set on the UDRPC object, without any bad consequences.
			
			if ('via_mothership' == connection_method) {
				
				message_wrapper = {
					action: 'updraftcentral_dashboard_ajax',
					subaction: 'site_rpc',
					component: 'dashboard',
					nonce: udclion.updraftcentral_dashboard_nonce,
					site_id: site_id,
					site_rpc_preencrypted: 1
				};
				
			}
			
		}
		
		var send_cors_headers = $site_row.data('send_cors_headers');
		if ('undefined' === typeof send_cors_headers) { send_cors_headers = 1; }
		
		var auth_method = ('direct_manual_auth' == connection_method) ? 'manual' : 'jquery';
		
		var http_credentials = {};
		
		var comms_url = site_url;
		
		// When routing via the mothership, don't put in credentials, as the mothership will do that
		if ('via_mothership_encrypting' != connection_method && 'via_mothership' != connection_method) {
			var http_username = $site_row.data('http_username');
			if ('undefined' !== typeof http_username && http_username) {
				http_credentials.username = http_username;
				var http_password = $site_row.data('http_password');
				if ('undefined' !== typeof http_password) {
					http_credentials.password = http_password;
				}
			}
		} else {
			comms_url = udclion.ajaxurl;
		}
		
		if (updraftcentral_debug_level > 0) {
			console.log("UDRPC communications method: site_id="+site_id+", name_indicator="+key_name_indicator+", site_url="+site_url+", comms_url="+comms_url+", remote_user_id="+remote_user_id+", connection_method="+connection_method_config+"/"+connection_method+", send_cors_headers="+send_cors_headers);
			if (updraftcentral_debug_level > 1) {
				console.log("Remote public key follows");
				console.log(site_remote_public_key);
			}
		}
		
		var reuse_id = site_id+' '+connection_method;
		
		return get_udrpc(reuse_id, remote_user_id, key_name_indicator, site_remote_public_key, site_local_private_key, comms_url, send_cors_headers, http_credentials, auth_method, message_wrapper);
		
	}
	
	/**
	 * Given the relevant site information, get a UpdraftPlus_Remote_Communications (remote communications) object for remote communications, and also sets the current debugging level upon it.
	 * 
	 * @param {number} reuse_id - A unique ID, that can be used for re-using of the result
	 * @param {number} remote_user_id - The ID of the user on the remote WP site that the keys are for
	 * @param {string} key_name_indicator - The key name indicator (which indicates to the remote site which key to use to decrypt the message)
	 * @param {string} site_remote_public_key - The RSA public key for contacting the remote site, in PEM format
	 * @param {string} site_local_private_key - The RSA private key for the local site, in PEM format
	 * @param {string} site_url - The URL for the remote site
	 * @param {boolean} [cors_headers_wanted=true] - Whether to request that the remote application sets CORS headers with its reply
	 * @param {Object} [http_credentials={}] - an object with any HTTP credentials to be set (useful properties: username, password)
	 * @param {string} [auth_method] - the authentication method to use ('jquery' or 'manual')
 	 * @param {Object|boolean} [message_wrapper=false] - a wrapper to enclose the message in; or false for none. This is passed on to the UDRPC library, which is where the actual wrapping is done.
	 * 
	 * 
	 * @returns {Object} - the UpdraftPlus_Remote_Communications object
	 * 
	*/
	function get_udrpc(reuse_id, remote_user_id, key_name_indicator, site_remote_public_key, site_local_private_key, site_url, cors_headers_wanted, http_credentials, auth_method, message_wrapper) {
		if (typeof ud_rpcs[reuse_id] != 'undefined') {
			ud_rpc = ud_rpcs[reuse_id];
		} else {
			cors_headers_wanted = (typeof cors_headers_wanted === 'undefined') ? true : cors_headers_wanted;
			var ud_rpc = new UpdraftPlus_Remote_Communications(key_name_indicator);
			ud_rpc.set_key_local(site_local_private_key);
			ud_rpc.set_key_remote(site_remote_public_key);
			ud_rpc.activate_replay_protection();
			
			var url_match = /\/admin-ajax.php$/;
			if (url_match.test(site_url)) {
				// wp-admin/admin-ajax.php before WP 3.5 will die() if $_REQUEST['action'] is not set (3.2) or is empty (3.4). Later WP versions also check that, but after (instead of before) wp-load.php, which is where we are ultimately hooked in.
				site_url = site_url + '?action=updraft_central';
			}
			
			ud_rpc.set_destination_url(site_url);
			if ('undefined' != typeof http_credentials) { ud_rpc.set_http_credentials(http_credentials); }
			if ('undefined' != typeof auth_method) { ud_rpc.set_auth_method(auth_method); }
			if ('undefined' != typeof message_wrapper && false !== message_wrapper) {
				ud_rpc.set_message_wrapper(message_wrapper);
				ud_rpc.set_message_unwrapper(function(response) {
					var processed = process_direct_ajax_response(response, 2, false);
					if (true === processed) {
						if (response.hasOwnProperty('wrapped_response')) {
							return response.wrapped_response;
						} else {
							processed = 'wrapped_response_not_found';
						}
					}
					console.error("UDRPC: Attempt to unwrap the message failed (code: "+processed+")");
					// This is usually redundant - something further down the line will log it
					if (updraftcentral_debug_level > 1) {
						console.log(response);
					}
					return false;
				});
			}
			ud_rpc.set_cors_headers_wanted(cors_headers_wanted);
			ud_rpcs[reuse_id] = ud_rpc;
		}
		if (updraftcentral_debug_level > 0) {
			// UDRPC, at debug level 2, console.log()s lots of cryptographic internals which are only really needed when debugging that
			var ud_rpc_debug_level = (updraftcentral_debug_level > 2) ? 2 : 1;
			ud_rpc.set_debug_level(ud_rpc_debug_level);
		}
		return ud_rpc;
	}
	
	/**
	 * @callable ajaxCallback
	 * @param {*} response - the response data for the result of the call
	 * @param {String} [code] - the response code; can be 'error' in the case of an error
	 * @param {String} [error_code] - in the case of code being 'error', this contains the error code
	 */
	
	/**
	 * This function is for processing responses received via send_ajax. Since that function has two separate methods for routing the request, the common response code is abstracted out.
	 * 
	 * @param {ajaxCallback} response - callback that will be called with the results of the AJAX call
	 * @param {string} [code] - the response code; can be 'error' in the case of an error
	 * @param {string} [error_code] - in the case of code being 'error', this contains the error code
	 * @param {boolean|number} is_site_rpc - whether it was command to a remote site or not. If set to '2', then this indicates that it is site_rpc, and that the encryption was definitely done in the browser (so, we can ignore/drop certain unencrypted responses)
	 * @param {ajaxCallback} response_callback - callback that will be called with the results of the AJAX call
	 * @param {boolean} [allow_visual_responses=true] - whether or not it is permissible to display UI elements in response to the results (set this to false if the caller wants to handle it internally only)
	 * @returns {void}
	 */
	function process_ajax_response(response, code, error_code, is_site_rpc, response_callback, allow_visual_responses) {
		
		allow_visual_responses = ('undefined' === typeof allow_visual_responses) ? true : allow_visual_responses;
		
		// Bring errors up from the RPC layer. "ok" as the main code just means that a result came back successfully; but that result might itself be an error. That is someting to handle here, not in the lower-level communications library.
		
		if ('error' == code) {
			console.error("process_ajax_response: return code: "+code+", error_code: "+error_code+" - parsed response follows");
			console.log(response);
		} else if (updraftcentral_debug_level > 0) {
			console.log("process_ajax_response: return code: "+code+" - parsed response follows");
			console.log(response);
		}
		
		if (is_site_rpc && 'ok' == code && response.hasOwnProperty('response') && 'rpcerror' == response.response) {
			code = 'error';
			error_code = 'rpc_unknown_error';
			
			if (response.hasOwnProperty('data') && response.data.hasOwnProperty('code')) {
				error_code = response.data.code;
				console.error("UpdraftCentral: RPC: Error occurred ("+error_code+"); data follows");
				console.log(response.data);
				response = response.data.data;
				
				var handled = response_callback.call(this, response, code, error_code);
				
				if (true !== handled) {
					// A default message for if we don't recognise the code
					var dash_message = udclion.js_exception_occurred+' ('+error_code+')';
					// Get the error's own message, if we know about it
					if (udclion.rpcerrors.hasOwnProperty(error_code)) { dash_message = udclion.rpcerrors[error_code]; }
					
					if (allow_visual_responses) { UpdraftCentral.dialog.alert('<h2>'+udclion.communications_error+'</h2>'+dash_message); }
				}
				
				return;
			}
		}
		
		if (code == 'error') {
			
			var msg = udclion.general_js_comms_failure;
			// This variable doesn't have to be 100% correct - it's use is that a link to a relevant article is shown if it is true
			var is_comms_failure = true;
			var title = udclion.error;
			
			// If the response didn't unwrap, it may be an error response.
			if (2 == is_site_rpc && 'unwrapper_failure' == error_code && response.hasOwnProperty('code')) { error_code = response.code; }
						
			if ('json_parse_fail' == error_code) {
				if (response.indexOf('<html') > -1) {
					console.error("UpdraftCentral: JSON parse fail: looks like html was returned - remote plugin is probably not installed/inactive/blocked");
					msg = udclion.general_js_comms_failure;
					title = udclion.communications_error;
				}
			} else if ('response_empty' == error_code || 'http_post_fail' == error_code) {
				msg = udclion.general_js_comms_failure;
				title = udclion.communications_error;
			} else if ('timeout' == error_code) {
				msg = udclion.comms_failure_timeout;
				title = udclion.communications_error+' - '+udclion.timeout;
			} else if ('unauthorized' == error_code) {
				msg = udclion.comms_failure_unauthorised;
				title = udclion.communications_error;
			} else if ('unknown_response' == error_code) {
				msg = udclion.unknown_response;
				title = udclion.communications_error;
			} else if ('cannot_contact_localdev' == error_code) {
				title = udclion.communications_error;
				msg = response.message;
				if (response.hasOwnProperty('request_info') && response.request_info.hasOwnProperty('method') && response.request_info.hasOwnProperty('use_method') && response.request_info.method != response.request_info.use_method) {
					msg += '<br>'+udclion.localdev_can_work_better_with_https;
				}
			} else if ('unexpected_http_code' == error_code && is_site_rpc && response.hasOwnProperty('data') && response.data !== null && response.data.hasOwnProperty('headers') && response.data.headers.hasOwnProperty('www-authenticate') && response.data.headers['www-authenticate'].search(/Digest/i) == 0) {
				msg = response.message;
				msg += "<br>"+udclion.digest_auth_not_supported;
			} else if ('unexpected_http_code' == error_code && is_site_rpc && response.hasOwnProperty('data') && null !== response.data && response.data.hasOwnProperty('response') && response.data.response.hasOwnProperty('code') && 401 == response.data.response.code) {
				msg = udclion.comms_failure_unauthorised+' <a href="#" class="updraftcentral_site_editdescription">'+udclion.open_site_configuration+'...</a>';
			} else if (response.hasOwnProperty('message')) {
				msg = response.message;
				if (!is_site_rpc) { is_comms_failure = false; }
			} else {
				is_comms_failure = false;
				msg += '<br>'+udclion.error_code+': '+error_code;
			}
			
			// ns_error_dom_bad_uri: access to restricted uri denied - Firefox
			if (response.hasOwnProperty('status') && 401 == response.status) {
				msg = udclion.comms_failure_unauthorised+' <a href="#" class="updraftcentral_site_editdescription">'+udclion.open_site_configuration+'...</a>';
			} else if ('ns_error_dom_bad_uri: access to restricted uri denied' == error_code) {
				msg = udclion.comms_failure_unauthorised_by_browser+' <a href="#" class="updraftcentral_site_editdescription">'+udclion.open_site_configuration+'...</a>';
			}
			
			msg = '<p>'+msg+'</p>';
			
			if (is_comms_failure) {
				msg += '<p><a href="'+udclion.common_urls.connection_checklist+'">'+udclion.go_here_for_connection_help+'</a></p>';
				msg += '<p><a href="#" class="updraftcentral_test_other_connection_methods">'+udclion.test_other_connection_methods+'</a></p>';
			}
			
			if (response.hasOwnProperty('status') && 200 != response.status && 0 != response.status) {
				msg += '<p>'+udclion.http_response_status+': '+response.status+'</p>';
			}
			
			if (allow_visual_responses) { UpdraftCentral.dialog.alert('<h2>'+title+'</h2>'+msg); }
		}
		
		if (is_site_rpc && response.hasOwnProperty('data') && null != response.data) {
			if (response.data.hasOwnProperty('php_events')) {
				$.each(response.data.php_events, function(index, logline) {
					console.log("UpdraftCentral: PHP event on remote side: "+logline);
				});
			}
			if (response.data.hasOwnProperty('caught_output')) {
				console.log("UpdraftCentral: direct output on remote side: "+response.data.caught_output);
			}
			if (response.data.hasOwnProperty('php_events') || response.data.hasOwnProperty('caught_output')) {
				response.data = response.data.previous_data;
			}
		}
		
		response_callback.call(this, response, code, error_code);
	}
	
	/**
	 * Process responses received back from the mothership over AJAX. This will do some processing, and then call process_ajax_response()
	 * 
	 * @param {string} response - the response received
	 * @param {boolean} is_site_rpc - whether it was command to a remote site or not.
	 * @param {ajaxCallback|boolean} response_callback - callback that will be called with the results of the AJAX call - or, to not call, false
	 * @param {boolean} [allow_visual_responses=true] - whether or not it is permissible to display UI elements in response to the results (set this to false if the caller wants to handle it internally only). This is just passed on to process_ajax_response
	 * 
	 * @returns {boolean|string} - If the parsing did not turn up any errors, true is return; otherwise, an error code.
	 */
	function process_direct_ajax_response(response, is_site_rpc, response_callback, allow_visual_responses) {
		
		allow_visual_responses = ('undefined' === typeof allow_visual_responses) ? true : allow_visual_responses;
		
		// AJAX via the mothership comes with its results wrapped 
		
		if (response.hasOwnProperty('responsetype') && 'error' == response.responsetype) {
			if (response.hasOwnProperty('message')) { console.error("UpdraftCentral error via AJAX: "+response.message); }
			if ('cannot_contact_localdev' == response.code) { response.request_info = { method: method, use_method: use_method} }
			if (false !== response_callback) {
				process_ajax_response(response, 'error', response.code, is_site_rpc, response_callback, allow_visual_responses);
			}
			return response.code;
		}
		
		if (!response.hasOwnProperty('message') && !response.hasOwnProperty('code')) {
			console.log(response);
			if (false !== response_callback) {
				process_ajax_response(response, 'error', 'unknown_response', is_site_rpc, response_callback, allow_visual_responses);
			}
			return 'unknown_response';
		}
		
		if (updraftcentral_debug_level > 1) {
			console.log(response.responsetype+': '+response.message);
		}
		
		// When doing site RPC, the remote site's reply is in the 'data' attribute
		if (is_site_rpc) {
			
			if (response.hasOwnProperty('php_events')) {
				$.each(response.php_events, function(index, logline) {
					console.info("UpdraftCentral: PHP event on remote side: "+logline);
				});
			}
			
			if (response.hasOwnProperty('mothership_caught_output')) {
				console.info("UpdraftCentral: direct output on remote side: "+response.caught_output);
			}
			
			// This is set for a successful communication
			if (response.hasOwnProperty('rpc_response')) {
				response = response.rpc_response;
			}
		}
		
		if (false !== response_callback) {
			process_ajax_response(response, 'ok', null, is_site_rpc, response_callback, allow_visual_responses);
		}
		
		return true;
	}
	
	/**
	 * Sends a remote command via AJAX - either directly, or via the site that this plugin is installed upon.
	 * 
	 * @param {String} command - the command to send
	 * @param {*} data - data to send with the remote request
	 * @param {Object|null} $site_row - the jQuery object for the site that the command is for; or, for commands not associated with a site, null
	 * @param {String} [connection_method="direct_default_auth"] - either 'direct_default_auth' (which means to send directly to the destination) or 'via_mothership_encrypting' (which means to send via our PHP-back-end, which then sends the request), or 'via_mothership' which also sends via the PHP-back-end, but does the RSA encryption in the browser. This is not necessarily the same value as inferred from $site_row - we provide it as an extra parameter to make it possible to over-ride - e.g. for diagnostics, or where the browser mixed-content model restricts the choices.
	 * @param {Object|String|null} [spinner_where=null] - jQuery object or CSS identifier indicating where, if anywhere, to add a spinner whilst the call is ongoing
	 * @param {ajaxCallback} response_callback - callback that will be called with the results of the AJAX call
	 * @param {Number} [timeout=30] - the number of seconds to allow before the call times out
	 * @param {Boolean} [allow_visual_responses=true] - whether or not it is permissible to display UI elements in response to the results (set this to false if the caller wants to handle it internally only). This is just passed on to process_ajax_response
	 * 
	 * @uses process_ajax_response
	 */
	
	function send_ajax(command, data, $site_row, connection_method, spinner_where, response_callback, timeout, allow_visual_responses) {
		
		connection_method = typeof connection_method !== 'undefined' ? connection_method : 'direct_default_auth';
		timeout = typeof timeout !== 'undefined' ? timeout : 30;
		spinner_where = typeof spinner_where !== 'undefined' ? spinner_where : null;
		allow_visual_responses = ('undefined' === typeof allow_visual_responses) ? true : allow_visual_responses;
		
		// Boil it down to one of 'direct', 'server', 'server_proxies' (i.e. factor out the sub-methods)
		var ajax_method = ('via_mothership' == connection_method || 'via_mothership_encrypting' == connection_method) ? ('via_mothership' == connection_method ? 'server_proxies' : 'server') : 'direct';
		
		var is_site_rpc = (null === $site_row) ? false : true;
		
		if (is_site_rpc) {
			var unlicensed = $site_row.data('site_unlicensed');
			if ('undefined' !== typeof unlicensed && unlicensed) {
				UpdraftCentral.dialog.alert('<h2>'+udclion.error+'</h2>'+udclion.site_unlicensed_message);
				return;
			}
		}
		
		if (spinner_where) {
			// 			$(spinner_where).addClass('updraftcentral_spinner');
			$(spinner_where).prepend('<div class="updraftcentral_spinner"></div>');
		}
		
		if ('direct' == ajax_method && 'https:'== document.location.protocol) {
			var site_url = this.get_contact_url($site_row);
			if (site_url.substring(0, 5).toLowerCase() == 'http:') {
				// Mixed content policy in all mainstream desktop browsers forbids requests to HTTP from HTTPS domains
				ajax_method = 'server';
			}
		}
		
		if (updraftcentral_debug_level > 0) {
			console.log("send_message(ajax_method="+ajax_method+", requested_method="+connection_method+", command="+command+", data(follows))");
			console.log(data);
		}
		
		if ('direct' == ajax_method || 'server_proxies' == ajax_method) {
			
			if (!is_site_rpc) { throw 'send_ajax() called with direct method ('+connection_method+'), but no site row object passed in'; }
			
			var ud_rpc = get_site_udrpc($site_row, connection_method);
			ud_rpc.send_message(command, data, timeout, function(response, code, error_code) {
				
				if (spinner_where) {
					$(spinner_where).removeClass('updraftcentral_spinner');
					$(spinner_where).children('.updraftcentral_spinner').remove();
				}
				
				if (updraftcentral_debug_level > 2) {
					console.log("Raw response, pre-processing, follows");
					console.log(response);
				}
				
				var is_site_rpc_flag = ('server_proxies' == ajax_method) ? 2 : 1;
				
				try {
					process_ajax_response(response, code, error_code, is_site_rpc_flag, response_callback, allow_visual_responses);
				} catch (e) {
					UpdraftCentral.dialog.alert('<h2>'+udclion.error+'</h2>'+udclion.js_exception_occurred+'<br>'+e.toString());
					console.log(e);
				}
			});
				
				
		} else {
			// 'server' == ajax_method
			
			var site_id = 0;
			if (null !== $site_row) {
				site_id = $site_row.data('site_id');
			}
			
			var ajax_subaction = (is_site_rpc) ? 'site_rpc' : command;
			
			var ajax_data = (is_site_rpc) ? { command: command, data: data } : data;
			
			var ajax_options = {
				type: 'POST',
				url: udclion.ajaxurl,
				timeout: timeout * 1000, // In ms
				headers: {
					'X-Secondary-User-Agent': 'UpdraftCentral-dashboard.js/'+udclion.udc_version
				},
				data: {
					action: 'updraftcentral_dashboard_ajax',
					subaction: ajax_subaction,
					component: 'dashboard',
					nonce: udclion.updraftcentral_dashboard_nonce,
					site_id: site_id,
					data: ajax_data
				},
				dataType: 'text',
				success: function(response) {
					
					if (spinner_where) {
						$(spinner_where).children('.updraftcentral_spinner').remove();
						// $(spinner_where).removeClass('updraftcentral_spinner');
					}
					
					if ('undefined' === typeof response || '' === response) {
						console.log("UDRPC: the response from the remote site was empty");
						process_ajax_response(response, 'error', 'response_empty', is_site_rpc, response_callback, allow_visual_responses);
						return;
					}
					
					try {
						var parsed_response = JSON.parse(response);
					} catch(e) {
						
						var valid_json = response.match(/\{"format":.*}/);

						if (null === valid_json) {
							console.log(e);
							console.log(response);
							process_ajax_response(response, 'error', 'json_parse_fail', is_site_rpc, response_callback, allow_visual_responses);
							return;
						} else {
							response = valid_json[0];
							try {
								var parsed_response = JSON.parse(response);
								console.log("UpdraftCentral: successfully parsed JSON after removing unwanted elements");
								console.log(response);
							} catch(e) {
								console.log(e);
								console.log(response);
								process_ajax_response(response, 'error', 'json_parse_fail', is_site_rpc, response_callback, allow_visual_responses);
								return;
							}
						}
						
					}
					
					response = parsed_response;
					
					process_direct_ajax_response(response, is_site_rpc, response_callback, allow_visual_responses);
					
				},
				error: function(request, status, error_thrown) {
					
					if (spinner_where) {
						$(spinner_where).children('.updraftcentral_spinner').remove();
						// $(spinner_where).removeClass('updraftcentral_spinner');
					}
					
					console.error("UpdraftCentral: Error in AJAX operation");
					console.log(request);
					console.log(status);
					// https://api.jquery.com/jquery.ajax/ says: 'When an HTTP error occurs, (this parameter) receives the textual portion of the HTTP status, such as "Not Found" or "Internal Server Error."'
					// "Unauthorized" is what you get when HTTP authentication is required. "Timeout" when there's a timeout.
					console.error(error_thrown);
					
					if ('' == error_thrown) { error_thrown = 'http_post_fail'; }
					
					if (error_thrown.hasOwnProperty('statusText')) {
						error_thrown = error_thrown.statusText.toString();
					}
					
					if ('function' === typeof error_thrown.toLowerCase) {
						error_thrown = error_thrown.toLowerCase();
					} else {
						try {
							var tmp = error_thrown.toString().toLowerCase();
							if (tmp) { error_thrown = tmp; }
						} catch (e) {
						}
					}
					
					process_ajax_response(request, 'error', error_thrown, is_site_rpc, response_callback, allow_visual_responses);
				}
			}
			
			if (updraftcentral_debug_level > 1) {
				console.log("UpdraftCentral: jQuery POST: options follow:");
				console.log(ajax_options);
			}
			
			jQuery.ajax(ajax_options);
			
		}
		
	}

	/**
	 * Set up menu navigation for each site row item. This should be called after any actions that replace the HTML of row items
	 * @returns {void}
	 */
	function setup_menunav() {
		// This is no longer needed.
		//$('#updraftcentral_dashboard .updraft-dropdown-menu').dropit();
		var how_many_sites = $('#updraftcentral_dashboard_existingsites > .updraftcentral_site_row:not(.updraft_site_unlicensed)').length;
		$('#updraftcentral_licences_in_use').html(how_many_sites);
	}

	// Toggle the mobile menu on/off, if at a relevant width
	$('#updraftcentral_dashboard .updraft-mobile-menu').on('click', function() {
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		// Currently only using the width.
		// var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		if(w <= mobile_width) {
			$('#updraftcentral_dashboard .updraft-menu-item-links').slideToggle();
		}
	});
	
	/**
	 * Set the section of the dashboard that displays the existing sites to the specified value. All code that wants to update this section should route through here, so that any other associated operations can be carried out.
	 * 
	 * @param {string} html - the HTML to place within the site list container in the dashboard
	 * @returns {void}
	 */
	function set_existing_sites_to(html) {
		// Reset the connection objects, as the IDs and credentials/options may have changed
		ud_rpcs = [];
		$('#updraftcentral_dashboard_existingsites').html(html);
		// Show/hide the relevant buttons/sections for the current tab
		UpdraftCentral.set_dashboard_mode(true, true);
		setup_menunav();
	}
	
	/*
	 * Used to show or hide the mobile menu on click. Will also toggle when a menu item has been clicked.
	 * Only want this to occur at when the mobile menu is visible to stop the toggle at higher viewports
	 */
	// Toggle the mobile menu on/off, if at a relevant width
	$('#updraftcentral_dashboard .updraft-menu-item-links').on('click', function(event) { /* .updraft-mobile-menu, */
		event.stopPropagation();
		var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

		if(width < mobile_width) {
			$('#updraftcentral_dashboard .updraft-menu-item-links').slideToggle();
		}
	});
		
	/**
	 * Quote the input, so that it is suitable for placing in HTML attributes values
	 * @see https://stackoverflow.com/questions/7753448/how-do-i-escape-quotes-in-html-attribute-values
	 * 
	 * @param {string} s - The string to be quoted
	 * @param {boolean} preserveCR - if true, then \r and \n are replaced with an HTML entity; otherwise with \n
	 * 
	 * @returns {string} the quoted string
	 */
	function quote_attribute(s, preserveCR) {
		preserveCR = preserveCR ? '&#13;' : '\n';
		return ('' + s) /* Forces the conversion to string. */
		.replace(/&/g, '&amp;') /* This MUST be the 1st replacement. */
		.replace(/'/g, '&apos;') /* The 4 other predefined entities, required. */
		.replace(/"/g, '&quot;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		/*
		 *       You may add other replacements here for HTML only 
		 *       (but it's not necessary).
		 *       Or for XML, only if the named entities are defined in its DTD.
		 */ 
		.replace(/\r\n/g, preserveCR) /* Must be before the next replacement. */
		.replace(/[\r\n]/g, preserveCR);
	}
	
	/**
	 * Adds a dashboard notice
	 * 
	 * @param {string} message - The message text to display
	 * @param {string} [level="notice"] - The level for the notice. Can also start with 'listener_', which is styled as if it were 'info' (but can be over-ridden, as it gets its own classes too)
	 * @param {Number|bool} [remove_after=30000] - The number of milliseconds to remove the notice after; or, 0|false to not remove
	 * @param {Object} [extra_data={}] - Extra data to store with the dashboard notice (via data attributes)
	 * 
	 * @returns {Object} the jQuery object for the newly created notice
	 */
	this.add_dashboard_notice = function(message, level, remove_after, extra_data) {
		remove_after = typeof remove_after !== 'undefined' ? remove_after : 30000;
		extra_data = typeof extra_data !== 'undefined' ? extra_data : { };
		level = typeof level !== 'undefined' ? level : 'notice';
		var type = 'notice';
		var extra_classes = '';
		
		if ('listener_' == level.substr(0, 9)) {
			type = 'listener';
			extra_classes = 'updraftcentral_listener updraftcentral_listener_'+level.substr(9);
			extra_data.type = level.substr(9);
			level = 'info';
		}
		
		$container = $('#updraftcentral_notice_container');
		
		var newnotice_container_opener = '<div class="updraftcentral_notice updraftcentral_notice_new updraftcentral_notice_level_'+level+' '+extra_classes+'"';
		$.each(extra_data, function(key, val) {
			newnotice_container_opener += 'data-'+key+'="'+quote_attribute(val)+'"';
		});
		
		var $newnotice = $(newnotice_container_opener+'><button type="button" class="updraftcentral_notice_dismiss"></button><div class="updraftcentral_notice_contents">'+message+'</div></div>');
		$container.append($newnotice);
		if (remove_after) {
			$newnotice.slideDown('medium').delay(30000).slideUp('slow', function() {
				$(this).remove();
			});
		} else {
			$newnotice.slideDown('medium');
		}
		
		return $newnotice;
	}
	
	/**
	 * Creates a special type of dashboard notice which polls for status updates
	 * @see register_listener_processor
	 * 
	 * @param {string} type - Listener type (an identifying string) (not shown; stored and used for CSS classes)
	 * @param {Object} $site_row - a jQuery object identifying the site row that the listener is associated with
	 * @param {string} message - HTML to be placed in the dashboard notice
	 * @param {*} [data={}] - Data associated with the listener (which will be stored in an HTML data attribute)
	 * @param {string} [title] - HTML to be used as the notice title. If not specified, a default will be used.
	 * @returns {Object} the jQuery object for the newly created notice
	 * 
	 */
	this.create_dashboard_listener = function(type, $site_row, message, data, title) {
		data = ('undefined' === typeof data) ? {} : data;
		data.site_url = $site_row.data('site_url');
		data.site_id = $site_row.data('site_id');
		var listener_title = (typeof title === 'undefined') ? '<h2>'+$site_row.data('site_description')+'</h2>' : title;
		return this.add_dashboard_notice(listener_title+message, 'listener_'+type, false, data);
	}
	
	// Only trigger a removal if the close button is directly in the notice. This allows other sub-elements to re-use the style class.
	$('#updraftcentral_notice_container').on('click', '.updraftcentral_notice > .updraftcentral_notice_dismiss', function() {
		$(this).parents('.updraftcentral_notice').clearQueue().slideUp('slow', function() { $(this).remove(); });
	});
	
	/**
	 * Get the current dashboard mode
	 * 
	 * @returns {string} - the current dashboard mode
	 */
	this.get_dashboard_mode = function() {
		return $('#updraftcentral_dashboard').data('updraftcentral_mode');
	}
	
	/**
	 * Set up the dashboard, by hiding things that don't belong in the currently active tab
	 * 
	 * @param {string|boolean} new_mode=true - the mode to switch to. These correspond to keys for items placed in the main menu via the updraftcentral_main_navigation_items filter. If set to true, then it will choose the current mode (only useful if setting force to true).
	 * @param {boolean} [force=false] - run the commands to set up the mode, even if it appears to be the current mode (useful for resetting the state within the mode)
	 * @returns {void}
	 */
	this.set_dashboard_mode = function (new_mode, force) {
		
		force = ('undefined' === typeof force) ? false : true;
		
		var current_mode = this.get_dashboard_mode();

		if (true === new_mode) { new_mode = current_mode; }
		
		if (!force && new_mode == current_mode) { return; }
		
		if (current_mode) { $('#updraftcentral_dashboard').removeClass('updraftcentral_mode_'+current_mode); }

		$('#updraftcentral_dashboard_existingsites_container .updraftcentral_row_extracontents').empty();
		
		// Show all sites again
		$('#updraftcentral_dashboard_existingsites > .updraftcentral_site_row, #updraftcentral_dashboard_existingsites > .updraftcentral_row_divider').show();
		
		$('#updraftcentral_dashboard').data('updraftcentral_mode', new_mode);
		$('#updraftcentral_dashboard').addClass('updraftcentral_mode_'+new_mode);
		$('#updraft-menu-item-'+current_mode).removeClass('updraft-menu-item-links-active');
		$('#updraft-menu-item-'+new_mode).addClass('updraft-menu-item-links-active');
		
		// Since there exist classes for both "show everywhere except <here>" and "hide everywhere except here", you could, of course, add CSS classes that result in contradictory instructions. The outcome of doing so is not defined.
		$('#updraftcentral_dashboard .updraftcentral-hide-in-other-tabs:not(.updraftcentral-show-in-tab-'+new_mode+'), #updraftcentral_dashboard .updraftcentral-hide-in-tab-'+new_mode).hide();
		$('#updraftcentral_dashboard .updraftcentral-show-in-tab-'+new_mode+' .updraftcentral-hide-in-tab-initially').hide();
		$('#updraftcentral_dashboard .updraftcentral-show-in-tab-'+new_mode+', #updraftcentral_dashboard .updraftcentral-show-in-other-tabs:not(.updraftcentral-hide-in-tab-'+new_mode+')').slideDown(1);

		deregister_row_clickers();
		deregister_modal_listeners();
		
		$('#updraftcentral_dashboard_existingsites').trigger('updraftcentral_dashboard_mode_set', { new_mode: new_mode, previous_mode: current_mode } );
		$('#updraftcentral_dashboard_existingsites').trigger('updraftcentral_dashboard_mode_set_'+new_mode, { new_mode: new_mode, previous_mode: current_mode } );
		
	}
	
	$('.updraftcentral_mode_actions .updraftcentral_action_choose_another_site').click(function() {
		UpdraftCentral.set_dashboard_mode(true, true);
	});
	
	$('#updraft-central-navigation .updraft-menu-item').click(function() {
		var item_dom_id = $(this).attr('id');
		if ('undefined' === typeof item_dom_id) { return; }
		if ('updraft-menu-item-' != item_dom_id.substring(0, 18)) {
			console.log("UDCentral: menu item without the ID in the expected format");
			console.log(this);
			return;
		}
		
		var new_mode = item_dom_id.substring(18);
		UpdraftCentral.set_dashboard_mode(new_mode);
			
	});
	
	$('#updraftcentral_dashboard .updraftcentral_action_box .updraftcentral_action_manage_sites').click(function() {
		UpdraftCentral.set_dashboard_mode('sites');
	});
	
	/**
	 * Stores persistent data in the browser, using the HTML5 local storage API. Uses a fixed prefix of 'updraftcentral_' to avoid clashing with other applications.
	 * 
	 * We are abstracting this to allow not only for expiring data, but for other possible future enhancements; e.g. an option to duplicate some items persiently in the database. For now we are keeping our options open.
	 * 
	 * @param {string} key - storage key
	 * @param {*} data - data to store; must be data than can be turned into JSON
	 * @param {boolean} [can_expire=false] - whether the time of updating the data should be stored (to allow assessing its age when retrieving it). Note that this should always be used consistently (either always on, or always off) with any particular key - otherwise, its results can be out of date.
	 * @returns {void}
	 */
	this.storage_set = function(key, data, can_expire) {
		if ('undefined' !== typeof can_expire && can_expire) {
			var epoch_time = Math.floor(Date.now() / 1000);
			localStorage.setItem('updraftcentral_saved_at_'+key, epoch_time);
		}
		if (updraftcentral_debug_level > 1) {
			console.log("UpdraftCentral.storage_set(key="+key+")");
		}
		localStorage.setItem('updraftcentral_'+key, JSON.stringify(data));
	}
	
	/**
	 * Reverses serialisation that was performed using jQuery's .serialize() method
	 * From: https://gist.github.com/brucekirkpatrick/7026682
	 * 
	 * @param {string} serialized_string - the string to unserialize
	 * 
	 * @returns {Object} - the resulting object
	 */
	this.unserialize = function(serialized_string) {
		var str = decodeURI(serialized_string); 
		var pairs = str.split('&');
		var obj = {}, p, idx;
		for (var i=0, n=pairs.length; i < n; i++) {
			p = pairs[i].split('=');
			idx = p[0]; 
			if (obj[idx] === undefined) {
				obj[idx] = unescape(p[1]);
			}else{
				if (typeof obj[idx] == "string") {
					obj[idx]=[obj[idx]];
				}
				obj[idx].push(unescape(p[1]));
			}
		}
		return obj;
	}
	
	/**
	 * Retrieves stored data from the browser, using the HTML5 local storage API. Uses a fixed prefix of 'updraftcentral_' to avoid clashing with other applications.
	 * 
	 * @param {string} key - storage key
	 * @param {Number|Boolean} [maximum_age=false] - if set to a strictly positive numerical value, then only return the data if it was stored within the indicated number of seconds.
	 * @returns {*} - stored data. Returns null if the age check fails. The result for never-stored data is undefined.
	 * 
	 */
	this.storage_get = function(key, maximum_age) {
		if ('undefined' !== typeof maximum_age && maximum_age > 0) {
			var stored_at = localStorage.getItem('updraftcentral_saved_at_'+key);
			if (!stored_at) { return null; }
			var epoch_time = Math.floor(Date.now() / 1000);
			var stored_ago = epoch_time - stored_at;
			if (updraftcentral_debug_level > 1) {
				console.log("UpdraftCentral.storage_get(key="+key+", maximum_age="+maximum_age+"): stored_at="+stored_at+", epoch_time="+epoch_time+", stored_ago="+stored_ago);
			}
			
			if (stored_ago > maximum_age) { return null; }
		}
		var item = localStorage.getItem('updraftcentral_'+key);
		if ('undefined' === typeof item) { return null; }
		try {
			var parsed = JSON.parse(item);
			return parsed;
		} catch (e) {
		}
		return null;
	}
	
	/**
	 * Retrieves stored data from the browser, using the HTML5 local storage API. Uses a fixed prefix of 'updraftcentral_' to avoid clashing with other applications.
	 * 
	 * @param {string} key - storage key
	 * @returns {string} the stored value
	 */
	this.storage_remove = function(key) {
		localStorage.removeItem('updraftcentral_saved_at_'+key);
		return localStorage.removeItem('updraftcentral_'+key);
	}
	
	/**
	 * Do any processing necessary with the passed information about current status
	 * 
	 * @param {Object} status_info - any recognised properties will be processed
	 * @returns {void}
	 */
	function process_sites_status_info(status_info) {
		if (status_info.hasOwnProperty('how_many_licences_in_use')) {
			$('.updraftcentral_licences_in_use').html(status_info.how_many_licences_in_use);
		}
		if (status_info.hasOwnProperty('how_many_licences_available')) {
			var display = (status_info.how_many_licences_available < 0) ? '&#8734;' : status_info.how_many_licences_available;
			$('.updraftcentral_licences_total').html(display);
		}
	}
	
	$('#updraftcentral_dashboard_newsite').click(function() {
		
		var advanced_site_options_html = UpdraftCentral.get_advanced_site_options_html({ http_username: '', http_password: ''});
		
		UpdraftCentral.open_modal(udclion.add_site, UpdraftCentral.template_replace('sites-add-new-modal', { advanced_options: advanced_site_options_html } ), function() {
			
			var key = $('#updraftcentral_addsite_key').val();
			UpdraftCentral.close_modal();
			
			if ('undefined' === typeof key || key === null || key === '') { return; }

			var extra_site_info = UpdraftCentral.get_serialized_options('#updraftcentral_modal #updraftcentral_editsite_expertoptions .expert_option');
			var send_cors_headers =  $('#updraftcentral_modal #updraftcentral_site_send_cors_headers').is(':checked') ? 1 : 0;
			var connection_method = $('#updraftcentral_modal #updraftcentral_site_connection_method').val();
			
			send_ajax('newsite', { key: key, extra_site_info: extra_site_info, send_cors_headers: send_cors_headers, connection_method: connection_method }, null, 'via_mothership_encrypting', '#updraftcentral_dashboard_existingsites', function(resp, code, error_code) {
				
				if ('ok' == code) {
					
					if (resp.hasOwnProperty('message')) {
						add_dashboard_notice(resp.message, 'info');
						if (resp.hasOwnProperty('sites_html')) {
							set_existing_sites_to(resp.sites_html);
						} else {
							console.log("Expected sites_html data not found:");
							console.log(resp);
						}
						if (resp.hasOwnProperty('status_info')) { process_sites_status_info(resp.status_info); }
					}

					if (resp.hasOwnProperty('key_needs_sending')) {
						
						var site_id = resp.key_needs_sending.key_site_id;
						var site_ajax_url = resp.key_needs_sending.url;
						var $site_row = $('#updraftcentral_dashboard_existingsites .updraftcentral_site_row[data-site_id="'+site_id+'"');
						var site_remote_public_key = resp.key_needs_sending.remote_public_key;
						
						$($site_row).prepend('<div class="updraftcentral_spinner"></div>');
						
						var send_key_url = site_ajax_url+'&action=updraftcentral_receivepublickey&updraft_key_index='+encodeURIComponent(resp.key_needs_sending.updraft_key_index)+'&public_key='+encodeURIComponent(UpdraftCentral.base64_encode(site_remote_public_key));
						var win = window.open(send_key_url, '_blank', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=320');
						win.focus();
						return;
						
						// This requires CORS code on the receiving side
						/*
						$.post(site_ajax_url, {
							action: 'updraftcentral_receivepublickey',
							public_key: site_remote_public_key,
							updraft_key_index: resp.key_needs_sending.updraft_key_index
						}, function(response) {
							if (spinner_where) {
								$($site_row).children('.updraftcentral_spinner').remove();
							}
							// Obsolete - parse_response() no longer exists
							var resp = parse_response(response);
							if (false !== resp) {
								if (resp.code == 'ok') {
									console.log("UDCentral: successfully sent public key to remote site");
								} else {
									console.log("UDCentral: failed to send public key to remote site");
									console.log(resp);
									if (resp.code == 'already_have' || resp.code == 'already_have') {
										add_dashboard_notice(udclion[resp.code], 'error');
									} else {
										add_dashboard_notice(udclion.unknown_response, 'error');
									}
								}
							}
							
						});
						*/
							
					}
				}
			});
		}, udclion.add_site, function() {
			$('#updraftcentral_modal #updraftcentral_site_send_cors_headers').prop('checked', true);
// 			$('#updraftcentral_modal #updraftcentral_addsite_key').focus();
		} , false);
	});
	
	// Register the modal events which are active in the 'Sites' tab
	$('#updraftcentral_dashboard_existingsites').on('updraftcentral_dashboard_mode_set_sites', function(e) {
		
		register_modal_listener('#updraftcentral_addsite_expertoptions_show', function(e) {
			$(this).slideUp();
			$('#updraftcentral_modal #updraftcentral_editsite_expertoptions .initially-hidden').show();
			e.preventDefault();
		});
		
	});
	
	// Register the row clickers and modal listeners which are active in every tab
	$('#updraftcentral_dashboard_existingsites').on('updraftcentral_dashboard_mode_set', function(event, data) {
		
		// Use a new browser portal for any clicks to updraftplus.com
		register_modal_listener('a', function(e) {
			var href = $(this).attr('href');
			if ('undefined' === typeof href) { return; }
			if (href.substring(0, 23) == 'https://updraftplus.com' || href.substring(0, 23) == 'http://updraftplus.com') {
				e.preventDefault();
				var win = window.open(href, '_blank');
				win.focus();
			}
		});
		
		register_modal_listener('#updraft_debug_empty_browser_cache', function(e) {
			
			var how_many = 0;
			var verbose = (updraftcentral_debug_level > 0) ? true : false;
			
			for ( var i = localStorage.length; i >= 0; --i ) {
				var key = localStorage.key(i);
				if (key !== null && key.substr(0, 15) == 'updraftcentral_') {
					if (verbose) { console.log("UpdraftCentral: Removing key from local storage: "+key); }
					localStorage.removeItem(key);
					how_many++;
				}
			}
			if (how_many > 0) {
				UpdraftCentral.dialog.alert('<h2>'+udclion.empty+' '+udclion.browser_cache+'</h2>'+sprintf(udclion.cache_emptied, how_many));
			} else {
				UpdraftCentral.dialog.alert('<h2>'+udclion.empty+' '+udclion.browser_cache+'</h2>'+udclion.cache_no_contents);
			}
		});
		
		register_modal_listener('#updraft_debug_show_browser_cache', function(e) {
			var how_many = 0;
			for ( var i = 0, len = localStorage.length; i < len; ++i ) {
				var key = localStorage.key(i);
				var value = localStorage.getItem( key );
				if (key.substr(0, 15) == 'updraftcentral_') {
					how_many++;
					console.log(key+": "+value);
				}
			}
			if (how_many > 0) {
				UpdraftCentral.dialog.alert('<h2>'+udclion.log_contents+'</h2>'+udclion.cache_contents_logged);
			} else {
				UpdraftCentral.dialog.alert('<h2>'+udclion.log_contents+'</h2>'+udclion.cache_no_contents);
			}
		});
		
		// The 'future' tab has no sites rows visible
		if (data && data.hasOwnProperty('new_mode') && data.new_mode == 'future') { return; }
		
		register_modal_listener('.updraftcentral_site_editdescription', function(e) {
			e.preventDefault();
			open_site_configuration(UpdraftCentral.$site_row);
		});
		
		register_modal_listener('.updraftcentral_test_other_connection_methods', function(e) {
			e.preventDefault();
			open_connection_test(UpdraftCentral.$site_row);
		});
		
		register_modal_listener('a.connection-test-switch', function(e) {
			e.preventDefault();
			var connection_method = $(this).data('connection_method');
			
			UpdraftCentral.close_modal();
			
			var site_id = $(this).data('site_id');
			
			send_ajax('edit_site_connection_method', { site_id: site_id, connection_method: connection_method }, null, 'via_mothership_encrypting', '#updraftcentral_dashboard_existingsites', function(resp, code, error_code) {
				
				if ('ok' == code) {
					
					if (resp.hasOwnProperty('message')) { add_dashboard_notice(resp.message); }
					
					if (resp.hasOwnProperty('sites_html')) {
						set_existing_sites_to(resp.sites_html);
						setup_menunav();
					} else {
						console.log(resp);
						add_dashboard_notice(udclion.unknown_response, 'error');
					}
					if (resp.hasOwnProperty('status_info')) { process_sites_status_info(resp.status_info); }
				}
			});
		});
		
		register_modal_listener('.updraftcentral_siteinfo_results .phpinfo', function(e) {
			e.preventDefault();
			UpdraftCentral.send_site_rpc('core.phpinfo', null, UpdraftCentral.$site_row, function(response, code, error_code) {
				
				if ('ok' == code && response.data) {
					var output = '';
					$.each(response.data, function(name, section) {
						output += "<h3>"+name+"</h3>\n"+'<table>'+"\n";
						$.each(section, function(key, val) {
							if (val.constructor === Array) {
								output += "<tr><td>"+key+"</td><td>"+val[0]+"</td><td>"+val[1]+"</td></tr>\n";
							} else if (typeof val === 'string') {
								if ($.isNumeric(key)) {
									output += "<tr><td></td><td>"+val+"</td></tr>\n";
								} else {
									output += "<tr><td>"+key+"</td><td>"+val+"</td></tr>\n";
								}
							} else {
								console.log("UpdraftCentral: phpinfo: Unrecognised output for key "+key+" (follows)");
								console.log(val);
							}
						});
						output += "</table>\n";
					});
					
					// N.B. open_modal() by default sanitizes the body data
					UpdraftCentral.open_modal(udclion.phpinfo, '<div id="updraftcentral_phpinfo_results">'+output+'</div>', null, false, null, true, 'modal-lg');
				}
			}, $(this));
		});
		
		register_modal_listener('#updraftcentral_site_connection_method', function() {
			var site_connection_method = $('#updraftcentral_site_connection_method').val();
			
			if (null == site_connection_method) { return; }
			
			if (site_connection_method.substring(0, 7) == 'direct_' && 'https:' == document.location.protocol) {
				$('#updraftcentral_site_connection_method_message').show().html(udclion.http_must_go_via_mothership);
			} else {
				$('#updraftcentral_site_connection_method_message').hide();
			}
		}, 'change');
		
		register_row_clicker('.updraftcentral_site_adddescription', function($site_row) {
			open_site_configuration($site_row);
		});
		
		register_row_clicker('.updraftcentral_site_delete', function($site_row) {
			UpdraftCentral.dialog.confirm('<h2>'+udclion.remove_site+'</h2><p>'+$site_row.data('site_url')+'</p><p>'+udclion.really_delete_site+'</p>', function(result) {
				if (!result) return;
				var site_id = UpdraftCentral.$site_row.data('site_id');
				if (!site_id) { return; }
				$site_row.slideUp('slow');
				
				send_ajax('delete_site', { site_id: site_id }, null, 'via_mothership_encrypting', '#updraftcentral_dashboard_existingsites', function(resp, code, error_code) {
					if ('ok' == code) {
						if (resp.hasOwnProperty('message')) {
							add_dashboard_notice(resp.message);
						}
						if (resp.hasOwnProperty('sites_html')) {
							set_existing_sites_to(resp.sites_html);
						} else {
							console.log(resp);
							add_dashboard_notice(udclion.unknown_response, 'error');
						}
						if (resp.hasOwnProperty('status_info')) { process_sites_status_info(resp.status_info); }
					}
				});
				
			});
		});
		
		register_row_clicker('.row_siteinfo', function($site_row) {
			UpdraftCentral.send_site_rpc('core.site_info', null, $site_row, function(response, code, error_code) {
				if (updraftcentral_debug_level > 1) {
					console.log("send_site_rpc(site_info): parsed response follows");
					console.log(response);
				}
				if ('ok' == code) {
					if (false !== response) {
						var versions = response.data.versions;
						var bloginfo = response.data.bloginfo;
						var url = UpdraftCentral.sanitize_html(bloginfo.url);
						var name = UpdraftCentral.sanitize_html(bloginfo.name);
						// 'This site is running WordPress version %s (PHP %s, MySQL %s) and UpdraftPlus version %s (UDRPC version %s)'
						// 					var message = sprintf(udclion.what_remote_running, UpdraftCentral.sanitize_html(versions.wp), UpdraftCentral.sanitize_html(versions.php), UpdraftCentral.sanitize_html(versions.mysql), UpdraftCentral.sanitize_html(versions.ud), UpdraftCentral.sanitize_html(versions.udrpc_php));
						// add_dashboard_notice(message, 'info');
						// N.B. By default, open_modal() sanitizes the body.
						var ud_version = versions.ud;
						if ('none' == ud_version) { ud_version = udclion.updraftplus.version_none; }
						var message = sprintf(udclion.what_remote_running, versions.wp, versions.php, versions.mysql, ud_version, versions.udrpc_php);
						UpdraftCentral.open_modal(
							UpdraftCentral.sanitize_html(bloginfo.name),
							UpdraftCentral.template_replace('dashboard-siteinfo', { url: url, message: message, phpinfo: udclion.phpinfo }),
							null,
							false
						);
					} 
				}
			});
		});
		
		register_row_clicker('.updraftcentral_site_dashboard', function($site_row) {
			UpdraftCentral.open_browser_at($site_row);
		});		
		
	});
	
	/**
	 * Gets the HTML fragment for advanced site editing options
	 * 
	 * @param {Object} values - the values to pass to the template
	 * 
	 * @returns {string} - the HTML
	 */
	this.get_advanced_site_options_html = function(values) {
		return UpdraftCentral.template_replace('sites-advanced-site-options', values);
	}
	
	/**
	 * Get serialized options within a specified selector. Includes making sure that checkboxes are included when not checked.
	 * 
	 * @param {string} selector - the jQuery selector to use to locate the options
	 * 
	 * @returns {string} - the serialized options
	 */
	this.get_serialized_options = function(selector) {
		var form_data = $(selector).serialize();
		$.each($(selector+' input[type=checkbox]')
		.filter(function(idx){
			return $(this).prop('checked') == false
		}),
		function(idx, el){
			//attach matched element names to the form_data with chosen value.
			var empty_val = '0';
			form_data += '&' + $(el).attr('name') + '=' + empty_val;
		});
		return form_data;
	}
	
	/**
	 * Opens the site connection test dialog for the specified site
	 * 
	 * @param {Object} $site_row - the jQuery row object for the site whose configuration is to be edited
	 * @returns {void}
	 */
	this.open_connection_test = function($site_row) {
		
		var site_url = UpdraftCentral.get_contact_url($site_row);
		var site_id = $site_row.data('site_id');
		
		var current_connection_method = $site_row.data('connection_method');
		
		if ('via_mothership_encrypting' == current_connection_method) {
			UpdraftCentral.dialog.alert('<h2>'+udclion.test_connection_methods+'</h2><p>'+udclion.test_not_possible_in_current_mode+'</p>');
			// <p><a href="#" class="updraftcentral_site_editdescription">'+udclion.open_site_configuration+'...</a></p>
			return;
		}
		
		var current_method_simplified = ('direct_jquery_auth' == current_connection_method || 'direct_default_auth' == current_connection_method || 'direct_default_auth' == current_connection_method) ? 'direct' : current_connection_method;
		
		UpdraftCentral.open_modal(udclion.test_connection_methods, UpdraftCentral.template_replace('sites-connection-test', { site_url: site_url }), true, false, function() {
			
			var direct_method_can_be_attempted = true;
			if ('https:'== document.location.protocol) {
				if (site_url.substring(0, 5).toLowerCase() == 'http:') {
					direct_method_can_be_attempted = false;
				}
			}
			
			if (direct_method_can_be_attempted) {
			
				$('#updraftcentral_modal .connection-test-direct .connection-test-result').html('');
				
				send_ajax('ping', null, $site_row, 'direct_default_auth', '#updraftcentral_modal .connection-test-direct .connection-test-result', function(response, code, error_code) {
					if (updraftcentral_debug_level > 0) {
						console.log("Result follows for 'direct_default_auth' method:");
						console.log(response);
					}
					if ('ok' == code) {
						var new_html = '<span class="connection-test-succeeded">'+udclion.succeeded+'</span> ';
						if ('direct' == current_method_simplified) {
							new_html += udclion.current_method+' '+udclion.best_method+' '+udclion.recommend_keep;
						} else {
							new_html += udclion.best_method+' '+udclion.recommend_use+' <a href="#" class="connection-test-switch" data-site_id="'+site_id+'" data-connection_method="direct_default_auth">'+udclion.switch_to+'...</a>';
						}
						$('#updraftcentral_modal .connection-test-direct .connection-test-result').html(new_html);
					} else {
						$('#updraftcentral_modal .connection-test-direct .connection-test-result').html('<span class="connection-test-failed">'+udclion.failed+' ('+error_code+')</span>');
					}
				}, 30, false);
			
			} else {
				$('#updraftcentral_modal .connection-test-direct .connection-test-result').html(udclion.not_possible_browser_restrictions);
			}
			
			$('#updraftcentral_modal .connection-test-via_mothership .connection-test-result').html('');
			send_ajax('ping', null, $site_row, 'via_mothership', '#updraftcentral_modal .connection-test-via_mothership .connection-test-result', function(response, code, error_code) {
				$('#updraftcentral_modal .connection-test-via_mothership .connection-test-result').html(code);
				if (updraftcentral_debug_level > 0) {
					console.log("Result follows for 'via_mothership' method:");
					console.log(response);
				}
				if ('ok' == code) {
					var new_html = '<span class="connection-test-succeeded">'+udclion.succeeded+'</span> ';
					if ('via_mothership' != current_connection_method) {
						new_html += '<a href="#" class="connection-test-switch" data-site_id="'+site_id+'" data-connection_method="via_mothership">'+udclion.switch_to+'...</a>';
					} else {
						new_html += udclion.current_method;
					}
					$('#updraftcentral_modal .connection-test-via_mothership .connection-test-result').html(new_html);
				} else {
					
					var code_msg = error_code;
					if ('unexpected_http_code' == error_code) {
						if (null != response && response.hasOwnProperty('data') && null != response.data && response.data.hasOwnProperty('response') && response.data.response.hasOwnProperty('code')) {
							code_msg += ' - '+response.data.response.code;
						}
						if (null != response && response.hasOwnProperty('data') && null != response.data && response.data.hasOwnProperty('response') && response.data.response.hasOwnProperty('message')) {
							code_msg += ' - '+response.data.response.message;
						}
					}
					
					$('#updraftcentral_modal .connection-test-via_mothership .connection-test-result').html('<span class="connection-test-failed">'+udclion.failed+' ('+code_msg+')</span>');
				}
			}, 30, false);
			
			$('#updraftcentral_modal .connection-test-via_mothership_encrypting .connection-test-result').html('');
			send_ajax('ping', null, $site_row, 'via_mothership_encrypting', '#updraftcentral_modal .connection-test-via_mothership_encrypting .connection-test-result', function(response, code, error_code) {
				if (updraftcentral_debug_level > 0) {
					console.log("Result follows for 'via_mothership_encrypting' method:");
					console.log(response);
				}
				if ('ok' == code) {
					var new_html = '<span class="connection-test-succeeded">'+udclion.succeeded+'</span> ';
					if ('via_mothership_encrypting' != current_connection_method) {
						new_html += '<a href="#" class="connection-test-switch" data-site_id="'+site_id+'" data-connection_method="via_mothership_encrypting">'+udclion.switch_to+'...</a>';
					} else {
						new_html += udclion.current_method;
					}
					$('#updraftcentral_modal .connection-test-via_mothership_encrypting .connection-test-result').html(new_html);
				} else {
					
					var code_msg = error_code;
					if ('unexpected_http_code' == error_code) {
						if (null != response && response.hasOwnProperty('data') && null != response.data && response.data.hasOwnProperty('response') && response.data.response.hasOwnProperty('code')) {
							code_msg += ' - '+response.data.response.code;
						}
						if (null != response && response.hasOwnProperty('data') && null != response.data && response.data.hasOwnProperty('response') && response.data.response.hasOwnProperty('message')) {
							code_msg += ' - '+response.data.response.message;
						}
					}
					
					$('#updraftcentral_modal .connection-test-via_mothership_encrypting .connection-test-result').html('<span class="connection-test-failed">'+udclion.failed+' ('+code_msg+')</span>');
				}
			}, 30, false);
			
		}, true, 'modal-lg');
	}
	
	/**
	 * Opens the site configuration dialog for the specified site
	 * 
	 * @param {Object} $site_row - the jQuery row object for the site whose configuration is to be edited
	 * @returns {void}
	 */
	this.open_site_configuration = function($site_row) {

		var site_url = $site_row.data('site_url');
		
		var http_username = $site_row.data('http_username');
		if ('undefined' === typeof http_username) { http_username = ''; }
		
		var http_password = $site_row.data('http_password');
		if ('undefined' === typeof http_password) { http_password = ''; }
		
		var connection_method = $site_row.data('connection_method');
		if ('undefined' === typeof connection_method) { connection_method = 'direct_default_auth'; }
		
		var http_authentication_method = $site_row.data('http_authentication_method');
		if ('undefined' === typeof http_authentication_method) { http_authentication_method = 'basic'; }
		
		var existing_description = $site_row.data('site_description');
		if (existing_description == site_url) { existing_description = ''; }
		
		var send_cors_headers = $site_row.data('send_cors_headers');
		if ('undefined' === typeof send_cors_headers || send_cors_headers) { send_cors_headers = 1; }
		
		var advanced_site_options_html = UpdraftCentral.get_advanced_site_options_html({http_username: http_username, http_password: http_password});

		UpdraftCentral.open_modal(udclion.edit_site_configuration, UpdraftCentral.template_replace('sites-edit-configuration', { site_url: site_url, advanced_options: advanced_site_options_html }, { existing_description: existing_description } ), function() {
			
			var description = $('#updraftcentral-edit-site-description').val();
			
			var send_cors_headers = $('#updraftcentral_modal #updraftcentral_site_send_cors_headers').is(':checked') ? 1 : 0;

			var connection_method = $('#updraftcentral_modal #updraftcentral_site_connection_method').val();
			
			var site_id = $site_row.data('site_id');
			if (!site_id) { return; }
			
			UpdraftCentral.close_modal();
			
			var extra_site_info = UpdraftCentral.get_serialized_options('#updraftcentral_modal .expert_option');
			
			send_ajax('edit_site_configuration', { site_id: site_id, description: description, extra_site_info: extra_site_info, send_cors_headers: send_cors_headers, connection_method: connection_method }, null, 'via_mothership_encrypting', '#updraftcentral_dashboard_existingsites', function(resp, code, error_code) {

				if ('ok' == code) {
					
					if (resp.hasOwnProperty('message')) { add_dashboard_notice(resp.message); }
					
					if (resp.hasOwnProperty('sites_html')) {
						set_existing_sites_to(resp.sites_html);
						setup_menunav();
					} else {
						console.log(resp);
						add_dashboard_notice(udclion.unknown_response, 'error');
					}
					if (resp.hasOwnProperty('status_info')) { process_sites_status_info(resp.status_info); }
				}
			});
			
		}, udclion.edit, function() {
			$('#updraftcentral_modal #updraftcentral_site_connection_method').val(connection_method).change();
			if (send_cors_headers) { $('#updraftcentral_modal #updraftcentral_site_send_cors_headers').prop('checked', true); }
			$('#updraftcentral_modal #updraftcentral_addsite_http_authentication_method').val(http_authentication_method);
		}, false);
	}
	
	/**
	 * @callable RPCCallback
	 * @param {Object} response - the data returned by the RPC call. The format of this object depends upon both code and (if code is not 'ok') on error_code, and so should not be processed before those variables have been inspected.
	 * @param {string} code - the code returned by the RPC call; currently possible values are 'ok' or 'error'
	 * @param {string|null} error_code - the error code returned by the RPC call (if any).
	 *
	 * @returns {*} - if true, then in the case of an error (code is 'error'), then no further action will be taken; otherwise, default actions (e.g. displaying an error) will be taken
	 */
	
	/**
	 * Send a command to the remote site. This is a very thin wrapper around send_ajax.
	 * 
	 * @uses send_ajax
	 *
	 * @param {string} rpc_command - the command to send
	 * @param {*} data - the data to send with the command
	 * @param {Object} $site_row - the jQuery object for the row of the site that the request is being sent to
	 * @param {RPCCallback} callback - function to call with the results
	 * @param {Object|null|false} spinner_where - jQuery object indicating where any spinner should be shown
	 * @param {number} [timeout=30] - the number of seconds for the timeout on the HTTP call
	 * @returns {void}
	*/
	this.send_site_rpc = function(rpc_command, data, $site_row, callback, spinner_where, timeout) {
		timeout = 'undefined' !== typeof timeout ? timeout : 30;
		var site_id = $site_row.data('site_id');
		if (!site_id) {
			console.log("UpdraftCentral: sent_site_rpc("+rpc_command+") command sent, but site ID could not be identified from the row (follows)");
			console.log($site_row);
		}

		var connection_method = $site_row.data('connection_method');
		
		if ('undefined' === typeof spinner_where || null === spinner_where) { spinner_where = $site_row; }
		
		try {
			return send_ajax(rpc_command, data, $site_row, connection_method, spinner_where, callback, timeout);
		} catch (e) {
			if (spinner_where) {
				$(spinner_where).children('.updraftcentral_spinner').remove();
			}
			//add_dashboard_notice(udclion.js_exception_occurred+'<br>'+e.toString(), 'error');
			UpdraftCentral.dialog.alert('<h2>'+udclion.error+'</h2>'+udclion.js_exception_occurred+'<br>'+e.toString());
			console.log(e);
		}
	}

	/**
	 * Opens a new browser portal at the specified URL
	 * @param {Object} $site_row - jQuery object for the site row
	 * @param {Object|string|null} [redirect_to=null] - where to redirect to (defaults to the network admin)
	 * @param {Object} [spinner_where=$site_row] - jQuery object indicating where to put the site row.
	 * @returns {void}
	*/
	this.open_browser_at = function($site_row, redirect_to, spinner_where) {
		redirect_to = typeof redirect_to !== 'undefined' ? redirect_to : null;
		spinner_where = ('undefined' === typeof spinner_where) ? $site_row : spinner_where;
		UpdraftCentral.send_site_rpc('core.get_login_url', redirect_to, $site_row, function(response, code, error_code) {
			if ('ok' == code && false !== response && response.hasOwnProperty('data')) {
				var login_url = response.data.login_url;
				var win = window.open(login_url, '_blank');
				if ('undefined' != typeof win) {
					win.focus();
				} else {
					UpdraftCentral.dialog.alert('<h2>'+udclion.open_new_window+'</h2>'+udclion.window_may_be_blocked);
				}
			}
		}, spinner_where);
	}
	
	/**
	 * Toggle whether or not UpdraftCentral is in "full screen" mode
	 * @returns {void}
	 */
	this.toggle_fullscreen = function() {
		// https://github.com/private-face/jquery.fullscreen
		if ($.fullscreen.isFullScreen()) {
			$('footer').show();
			$.fullscreen.exit();
			$('#updraftcentral_modal_dialog').appendTo(document.body);
		} else {
			$('footer').hide();
			$('#updraftcentral_dashboard').fullscreen({overflow: 'scroll', toggleClass: 'updraft-fullscreen' });
			$('#updraftcentral_modal_dialog').appendTo('#updraftcentral_dashboard');
		}
	}
	
	$('#updraftcentral_dashboard .updraft-central-logo img').dblclick(function() { UpdraftCentral.toggle_fullscreen(); });
	
	$('#updraft-central-navigation .updraft-full-screen').on('click', function() { UpdraftCentral.toggle_fullscreen(); });

	$('#updraft-central-navigation .updraftcentral-help').on('click', function() {
		UpdraftCentral.dialog.alert(UpdraftCentral.template_replace('dashboard-help', { uc_version: udclion.updraftcentral_version+': '+udclion.udc_version, running_on: UpdraftCentral.version_info_as_text() }));
	});

	/**
	 * Return a string with information on the current installation
	 * @returns {string} information on the current installation
	 */
	this.version_info_as_text = function() {
		return 'WP/'+udclion.wp_version+' PHP/'+udclion.php_version+' MySQL/'+udclion.mysql_version+' Curl/'+udclion.curl_version;
	}
		
	$('#updraft-central-navigation .updraftcentral-settings').on('click', function() {
		
		UpdraftCentral.open_modal(udclion.settings, UpdraftCentral.template_replace('dashboard-settings', {
			uc_version: udclion.updraftcentral_version+': '+udclion.udc_version,
			running_on: UpdraftCentral.version_info_as_text()
		}), function() {
			var new_debugging_level = $('#updraftcentral_debug_level').val();
			if (new_debugging_level >= 0 && new_debugging_level <=3) {
				UpdraftCentral.set_debug_level(new_debugging_level);
			}
			UpdraftCentral.close_modal();
		}, udclion.save_settings, function() {
			$('#updraftcentral_debug_level').val(updraftcentral_debug_level);
		});
		
	});
	
	// Refresh dashicon rotates after it has been clicked - stops when the settings are refreshed.
	$('.updraftcentral_row_extracontents').on('click', '.dashicons-image-rotate', function() {
		$('.dashicons-image-rotate').addClass('dashicon-image-rotating');
	});
	
	/**
	 * Returns the result of filling in the specified Handlebars (http://handlebarsjs.com) template with the provided values
	 * 
	 * @param {string} template_name - the name of the Handlebars template, based (though it is filterable) on the path within the 'templates' directory, with slashes replaced by dashes. e.g. templates/dashboard/something.handlebars.html is accessed via a name of 'dashboard-something'
	 * @param {Object} [vars] - an object with properties (and corresponding values) corresponding to the named variables in the template and the values to replace them. N.B. The udclion object is always passed through (as udclion).
	 * @param {Object} [attr_vars] - an optional object with properties (and corresponding values) corresponding to the named variables in the template and the values to replace them, but for which the values will first be sanitized for use in HTML attributes. This may not be necessary on input which isn't user-supplied (e.g. its format may already be known to be attribute-safe).
	 * 
	 * @returns {string} The template with values filled in
	 */
	this.template_replace = function(template_name, vars, attr_vars) {
		vars = ('undefined' === typeof vars) ? {} : vars;
		if (!UpdraftCentral_Handlebars.hasOwnProperty(template_name)) {
			console.log("UDCentral: UpdraftCentral_Handlebars template not found: "+template_name);
			console.log(UpdraftCentral_Handlebars);
		}
		if ('undefined' !== typeof attr_vars) {
			$.each(attr_vars, function(k, v) {
				vars[k] = quote_attribute(v);
			});
		}
		vars.udclion = udclion;
		
		/* Checks if the template was compiled by gulp-handlebars and not the default node compiler */
		if(typeof UpdraftCentral_Handlebars[template_name] === "object"){
			return UpdraftCentral_Handlebars[template_name].handlebars(vars)	
		}
		return UpdraftCentral_Handlebars[template_name](vars);
	}
	
	UpdraftCentral_Handlebars = (typeof UpdraftCentral_Handlebars === 'undefined') ? {} : UpdraftCentral_Handlebars;
	
	Handlebars.registerHelper('uc_each', function(context, options) {
		var ret = "";
		if ('undefined' === typeof context) { return ret; }
		for(var i=0, j=context.length; i<j; i++) {
			var vars = context[i];
			vars.as_json = JSON.stringify(vars);
			vars.udclion = udclion;
			ret = ret + options.fn(vars);
		}
		return ret;
	});
	
	/**
	 * Compiles any Handlebars templates that have been passed into the page. By default, they are pre-compiled; but compilation happens in-browser when in developer mode.
	 * @returns {void}
	 */
	function compile_handlebars_templates() {
		// Initialise Handlebars.templates - it may not already exist
		if (!udclion.hasOwnProperty('handlebars')) return;
		if (udclion.handlebars.hasOwnProperty('compile')) {
			$.each(udclion.handlebars.compile, function(template_name, source) {
				console.log("UpdraftCentral: in developer mode: compile template: "+template_name);
				UpdraftCentral_Handlebars[template_name] = Handlebars.compile(source);
			});
		}
	}
	
	compile_handlebars_templates();
	
	setup_menunav();
	
	set_dashboard_mode('sites');
	
	if ('undefined' !== typeof Modernizr && !Modernizr.lastchild) {
		console.log("UDCentral: Unsupported web browser");
		$('#updraftcentral_dashboard_loading').fadeOut();
		$('#updraftcentral_updraftplus_actions, #updraftcentral_sites_actions, #updraftcentral_dashboard_existingsites_container').remove();
		this.add_dashboard_notice(udclion.unsupported_browser, 'error', false);
	} else {
		
		$('#updraftcentral_dashboard_loading').fadeOut();
		$('#updraftcentral_dashboard_existingsites_container').fadeIn();
		
		if (udclion.hasOwnProperty('show_licence_counts') && udclion.show_licence_counts) { $('.updraftcentral_licence_info').show(); }
		
		// Refresh the sites list every 24 hours
		setInterval(function(){
			send_ajax('sites_html', null, null, 'via_mothership_encrypting', '#updraftcentral_dashboard_existingsites', function(resp, code, error_code) {
				if ('ok' == code) {
					if (resp.hasOwnProperty('sites_html')) {
						set_existing_sites_to(resp.sites_html);
					} else {
						console.log("Expected sites_html data not found:");
						console.log(resp);
					}
					if (resp.hasOwnProperty('status_info')) { process_sites_status_info(resp.status_info); }
				}
			});
		}, 86400000);
		
	}
	
	// Remove any indicated notices that came pre-printed on the page
	$('#updraftcentral_notice_container .updraftcentral_notice.remove_after_load').delay(30000).slideUp('slow', function() {
		$(this).remove();
	});
	
	// Move this out of the hierarchy, so that any parent elements in the theme with z-indexes can't result in it being hidden under the grey-out (since the grey-out is not in the hierarchy)
	$('#updraftcentral_modal_dialog').appendTo(document.body);
	
	return this;
}

// https://github.com/richadams/jquery-tripleclick/
// @author Rich Adams <rich@richadams.me>
// Implements a triple-click event. Click (or touch) three times within 1s on the element to trigger.
// Licence: https://creativecommons.org/licenses/by/3.0/ (verified at 2-Feb-2016)

;(function($)
{
	// Default options
	var defaults = {
		threshold: 1000 // ms
	}
	
	function tripleHandler(event)
	{
		var $elem = jQuery(this);
		
		// Merge the defaults and any user defined settings.
		settings = jQuery.extend({}, defaults, event.data);
		
		// Get current values, or 0 if they don't yet exist.
		var clicks = $elem.data("triclick_clicks") || 0;
		var start = $elem.data("triclick_start") || 0;
		
		// If first click, register start time.
		if (clicks === 0) { start = event.timeStamp; }
		
		// If we have a start time, check it's within limit
		if (start != 0
			&& event.timeStamp > start + settings.threshold)
		{
			// Tri-click failed, took too long.
			clicks = 0;
			start = event.timeStamp;
		}
		
		// Increment counter, and do finish action.
		clicks += 1;
		if (clicks === 3)
		{
			clicks = 0;
			start = 0;
			event.type = "tripleclick";
			
			// Let jQuery handle the triggering of "tripleclick" event handlers
			if (jQuery.event.handle === undefined) {
				jQuery.event.dispatch.apply(this, arguments);
			}
			else {
				// for jQuery before 1.9
				jQuery.event.handle.apply(this, arguments);
			}
		}
		
		// Update object data
		$elem.data("triclick_clicks", clicks);
		$elem.data("triclick_start", start);
	}
	
	var tripleclick = $.event.special.tripleclick =
	{
		setup: function(data, namespaces)
		{
			$(this).bind("touchstart click.triple", data, tripleHandler);
		},
		teardown: function(namespaces)
		{
			$(this).unbind("touchstart click.triple", tripleHandler);
		}
	};
})(jQuery);
