<?php

global $wpdb;
$resource_groups = $wpdb->get_results("SELECT * FROM resource_groups");
$return['resource_groups'] = array();
if ($resource_groups && count($resource_groups)) {
    foreach ($resource_groups as $resource_group) {
        $id = $resource_group->id;
        $name = $resource_group->name ? $resource_group->name : null;
        $return['resource_groups'][] = array(
            'id' => $id,
            'name' => $name
        );
    }
}
$singular = 'resource_group';
$plural = 'resource_groups';
$column_title = 'name';
$columns = array(
    'name'
);
$sortable_columns = array(
    'name'
);
//Create an instance of our package class...
$custom_list_table = new Custom_List_Table($return['resource_groups'], $singular, $plural, $columns, $column_title, $sortable_columns);
//Fetch, prepare, sort, and filter our data...
$custom_list_table->prepare_items();

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<div class="wrap">
    <h2>Resource Groups <a href="?page=sd_resource_groups&action=new" class="add-new-h2">Add New</a></h2>
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $custom_list_table->display() ?>
    </form>
</div>