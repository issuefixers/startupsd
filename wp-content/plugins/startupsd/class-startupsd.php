<?php

require_once( plugin_dir_path( __FILE__ ) . 'class-custom-list-table.php' );

class StartUp_San_Diego
{
    /**
     * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
     * match the Text Domain file header in the main plugin file.
     * @var string
     */
    protected $plugin_slug = 'StartUp San Diego';

    /**
     * Instance of this class.
     * @var object 
     */
    protected static $instance = null;

    /**
     * Slug of the plugin screen.
     * @var array
     */
    protected $plugin_screen_hook_suffix = null;

    /**
     * Initialize the plugin by setting localization, filters, and administration functions.
     */
    private function __construct()
    {
        // Load plugin text domain
        add_action('init', array($this, 'load_plugin_textdomain'));
        add_action('init', array($this, 'app_output_buffer'));

        // Add the options page and menu item.
        add_action('admin_menu', array($this, 'add_plugin_admin_menu'));

        // Load admin style sheet and JavaScript.
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));

        // Define custom functionality. Read more about actions and filters: http://codex.wordpress.org/Plugin_API#Hooks.2C_Actions_and_Filters
//        add_action('TODO', array($this, 'action_method_name'));
//        add_filter('TODO', array($this, 'filter_method_name'));
    }

    /**
     * Return an instance of this class.
     * @return object A single instance of this class.
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fired when the plugin is activated.
     */
    public static function activate()
    {
        global $wpdb;

        // industries
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_INDUSTRIES . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE (`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);
        
        // startups
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_STARTUPS . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `is_featured` tinyint(1) NOT NULL,
            `name` varchar(255) NOT NULL,
            `tier` varchar(255) NOT NULL,
            `founder_first_name` varchar(255) NOT NULL,
            `founder_last_name` varchar(255) NOT NULL,
            `founded` datetime NOT NULL,
            `industry_id` bigint(20) NOT NULL,
            `website_url` varchar(255) NOT NULL,
            `contact` varchar(255) NOT NULL,
            `logo_url` varchar(255) NOT NULL,
            `description` text NOT NULL,
            `status` varchar(255) NOT NULL DEFAULT 'Active',,
            PRIMARY KEY (`id`),
            UNIQUE (`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);

        // event groups
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_EVENT_GROUPS . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE (`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);
        
        // events
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_EVENTS . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `is_featured` tinyint(1) NOT NULL,
            `event_group_id` varchar(255) NOT NULL,
            `event_id` varchar(255) NOT NULL,
            `event_title` varchar(255) NOT NULL,
            `event_start` datetime NOT NULL,
            `event_end` datetime NOT NULL,
            `event_address` varchar(255) NOT NULL,
            `logo_url` varchar(255) NOT NULL,
            `content` text NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);

        // resource groups
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_RESOURCE_GROUPS . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE (`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);

        // resources
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_RESOURCES . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `is_featured` tinyint(1) NOT NULL,
            `resource_group_id` bigint(20) NOT NULL,
            `name` varchar(255) NOT NULL,
            `website_url` varchar(255) NOT NULL,
            `logo_url` varchar(255) NOT NULL,
            `phone` varchar(255) NOT NULL,
            `address` varchar(255) NOT NULL,
            `description` text NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE (`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);

        // mentors
        $structure = "CREATE TABLE IF NOT EXISTS `" . TABLE_MENTORS . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `first_name` varchar(255) NOT NULL,
            `last_name` varchar(255) NOT NULL,
            `company` varchar(255) NOT NULL,
            `position` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `linkedin` varchar(255) NOT NULL,
            `facebook` varchar(255) NOT NULL,
            `twitter` varchar(255) NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
        $wpdb->query($structure);
    }

    /**
     * Fired when the plugin is deactivated.
     */
    public static function deactivate()
    {
//        global $wpdb;
//        $structure = "DROP TABLE IF EXISTS `".TABLE_STARTUPS."`;";
//        $wpdb->query($structure);
//        $structure = "DROP TABLE IF EXISTS `".TABLE_EVENTS."`;";
//        $wpdb->query($structure);
//        $structure = "DROP TABLE IF EXISTS `".TABLE_GROUPS."`;";
//        $wpdb->query($structure);
//        $structure = "DROP TABLE IF EXISTS `".TABLE_RESOURCES."`;";
//        $wpdb->query($structure);
//        $structure = "DROP TABLE IF EXISTS `".TABLE_MENTORS."`;";
//        $wpdb->query($structure);
    }

    /**
     * Load the plugin text domain for translation.
     */
    public function load_plugin_textdomain()
    {

        $domain = $this->plugin_slug;
        $locale = apply_filters('plugin_locale', get_locale(), $domain);

        load_textdomain($domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo');
        load_plugin_textdomain($domain, FALSE, dirname(plugin_basename(__FILE__)) . '/lang/');
    }

    /**
     * Register and enqueue admin-specific style sheet.
     * @return null Return early if no settings page is registered.
     */
    public function enqueue_admin_styles()
    {
        if (!isset($this->plugin_screen_hook_suffix) || empty($this->plugin_screen_hook_suffix)) {
            return;
        }

        $screen = get_current_screen();
        if (in_array($screen->id, $this->plugin_screen_hook_suffix)) {
            wp_enqueue_style($this->plugin_slug . '-jquery-ui-styles', plugins_url('css/jquery-ui/smoothness/jquery-ui-1.10.3.custom.min.css', __FILE__), array());
            wp_enqueue_style($this->plugin_slug . '-admin-styles', plugins_url('css/admin.css', __FILE__), array());
        }
    }

    /**
     * Register and enqueue admin-specific JavaScript.
     * @return null Return early if no settings page is registered.
     */
    public function enqueue_admin_scripts()
    {
        if (!isset($this->plugin_screen_hook_suffix) || empty($this->plugin_screen_hook_suffix)) {
            return;
        }

        $screen = get_current_screen();
        if (in_array($screen->id, $this->plugin_screen_hook_suffix)) {
            wp_enqueue_script($this->plugin_slug . '-jquery-ui-script', plugins_url('js/plugins/jquery-ui-1.10.3.custom.min.js', __FILE__), array('jquery'));
            wp_enqueue_script($this->plugin_slug . '-timepicker-script', plugins_url('js/plugins/jquery-ui-timepicker-addon.js', __FILE__), array('jquery'));
            wp_enqueue_script($this->plugin_slug . '-admin-script', plugins_url('js/admin.js', __FILE__), array('jquery'));
        }
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     */
    public function add_plugin_admin_menu()
    {
        $this->plugin_screen_hook_suffix = array(
            add_menu_page("Startup San Diego Configuration", "StartUpSanDiego", 8, 'sd_dashboard', array($this, 'display_plugin_admin_page')),
            //add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
            add_submenu_page('sd_dashboard', 'Dashboard', 'Dashboard', 8, 'sd_dashboard', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Industries', 'Industries', 8, 'sd_industries', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Startups', 'Startups', 8, 'sd_startups', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Event Groups', 'Event Groups', 8, 'sd_event_groups', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Events', 'Events', 8, 'sd_events', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Resource Groups', 'Resource Groups', 8, 'sd_resource_groups', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Resources', 'Resources', 8, 'sd_resources', array($this, 'action')),
            add_submenu_page('sd_dashboard', 'Mentors', 'Mentors', 8, 'sd_mentors', array($this, 'action'))
        );
    }

    /**
     * Render the settings page for this plugin.
     */
    public function display_plugin_admin_page()
    {
        include_once('views/admin.php');
    }

    public function action()
    {
        global $wpdb;
        if (isset($_GET['page'])) {
            $plural = str_replace('sd_', '', $_GET['page']);
            $singular = ($plural == 'industries') ? 'industry' : substr($plural, 0, -1);
            if (isset($_GET['action'])) {
                switch ($_GET['action']) {
                    case 'new':
                        if (file_exists(plugin_dir_path( __FILE__ ).$singular.'-new.php')) {
                            include_once($singular.'-new.php');
                        }
                        break;
                    case 'edit':
                        if (file_exists(plugin_dir_path( __FILE__ ).$singular.'-edit.php')) {
                            include_once($singular.'-edit.php');
                        }
                        break;
                    case 'delete':
                        if (isset($_GET[$singular]) && is_numeric($_GET[$singular])) {
                            $object = $wpdb->get_row('SELECT * FROM '.$plural.' WHERE id = '.$_GET[$singular]);
                            if ($object) {
                                if (isset($object->logo_url) && $object->logo_url) {
                                    unlink(plugin_dir_path(__FILE__).$object->logo_url);
                                }
                                $result = $wpdb->delete($plural, array('id' => $_GET[$singular]), array('%d'));
                                if ($result) {
                                    $message = 'The item was DELETED successfully.';
                                    $_SESSION['message'] = $message;
                                } else {
                                    $notice = 'The item was NOT DELETED successfully.';
                                    $_SESSION['notice'] = $notice;
                                }
                                wp_redirect("admin.php?page=".$_GET['page']);
                                die();
                            }
                        }
                        break;
                    default:
                        $notice = 'Not valid action.';
                        $_SESSION['notice'] = $notice;
                        wp_redirect("admin.php?page=".$_GET['page']);
                        die();
                        break;
                }
            } else {
                if (file_exists(plugin_dir_path( __FILE__ ).$plural.'.php')) {
                    include_once($plural.'.php');
                }
            }
        }
        
    }
    
    /**
     * NOTE:  Actions are points in the execution of a page or process
     *        lifecycle that WordPress fires.
     *
     *        WordPress Actions: http://codex.wordpress.org/Plugin_API#Actions
     *        Action Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
     */
    public function action_method_name()
    {
        // TODO: Define your action hook callback here
    }

    /**
     * NOTE:  Filters are points of execution in which WordPress modifies data
     *        before saving it or sending it to the browser.
     *
     *        WordPress Filters: http://codex.wordpress.org/Plugin_API#Filters
     *        Filter Reference:  http://codex.wordpress.org/Plugin_API/Filter_Reference
     */
    public function filter_method_name()
    {
        // TODO: Define your filter hook callback here
    }

    function app_output_buffer() {
      ob_start();
    }
}