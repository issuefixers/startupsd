<?php

global $wpdb;
$industries = $wpdb->get_results("SELECT * FROM industries");
$return['industries'] = array();
if ($industries && count($industries)) {
    foreach ($industries as $industry) {
        $id = $industry->id;
        $name = $industry->name ? $industry->name : null;
        $return['industries'][] = array(
            'id' => $id,
            'name' => $name
        );
    }
}
$singular = 'industry';
$plural = 'industries';
$column_title = 'name';
$columns = array(
    'name'
);
$sortable_columns = array(
    'name'
);
//Create an instance of our package class...
$custom_list_table = new Custom_List_Table($return['industries'], $singular, $plural, $columns, $column_title, $sortable_columns);
//Fetch, prepare, sort, and filter our data...
$custom_list_table->prepare_items();

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<div class="wrap">
    <h2>Industries <a href="?page=sd_industries&action=new" class="add-new-h2">Add New</a></h2>
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $custom_list_table->display() ?>
    </form>
</div>