<?php

global $wpdb;
$header = '<h2>Add New Event Group</h2>';

if (isset($_POST['event_group_post'])) {
    $name = stripslashes($_POST['name']);
   
    if (!isset($notice) || empty($notice)) {       
        $result = $wpdb->insert( 
            'event_groups', 
            array( 
                'name' => $name
            ), 
            array( 
                '%s'
            ) 
        );
        if (!$result) {
            $notice = 'The item was NOT ADDED successfully.';
        } else {
            $message = 'The item was ADDED successfully.';
            $_SESSION['message'] = $message;
            wp_redirect("admin.php?page=sd_event_groups&action=edit&event_group=".$wpdb->insert_id);
            die();
        }
    }
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/event_group-form.php')) {
    include_once('views/event_group-form.php');
}

?>
