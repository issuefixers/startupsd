<?php

global $wpdb;
$search = '';
if (isset($_GET['resources-search-submit'])) {
    if (isset($_GET['s']) && !empty($_GET['s'])) {
        $search = " WHERE name LIKE '%".$_GET['s']."%' OR description LIKE '%".$_GET['s']."%'";
    }
}
$resource_groups = $wpdb->get_results("SELECT * FROM resource_groups");
$resource_group_pairs = array();
if ($resource_groups && count($resource_groups)) {
    foreach ($resource_groups as $resource_group) {
        $id = $resource_group->id;
        $name = $resource_group->name ? $resource_group->name : null;
        if ($name) {
            $resource_group_pairs[$id] = $name;
        }
    }
}
$resources = $wpdb->get_results("SELECT * FROM resources".$search);
$return['resources'] = array();
if ($resources && count($resources)) {
    foreach ($resources as $resource) {
        $id = $resource->id;
        $is_featured = empty($resource->is_featured) ? 'No' : 'Yes';
        $resource_group = ($resource->resource_group_id && isset($resource_group_pairs[$resource->resource_group_id])) ? $resource_group_pairs[$resource->resource_group_id] : null;
        $name = $resource->name ? $resource->name : null;
        $website_url = $resource->website_url ? $resource->website_url : null;
        $logo_url = $resource->logo_url ? plugins_url('startupsd/').$resource->logo_url : null;
        $phone = $resource->phone ? $resource->phone : null;
        $address = $resource->address ? $resource->address : null;
        $description = $resource->description ? $resource->description : null;
        $return['resources'][] = array(
            'id' => $id,
            'is_featured' => $is_featured,
            'resource_group' => $resource_group,
            'name' => $name,
            'website_url' => $website_url,
            'logo_url' => $logo_url,
            'phone' => $phone,
            'address' => $address,
            'description' => $description
        );
    }
}
$singular = 'resource';
$plural = 'resources';
$column_title = 'name';
$columns = array(
    'name',
    'is_featured',
    'resource_group',
    'website_url',
    'logo_url',
    'phone',
    'address',
    'description'
);
$sortable_columns = array(
    'name',
    'is_featured',
    'resource_group'
);
//Create an instance of our package class...
$custom_list_table = new Custom_List_Table($return['resources'], $singular, $plural, $columns, $column_title, $sortable_columns);
//Fetch, prepare, sort, and filter our data...
$custom_list_table->prepare_items();

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<div class="wrap">
    <h2>Resources <a href="?page=sd_resources&action=new" class="add-new-h2">Add New</a></h2>
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form method="get">
        <p class="search-box">
            <label class="screen-reader-text" for="resources-search-input">Search:</label>
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <input type="search" id="resources-search-input" name="s" value="<?php echo isset($_GET['s']) && !empty($_GET['s']) ? $_GET['s'] : "" ?>">
            <input type="submit" name="resources-search-submit" id="resources-search-submit" class="button" value="Search">
        </p>
    </form>     
    <form method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $custom_list_table->display() ?>
    </form>
</div>