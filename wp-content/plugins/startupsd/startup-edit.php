<?php

global $wpdb;
$header = '<h2>Edit Startup <a href="?page=sd_startups&action=new" class="add-new-h2">Add New</a></h2>';
$startup_id =  (isset($_GET['startup']) && is_numeric($_GET['startup'])) ? $_GET['startup'] : null;

if ($startup_id) {
    $startup = $wpdb->get_row('SELECT * FROM startups WHERE id = '.$startup_id);
    if ($startup) {
        if (isset($_POST['startup_post']) && isset($_POST['startup_id']) && ($startup_id == $_POST['startup_id'])) {
            $industry_id = stripslashes($_POST['industry_id']);
            if (empty($industry_id)) {
                $notice = 'No industry assigned.';
            }
            $tier = stripslashes($_POST['tier']);
            if (!$notice && !is_numeric($tier)) {
                $notice = 'No tier assigned.';
            }
            $name = stripslashes($_POST['name']);
            $is_featured = stripslashes($_POST['is_featured']);
            $founder_first_name = stripslashes($_POST['founder_first_name']);
            $founder_last_name = stripslashes($_POST['founder_last_name']);
            $founded = stripslashes($_POST['founded']);
            $website_url = stripslashes($_POST['website_url']);
            $contact = stripslashes($_POST['contact']);
            $status = stripslashes($_POST['status']);
            if ($startup->logo_url) {
                $logo_url = $startup->logo_url;
            }
            
            $logo_name = $_FILES["logo_url"]["name"];
            $logo_type = $_FILES["logo_url"]["type"];
            $logo_error = $_FILES["logo_url"]["error"];
            $logo_size = $_FILES["logo_url"]["size"];
            $logo_tmp = $_FILES["logo_url"]["tmp_name"];

            if ($logo_error != 4) { // UPLOAD_ERR_NO_FILE: No file was uploaded.
        
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $temp = explode(".", $logo_name);
                $extension = end($temp);

                if ((($logo_type == "image/gif") || ($logo_type == "image/jpeg") || ($logo_type == "image/jpg") 
                    || ($logo_type == "image/pjpeg") || ($logo_type == "image/x-png") || ($logo_type == "image/png"))
                    && ($logo_size < 10000000) && in_array($extension, $allowedExts)) {
                    if ($logo_error > 0) {
                        $notice = "Upload Error.";
                    } else {
                        if (file_exists(plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                            $notice = $logo_name." already exists.";
                        } else {
                            if (move_uploaded_file($logo_tmp, plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                                $logo_url = "uploads/".$logo_name;
                            } else {
                                $notice = "Upload Error.";
                            }
                        }
                    }
                } else {
                    $notice = "Invalid File.";
                }
            }
            
            $description = stripslashes($_POST['description']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'startups', 
                    array( 
                        'name' => $name,
                        'is_featured' => $is_featured,
                        'tier' => $tier,
                        'founder_first_name' => $founder_first_name,
                        'founder_last_name' => $founder_last_name,
                        'founded' => $founded,
                        'industry_id' => $industry_id,
                        'website_url' => $website_url,
                        'contact' => $contact,
                        'logo_url' => $logo_url,
                        'status' => $status,
                        'description' => $description
                    ), 
                    array( 'id' => $startup_id ), 
                    array( 
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $name = $startup->name ? $startup->name : null;
            $is_featured = $startup->is_featured ? $startup->is_featured : null;
            $tier = is_numeric($startup->tier) ? $startup->tier : null;
            $founder_first_name = $startup->founder_first_name ? $startup->founder_first_name : null;
            $founder_last_name = $startup->founder_last_name ? $startup->founder_last_name : null;
            $founded = $startup->founded ? $startup->founded : null;
            $industry_id = $startup->industry_id ? $startup->industry_id : null;
            $website_url = $startup->website_url ? $startup->website_url : null;
            $contact = $startup->contact ? $startup->contact : null;
            $logo_url = $startup->logo_url ? $startup->logo_url : null;
            $description = $startup->description ? $startup->description : null;
            $status = $startup->status ? $startup->status : null;
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/startup-form.php')) {
    include_once('views/startup-form.php');
}

?>
