<?php

global $wpdb;
$industries = $wpdb->get_results("SELECT * FROM industries ORDER BY id ASC");
$tiers = array(0,1,2);
$status_options = array('Active', 'Exited', 'Dead');

?>
<div class="wrap">
    <?php echo $header; ?>
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form name="startup_form" action="" method="post" enctype="multipart/form-data">
        <input type='hidden' id='startup_id' name='startup_id' value='<?php echo $startup_id; ?>' />
        <fieldset>
            <div class="standard-fields">
                <wrapper-field>
                    <label for="status">Status</label>
                    <select name="status" id="status">
                        <?php if ($status_options && count($status_options)): ?>
                            <?php foreach ($status_options as $status_option): ?>
                                <option value="<?php echo $status_option; ?>" <?php echo ($status_option == $status) ? 'selected' : ''; ?>>
                                    <?php echo $status_option; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </wrapper-field>
                <wrapper-field>
                    <label for="industry_id">Industry</label>
                    <select name="industry_id" id="industry_id">
                        <option value="">Select</option>
                        <?php if ($industries && count($industries)): ?>
                            <?php foreach ($industries as $industry): ?>
                                <?php $industry_name = $industry->name ? $industry->name : null; ?>
                                <?php if ($industry_name): ?>
                                    <option value="<?php echo $industry->id; ?>" <?php echo ($industry->id == $industry_id) ? 'selected' : ''; ?>>
                                        <?php echo $industry_name; ?>
                                    </option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </wrapper-field>
                <wrapper-field>
                    <label for="tier">Tier</label>
                    <select name="tier" id="tier">
                        <option value="">Select</option>
                        <?php if ($tiers && count($tiers)): ?>
                            <?php foreach ($tiers as $startup_tier): ?>
                                <option value="<?php echo $startup_tier; ?>" <?php echo (is_numeric($tier) && $startup_tier == $tier) ? 'selected' : ''; ?>>
                                    <?php echo $startup_tier; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </wrapper-field>
                <wrapper-field>
                    <label for="name">Name</label>
                    <input type="text" name="name" value="<?php echo $name; ?>" id="name">
                </wrapper-field>
                <wrapper-field>
                    <label for="is_featured">Is Featured</label>
                    <input type="checkbox" name="is_featured" value="1" id="is_featured" <?php echo empty($is_featured) ? '' : 'checked' ?>>
                </wrapper-field>
                <wrapper-field>
                    <label for="founder_first_name">Founder First Name</label>
                    <input type="text" name="founder_first_name" value="<?php echo $founder_first_name; ?>" id="founder_first_name">
                </wrapper-field>
                <wrapper-field>
                    <label for="founder_last_name">Founder Last Name</label>
                    <input type="text" name="founder_last_name" value="<?php echo $founder_last_name; ?>" id="founder_last_name">
                </wrapper-field>
                <wrapper-field>
                    <label for="founded">Founded</label>
                    <input type="text" name="founded" value="<?php echo $founded; ?>" id="founded">
                </wrapper-field>
                <wrapper-field>
                    <label for="website_url">Website Url</label>
                    <input type="text" name="website_url" value="<?php echo $website_url; ?>" id="website_url">
                </wrapper-field>
                <wrapper-field>
                    <label for="contact">Contact</label>
                    <input type="text" name="contact" value="<?php echo $contact; ?>"id="contact">
                </wrapper-field>
                <?php if ($logo_url && file_exists(plugin_dir_path(__DIR__).$logo_url)): ?>
                <wrapper-field>
                    <img src="<?php echo plugins_url('startupsd/').$logo_url; ?>" width="300" alt="">
                </wrapper-field>
                <?php endif; ?>
                <wrapper-field>
                    <label for="logo_url">Logo Url</label>
                    <input type="file" name="logo_url" value="<?php echo $logo_url; ?>" id="logo_url">
                </wrapper-field>
                <wrapper-field>
                    <?php wp_editor($description, 'description', array('dfw' => true, 'editor_height' => 360) ); ?>
                </wrapper-field>
                <input type="submit" value="save" name="startup_post">
            </div>
        </fieldset>
    </form>
</div>