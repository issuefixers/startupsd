<div class="wrap">
    <?php echo $header; ?>
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form name="event_group_form" action="" method="post" enctype="multipart/form-data">
        <input type='hidden' id='event_group_id' name='event_group_id' value='<?php echo $event_group_id; ?>' />
        <fieldset>
            <div class="standard-fields">
                <wrapper-field>
                    <label for="name">Name</label>
                    <input type="text" name="name" value="<?php echo $name; ?>" id="name">
                </wrapper-field>
                <input type="submit" value="save" name="event_group_post">
            </div>
        </fieldset>
    </form>
</div>