<?php

global $wpdb;
$resource_groups = $wpdb->get_results("SELECT * FROM resource_groups");

?>
<div class="wrap">
    <?php echo $header; ?>
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form name="resource_form" action="" method="post" enctype="multipart/form-data">
        <input type='hidden' id='resource_id' name='resource_id' value='<?php echo $resource_id; ?>' />
        <fieldset>
            <div class="standard-fields">
                <wrapper-field>
                    <label for="resource_group_id">Resource Group</label>
                    <select name="resource_group_id" id="resource_group_id">
                        <option value="">Select</option>
                        <?php if ($resource_groups && count($resource_groups)): ?>
                            <?php foreach ($resource_groups as $resource_group): ?>
                                <?php $resource_group_name = $resource_group->name ? $resource_group->name : null; ?>
                                <?php if ($resource_group_name): ?>
                                    <option value="<?php echo $resource_group->id; ?>" <?php echo ($resource_group->id == $resource_group_id) ? 'selected' : ''; ?>>
                                        <?php echo $resource_group_name; ?>
                                    </option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </wrapper-field>
                <wrapper-field>
                    <label for="name">Name</label>
                    <input type="text" name="name" value="<?php echo $name; ?>" id="name">
                </wrapper-field>
                <wrapper-field>
                    <label for="is_featured">Is Featured</label>
                    <input type="checkbox" name="is_featured" value="1" id="is_featured" <?php echo empty($is_featured) ? '' : 'checked' ?>>
                </wrapper-field>
                <wrapper-field>
                    <label for="website_url">Website Url</label>
                    <input type="text" name="website_url" value="<?php echo $website_url; ?>" id="website_url">
                </wrapper-field>
                <?php if ($logo_url && file_exists(plugin_dir_path(__DIR__).$logo_url)): ?>
                <wrapper-field>
                    <img src="<?php echo plugins_url('startupsd/').$logo_url; ?>" width="300" alt="">
                </wrapper-field>
                <?php endif; ?>
                <wrapper-field>
                    <label for="logo_url">Logo Url</label>
                    <input type="file" name="logo_url" value="<?php echo $logo_url; ?>" id="logo_url">
                </wrapper-field>
                <wrapper-field>
                    <label for="phone">Phone</label>
                    <input type="text" name="phone" value="<?php echo $phone; ?>"id="phone">
                </wrapper-field>
                <wrapper-field>
                    <label for="address">Address</label>
                    <input type="text" name="address" value="<?php echo $address; ?>"id="address">
                </wrapper-field>
                <wrapper-field>
                    <?php wp_editor($description, 'description', array('dfw' => true, 'editor_height' => 360) ); ?>
                </wrapper-field>
                <input type="submit" value="save" name="resource_post">
            </div>
        </fieldset>
    </form>
</div>