<div class="wrap">
    <?php echo $header; ?>
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form name="mentor_form" action="" method="post" enctype="multipart/form-data">
        <input type='hidden' id='mentor_id' name='mentor_id' value='<?php echo $mentor_id; ?>' />
        <fieldset>
            <div class="standard-fields">
                <wrapper-field>
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" value="<?php echo $first_name; ?>" id="first_name">
                </wrapper-field>
                <wrapper-field>
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" value="<?php echo $last_name; ?>" id="last_name">
                </wrapper-field>
                <wrapper-field>
                    <label for="company">Company</label>
                    <input type="text" name="company" value="<?php echo $company; ?>" id="company">
                </wrapper-field>
                <wrapper-field>
                    <label for="position">Position</label>
                    <input type="text" name="position" value="<?php echo $position; ?>" id="position">
                </wrapper-field>
                <wrapper-field>
                    <label for="email">Email</label>
                    <input type="text" name="email" value="<?php echo $email; ?>" id="email">
                </wrapper-field>
                <wrapper-field>
                    <label for="linkedin">Linkedin</label>
                    <input type="text" name="linkedin" value="<?php echo $linkedin; ?>" id="linkedin">
                </wrapper-field>
                <wrapper-field>
                    <label for="facebook">Facebook</label>
                    <input type="text" name="facebook" value="<?php echo $facebook; ?>" id="facebook">
                </wrapper-field>
                <wrapper-field>
                    <label for="twitter">Twitter</label>
                    <input type="text" name="twitter" value="<?php echo $twitter; ?>" id="twitter">
                </wrapper-field>
                <input type="submit" value="save" name="mentor_post">
            </div>
        </fieldset>
    </form>
</div>