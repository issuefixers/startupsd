<?php

global $wpdb;
$event_groups = $wpdb->get_results("SELECT * FROM event_groups");

?>
<div class="wrap">
    <?php echo $header; ?>
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form name="event_form" action="" method="post" enctype="multipart/form-data">
        <input type='hidden' id='event_id' name='event_id' value='<?php echo $event_id; ?>' />
        <fieldset>
            <div class="standard-fields">
                <wrapper-field>
                    <label for="event_group_id">Event Group</label>
                    <?php if ($event_groups && count($event_groups)): ?>
                        <select name="event_group_id[]" size="<?php echo count($event_groups); ?>" id="event_group_id" multiple>       
                            <?php foreach ($event_groups as $event_group): ?>
                                <?php $event_group_name = $event_group->name ? $event_group->name : null; ?>
                                <?php if ($event_group_name): ?>
                                    <option value="<?php echo $event_group->id; ?>" <?php echo in_array($event_group->id, $event_group_ids) ? 'selected' : ''; ?>>
                                        <?php echo $event_group_name; ?>
                                    </option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        <h2>No Available Groups!</h2>
                    <?php endif; ?>
                </wrapper-field>
                <wrapper-field>
                    <label for="title">Title</label>
                    <input type="text" name="title" value="<?php echo $title; ?>" id="title">
                </wrapper-field>
                <wrapper-field>
                    <label for="is_featured">Is Featured</label>
                    <input type="checkbox" name="is_featured" value="1" id="is_featured" <?php echo empty($is_featured) ? '' : 'checked' ?>>
                </wrapper-field>
                <wrapper-field>
                    <label for="link">Link</label>
                    <input type="text" name="link" value="<?php echo $link; ?>" id="link">
                </wrapper-field>
                <wrapper-field>
                    <label for="start">Start Date</label>
                    <input type="text" name="start" value="<?php echo $start; ?>" id="start" class="date-picker">
                </wrapper-field>
                <wrapper-field>
                    <label for="end">End Date</label>
                    <input type="text" name="end" value="<?php echo $end; ?>" id="end" class="date-picker">
                </wrapper-field>
                <wrapper-field>
                    <label for="location">Location</label>
                    <input type="text" name="location" value="<?php echo $location; ?>" id="location">
                </wrapper-field>
                <?php if ($logo_url && file_exists(plugin_dir_path(__DIR__).$logo_url)): ?>
                <wrapper-field>
                    <img src="<?php echo plugins_url('startupsd/').$logo_url; ?>" width="300" alt="">
                </wrapper-field>
                <?php endif; ?>
                <wrapper-field>
                    <label for="logo_url">Logo Url</label>
                    <input type="file" name="logo_url" value="<?php echo $logo_url; ?>" id="logo_url">
                </wrapper-field>
                <wrapper-field>
                    <?php wp_editor($description, 'description', array('dfw' => true, 'editor_height' => 360) ); ?>
                </wrapper-field>
                <input type="submit" value="save" name="event_post">
            </div>
        </fieldset>
    </form>
</div>