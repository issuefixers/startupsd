<?php

global $wpdb;
$search = '';
if (isset($_GET['startups-search-submit'])) {
    if (isset($_GET['s']) && !empty($_GET['s'])) {
        $search = " WHERE name LIKE '%".$_GET['s']."%' OR founder_first_name LIKE '%".$_GET['s']."%' OR founder_last_name LIKE '%".$_GET['s']."%' OR description LIKE '%".$_GET['s']."%'";
    }
}
$industries = $wpdb->get_results("SELECT * FROM industries");
$industry_pairs = array();
if ($industries && count($industries)) {
    foreach ($industries as $industry) {
        $id = $industry->id;
        $name = $industry->name ? $industry->name : null;
        if ($name) {
            $industry_pairs[$id] = $name;
        }
    }
}
$startups = $wpdb->get_results("SELECT * FROM startups".$search);
$return['startups'] = array();
if ($startups && count($startups)) {
    foreach ($startups as $startup) {
        $status = $startup->status;
        $id = $startup->id;
        $is_featured = empty($startup->is_featured) ? 'No' : 'Yes';
        $name = $startup->name ? $startup->name : null;
        $tier = is_numeric($startup->tier) ? $startup->tier : null;
        $founder_first_name = $startup->founder_first_name ? $startup->founder_first_name : null;
        $founder_last_name = $startup->founder_last_name ? $startup->founder_last_name : null;
        $founded = $startup->founded ? $startup->founded : null;
        $industry = ($startup->industry_id && isset($industry_pairs[$startup->industry_id])) ? $industry_pairs[$startup->industry_id] : null;
        $website_url = $startup->website_url ? $startup->website_url : null;
        $contact = $startup->contact ? $startup->contact : null;
        $logo_url = $startup->logo_url ? plugins_url('startupsd/').$startup->logo_url : null;
        $description = $startup->description ? $startup->description : null;
        $return['startups'][] = array(
            'status' => $status,
            'id' => $id,
            'is_featured' => $is_featured,
            'name' => $name,
            'tier' => $tier,
            'founder_first_name' => $founder_first_name,
            'founder_last_name' => $founder_last_name,
            'founded' => $founded,
            'industry' => $industry,
            'website_url' => $website_url,
            'contact' => $contact,
            'logo_url' => $logo_url,
            'description' => $description
        );
    }
}
$singular = 'startup';
$plural = 'startups';
$column_title = 'name';
$columns = array(
    'name',
    'is_featured',
    'tier',
    'founder_first_name',
    'founder_last_name',
    'founded',
    'industry',
    'website_url',
    'contact',
    'logo_url',
    'description',
    'status'
);
$sortable_columns = array(
    'name',
    'is_featured',
    'tier',
    'industry',
    'status'
);
//Create an instance of our package class...
$custom_list_table = new Custom_List_Table($return['startups'], $singular, $plural, $columns, $column_title, $sortable_columns);
//Fetch, prepare, sort, and filter our data...
$custom_list_table->prepare_items();

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<div class="wrap">
    <h2>Startups <a href="?page=sd_startups&action=new" class="add-new-h2">Add New</a></h2>
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form method="get">
        <p class="search-box">
            <label class="screen-reader-text" for="startups-search-input">Search:</label>
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <input type="search" id="startups-search-input" name="s" value="<?php echo isset($_GET['s']) && !empty($_GET['s']) ? $_GET['s'] : "" ?>">
            <input type="submit" name="startups-search-submit" id="startups-search-submit" class="button" value="Search">
        </p>
    </form>        
    <form method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $custom_list_table->display() ?>
    </form>
</div>