<?php

global $wpdb;
$header = '<h2>Edit Resource <a href="?page=sd_resources&action=new" class="add-new-h2">Add New</a></h2>';
$resource_id =  (isset($_GET['resource']) && is_numeric($_GET['resource'])) ? $_GET['resource'] : null;

if ($resource_id) {
    $resource = $wpdb->get_row('SELECT * FROM resources WHERE id = '.$resource_id);
    if ($resource) {
        if (isset($_POST['resource_post']) && isset($_POST['resource_id']) && ($resource_id == $_POST['resource_id'])) {
            $resource_group_id = stripslashes($_POST['resource_group_id']);
            if (empty($resource_group_id)) {
                $notice = 'No resource group assigned.';
            }
            $name = stripslashes($_POST['name']);
            $is_featured = stripslashes($_POST['is_featured']);
            $website_url = stripslashes($_POST['website_url']);
            $phone = stripslashes($_POST['phone']);
            $address = stripslashes($_POST['address']);
            
            if ($resource->logo_url) {
                $logo_url = $resource->logo_url;
            }
            
            $logo_name = $_FILES["logo_url"]["name"];
            $logo_type = $_FILES["logo_url"]["type"];
            $logo_error = $_FILES["logo_url"]["error"];
            $logo_size = $_FILES["logo_url"]["size"];
            $logo_tmp = $_FILES["logo_url"]["tmp_name"];

            if ($logo_error != 4) { // UPLOAD_ERR_NO_FILE: No file was uploaded.
        
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $temp = explode(".", $logo_name);
                $extension = end($temp);

                if ((($logo_type == "image/gif") || ($logo_type == "image/jpeg") || ($logo_type == "image/jpg") 
                    || ($logo_type == "image/pjpeg") || ($logo_type == "image/x-png") || ($logo_type == "image/png"))
                    && ($logo_size < 10000000) && in_array($extension, $allowedExts)) {
                    if ($logo_error > 0) {
                        $notice = "Upload Error.";
                    } else {
                        if (file_exists(plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                            $notice = $logo_name." already exists.";
                        } else {
                            if (move_uploaded_file($logo_tmp, plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                                $logo_url = "uploads/".$logo_name;
                            } else {
                                $notice = "Upload Error.";
                            }
                        }
                    }
                } else {
                    $notice = "Invalid File.";
                }
            }
            
            $description = stripslashes($_POST['description']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'resources', 
                    array(
                        'is_featured' => $is_featured,
                        'resource_group_id' => $resource_group_id,
                        'name' => $name,
                        'website_url' => $website_url,
                        'logo_url' => $logo_url,
                        'phone' => $phone,
                        'address' => $address,
                        'description' => $description
                    ), 
                    array( 'id' => $resource_id ), 
                    array( 
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $is_featured = $resource->is_featured ? $resource->is_featured : null;
            $resource_group_id = $resource->resource_group_id ? $resource->resource_group_id : null;
            $name = $resource->name ? $resource->name : null;
            $website_url = $resource->website_url ? $resource->website_url : null;
            $logo_url = $resource->logo_url ? $resource->logo_url : null;
            $phone = $resource->phone ? $resource->phone : null;
            $address = $resource->address ? $resource->address : null;
            $description = $resource->description ? $resource->description : null;  
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/resource-form.php')) {
    include_once('views/resource-form.php');
}

?>
