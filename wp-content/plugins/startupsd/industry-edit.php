<?php

global $wpdb;
$header = '<h2>Edit Industry <a href="?page=sd_industries&action=new" class="add-new-h2">Add New</a></h2>';
$industry_id =  (isset($_GET['industry']) && is_numeric($_GET['industry'])) ? $_GET['industry'] : null;

if ($industry_id) {
    $industry = $wpdb->get_row('SELECT * FROM industries WHERE id = '.$industry_id);
    if ($industry) {
        if (isset($_POST['industry_post']) && isset($_POST['industry_id']) && ($industry_id == $_POST['industry_id'])) {
            $name = stripslashes($_POST['name']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'industries', 
                    array( 
                        'name' => $name
                    ), 
                    array( 'id' => $industry_id ), 
                    array( 
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $name = $industry->name ? $industry->name : null;
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/industry-form.php')) {
    include_once('views/industry-form.php');
}

?>