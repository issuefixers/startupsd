<?php

global $wpdb;
$header = '<h2>Edit Event Group <a href="?page=sd_event_groups&action=new" class="add-new-h2">Add New</a></h2>';
$event_group_id =  (isset($_GET['event_group']) && is_numeric($_GET['event_group'])) ? $_GET['event_group'] : null;

if ($event_group_id) {
    $event_group = $wpdb->get_row('SELECT * FROM event_groups WHERE id = '.$event_group_id);
    if ($event_group) {
        if (isset($_POST['event_group_post']) && isset($_POST['event_group_id']) && ($event_group_id == $_POST['event_group_id'])) {
            $name = stripslashes($_POST['name']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'event_groups', 
                    array( 
                        'name' => $name
                    ), 
                    array( 'id' => $event_group_id ), 
                    array( 
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $name = $event_group->name ? $event_group->name : null;
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/event_group-form.php')) {
    include_once('views/event_group-form.php');
}

?>
