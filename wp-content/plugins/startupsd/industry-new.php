<?php

global $wpdb;
$header = '<h2>Add New Industry</h2>';

if (isset($_POST['industry_post'])) {
    $name = stripslashes($_POST['name']);
   
    if (!isset($notice) || empty($notice)) {       
        $result = $wpdb->insert( 
            'industries', 
            array( 
                'name' => $name
            ), 
            array( 
                '%s'
            ) 
        );
        if (!$result) {
            $notice = 'The item was NOT ADDED successfully.';
        } else {
            $message = 'The item was ADDED successfully.';
            $_SESSION['message'] = $message;
            wp_redirect("admin.php?page=sd_industries&action=edit&industry=".$wpdb->insert_id);
            die();
        }
    }
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/industry-form.php')) {
    include_once('views/industry-form.php');
}

?>
