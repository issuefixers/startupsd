<?php

global $wpdb;
$search = '';
if (isset($_GET['events-search-submit'])) {
    if (isset($_GET['s']) && !empty($_GET['s'])) {
        $search = " WHERE event_title LIKE '%".$_GET['s']."%' OR content LIKE '%".$_GET['s']."%' OR event_address LIKE '%".$_GET['s']."%'";
    }
}
$event_groups = $wpdb->get_results("SELECT * FROM event_groups");
$event_group_pairs = array();
if ($event_groups && count($event_groups)) {
    foreach ($event_groups as $event_group) {
        $id = $event_group->id;
        $name = $event_group->name ? $event_group->name : null;
        if ($name) {
            $event_group_pairs[$id] = $name;
        }
    }
}
$events = $wpdb->get_results("SELECT * FROM events".$search);
$return['events'] = array();
if ($events && count($events)) {
    foreach ($events as $event) {
        $id = $event->id;
        $event_group = null;
        if ($event->event_group_id) {
            $event_group_ids = explode(',', $event->event_group_id);
            foreach ($event_group_ids as $event_group_id) {
                if (isset($event_group_pairs[$event_group_id])) {
                    $event_group .= $event_group_pairs[$event_group_id].',';
                }
            }
            $event_group = substr($event_group, 0, -1);
        }
        $event_id = $event->event_id ? $event->event_id : null;
        $is_featured = empty($event->is_featured) ? 'No' : 'Yes';
        $event_title = $event->event_title ? htmlspecialchars_decode($event->event_title) : null;
        if ($event->event_start != '0000-00-00 00:00:00') {
            if (date('H:i:s', strtotime($event->event_start)) == '00:00:00') {
                $event_start = date('Y-m-d', strtotime($event->event_start));
            } else {
                $event_start = date('Y-m-d g:ia', strtotime($event->event_start));
            }
        } else {
            $event_start = null;
        }
        if ($event->event_end != '0000-00-00 00:00:00') {
            if (date('H:i:s', strtotime($event->event_end)) == '00:00:00') {
                $event_end = date('Y-m-d', strtotime($event->event_end));
            } else {
                $event_end = date('Y-m-d g:ia', strtotime($event->event_end));
            }
        } else {
            $event_end = null;
        }
        $event_address = $event->event_address ? $event->event_address : null;
        $logo_url = $event->logo_url ? plugins_url('startupsd/').$event->logo_url : null;
        $content = $event->content ? htmlspecialchars_decode($event->content) : null;
        $return['events'][] = array(
            'id' => $id,
            'is_featured' => $is_featured,
            'event_group' => $event_group,
            'link' => $event_id,
            'title' => $event_title,
            'start_date' => $event_start,
            'end_date' => $event_end,
            'logo_url' => $logo_url,
            'description' => $content,
            'location' => $event_address
        );
    }
}
$singular = 'event';
$plural = 'events';
$column_title = 'title';
$columns = array(
    'title',
    'is_featured',
    'event_group',
    'link',
    'start_date',
    'end_date',
    'location',
    'logo_url',
    'description'
);
$sortable_columns = array(
    'title',
    'is_featured',
    'event_group',
    'start_date'
);
//Create an instance of our package class...
$custom_list_table = new Custom_List_Table($return['events'], $singular, $plural, $columns, $column_title, $sortable_columns);
//Fetch, prepare, sort, and filter our data...
$custom_list_table->prepare_items();

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<div class="wrap">
    <h2>Events <a href="?page=sd_events&action=new" class="add-new-h2">Add New</a></h2>
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form method="get">
        <p class="search-box">
            <label class="screen-reader-text" for="events-search-input">Search:</label>
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <input type="search" id="events-search-input" name="s" value="<?php echo isset($_GET['s']) && !empty($_GET['s']) ? $_GET['s'] : "" ?>">
            <input type="submit" name="events-search-submit" id="events-search-submit" class="button" value="Search">
        </p>
    </form>     
    <form method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $custom_list_table->display() ?>
    </form>
</div>

