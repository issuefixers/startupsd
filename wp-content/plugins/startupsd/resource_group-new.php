<?php

global $wpdb;
$header = '<h2>Add New Resource Group</h2>';

if (isset($_POST['resource_group_post'])) {
    $name = stripslashes($_POST['name']);
   
    if (!isset($notice) || empty($notice)) {       
        $result = $wpdb->insert( 
            'resource_groups', 
            array( 
                'name' => $name
            ), 
            array( 
                '%s'
            ) 
        );
        if (!$result) {
            $notice = 'The item was NOT ADDED successfully.';
        } else {
            $message = 'The item was ADDED successfully.';
            $_SESSION['message'] = $message;
            wp_redirect("admin.php?page=sd_resource_groups&action=edit&resource_group=".$wpdb->insert_id);
            die();
        }
    }
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/resource_group-form.php')) {
    include_once('views/resource_group-form.php');
}

?>
