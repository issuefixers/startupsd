<?php

global $wpdb;
$header = '<h2>Add New Startup</h2>';

if (isset($_POST['startup_post'])) {
    $industry_id = stripslashes($_POST['industry_id']);
    if (empty($industry_id)) {
        $notice = 'No industry assigned.';
    }
    $tier = stripslashes($_POST['tier']);
    if (!$notice && !is_numeric($tier)) {
        $notice = 'No tier assigned.';
    }
    $name = stripslashes($_POST['name']);
    $is_featured = stripslashes($_POST['is_featured']);
    $founder_first_name = stripslashes($_POST['founder_first_name']);
    $founder_last_name = stripslashes($_POST['founder_last_name']);
    $founded = stripslashes($_POST['founded']);
    $website_url = stripslashes($_POST['website_url']);
    $contact = stripslashes($_POST['contact']);
    $status = stripslashes($_POST['status']);

    $logo_name = $_FILES["logo_url"]["name"];
    $logo_type = $_FILES["logo_url"]["type"];
    $logo_error = $_FILES["logo_url"]["error"];
    $logo_size = $_FILES["logo_url"]["size"];
    $logo_tmp = $_FILES["logo_url"]["tmp_name"];

    if ($logo_error != 4) { // UPLOAD_ERR_NO_FILE: No file was uploaded.
        
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $logo_name);
        $extension = end($temp);

        if ((($logo_type == "image/gif") || ($logo_type == "image/jpeg") || ($logo_type == "image/jpg") 
            || ($logo_type == "image/pjpeg") || ($logo_type == "image/x-png") || ($logo_type == "image/png"))
            && ($logo_size < 10000000) && in_array($extension, $allowedExts)) {
            if ($logo_error > 0) {
                $notice = "Upload Error.";
            } else {
                if (file_exists(plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                    $notice = $logo_name." already exists.";
                } else {
                    if (move_uploaded_file($logo_tmp, plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                        $logo_url = "uploads/".$logo_name;
                    } else {
                        $notice = "Upload Error.";
                    }
                }
            }
        } else {
            $notice = "Invalid File.";
        }
    }
    
    $description = stripslashes($_POST['description']);
    
    if (!isset($notice) || empty($notice)) {       
        $result = $wpdb->insert( 
            'startups', 
            array( 
                'name' => $name,
                'is_featured' => $is_featured,
                'tier' => $tier,
                'founder_first_name' => $founder_first_name,
                'founder_last_name' => $founder_last_name,
                'founded' => $founded,
                'industry_id' => $industry_id,
                'website_url' => $website_url,
                'contact' => $contact,
                'logo_url' => $logo_url,
                'status' => $status,
                'description' => $description
            ), 
            array( 
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            ) 
        );
        if (!$result) {
            $notice = 'The item was NOT ADDED successfully.';
        } else {
            $message = 'The item was ADDED successfully.';
            $_SESSION['message'] = $message;
            wp_redirect("admin.php?page=sd_startups&action=edit&startup=".$wpdb->insert_id);
            die();
        }
    }
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/startup-form.php')) {
    include_once('views/startup-form.php');
}

?>
