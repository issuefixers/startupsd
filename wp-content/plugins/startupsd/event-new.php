<?php

global $wpdb;
$header = '<h2>Add New Event</h2>';

if (isset($_POST['event_post'])) {
    $event_group_ids = array();
    $event_group_id = null;
    if (!$_POST['event_group_id'] || empty($_POST['event_group_id'])) {
        $notice = 'No event group assigned.';
    } else {
        foreach ($_POST['event_group_id'] as $egid) {
            $event_group_id .= $egid.',';
            $event_group_ids[] = $egid;
        }
        $event_group_id = substr($event_group_id, 0, -1);
    }
    $title = stripslashes($_POST['title']);
    $is_featured = stripslashes($_POST['is_featured']);
    $link = stripslashes($_POST['link']);
    $start = stripslashes($_POST['start']);
    $end = stripslashes($_POST['end']);
    $location = stripslashes($_POST['location']);
    
    $logo_name = $_FILES["logo_url"]["name"];
    $logo_type = $_FILES["logo_url"]["type"];
    $logo_error = $_FILES["logo_url"]["error"];
    $logo_size = $_FILES["logo_url"]["size"];
    $logo_tmp = $_FILES["logo_url"]["tmp_name"];

    if ($logo_error != 4) { // UPLOAD_ERR_NO_FILE: No file was uploaded.
        
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $logo_name);
        $extension = end($temp);

        if ((($logo_type == "image/gif") || ($logo_type == "image/jpeg") || ($logo_type == "image/jpg") 
            || ($logo_type == "image/pjpeg") || ($logo_type == "image/x-png") || ($logo_type == "image/png"))
            && ($logo_size < 10000000) && in_array($extension, $allowedExts)) {
            if ($logo_error > 0) {
                $notice = "Upload Error.";
            } else {
                if (file_exists(plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                    $notice = $logo_name." already exists.";
                } else {
                    if (move_uploaded_file($logo_tmp, plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                        $logo_url = "uploads/".$logo_name;
                    } else {
                        $notice = "Upload Error.";
                    }
                }
            }
        } else {
            $notice = "Invalid File.";
        }
    }

    $description = stripslashes($_POST['description']);
    
    if (!isset($notice) || empty($notice)) {       
        $result = $wpdb->insert( 
            'events', 
            array(
                'event_group_id' => $event_group_id,
                'event_title' => htmlspecialchars($title, ENT_QUOTES),
                'is_featured' => $is_featured,
                'event_id' => $link,
                'event_start' => strtotime($start) ? date('Y-m-d H:i:s', strtotime($start)) : null,
                'event_end' => strtotime($end) ? date('Y-m-d H:i:s', strtotime($end)) : null,
                'event_address' => $location,
                'logo_url' => $logo_url,
                'content' => htmlspecialchars($description, ENT_QUOTES)
            ), 
            array( 
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            ) 
        );
        if (!$result) {
            $notice = 'The item was NOT ADDED successfully.';
        } else {
            $message = 'The item was ADDED successfully.';
            $_SESSION['message'] = $message;
            wp_redirect("admin.php?page=sd_events&action=edit&event=".$wpdb->insert_id);
            die();
        }
    }
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/event-form.php')) {
    include_once('views/event-form.php');
}

?>
