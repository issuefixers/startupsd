<?php

global $wpdb;
$mentors = $wpdb->get_results("SELECT * FROM mentors");
$return['mentors'] = array();
if ($mentors && count($mentors)) {
    foreach ($mentors as $mentor) {
        $id = $mentor->id;
        $first_name = $mentor->first_name ? $mentor->first_name : null;
        $last_name = $mentor->last_name ? $mentor->last_name : null;
        $company = $mentor->company ? $mentor->company : null;
        $position = $mentor->position ? $mentor->position : null;
        $email = $mentor->email ? $mentor->email : null;
        $linkedin = $mentor->linkedin ? $mentor->linkedin : null;
        $facebook = $mentor->facebook ? $mentor->facebook : null;
        $twitter = $mentor->twitter ? $mentor->twitter : null;
        $return['mentors'][] = array(
            'id' => $id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'company' => $company,
            'position' => $position,
            'email' => $email,
            'linkedin' => $linkedin,
            'facebook' => $facebook,
            'twitter' => $twitter
        );
    }
}
$singular = 'mentor';
$plural = 'mentors';
$column_title = 'first_name';
$columns = array(
    'first_name',
    'last_name',
    'company',
    'position',
    'email',
    'linkedin',
    'facebook',
    'twitter'
);
$sortable_columns = array(
    'first_name',
    'last_name'
);
//Create an instance of our package class...
$custom_list_table = new Custom_List_Table($return['mentors'], $singular, $plural, $columns, $column_title, $sortable_columns);
//Fetch, prepare, sort, and filter our data...
$custom_list_table->prepare_items();

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<div class="wrap">
    <h2>Mentors <a href="?page=sd_mentors&action=new" class="add-new-h2">Add New</a></h2>
    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <?php if ($notice) : ?>
        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif; ?>
    <?php if ($message) : ?>
        <div id="message" class="updated"><p><?php echo $message; ?></p></div>
    <?php endif; ?>
    <form method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $custom_list_table->display() ?>
    </form>
</div>

