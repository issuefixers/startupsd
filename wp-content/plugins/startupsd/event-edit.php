<?php

global $wpdb;
$header = '<h2>Edit Event <a href="?page=sd_events&action=new" class="add-new-h2">Add New</a></h2>';
$event_id =  (isset($_GET['event']) && is_numeric($_GET['event'])) ? $_GET['event'] : null;

if ($event_id) {
    $event = $wpdb->get_row('SELECT * FROM events WHERE id = '.$event_id);
    if ($event) {
        if (isset($_POST['event_post']) && isset($_POST['event_id']) && ($event_id == $_POST['event_id'])) {
            $event_group_ids = array();
            $event_group_id = null;
            if (!$_POST['event_group_id'] || empty($_POST['event_group_id'])) {
                $notice = 'No event group assigned.';
            } else {
                foreach ($_POST['event_group_id'] as $egid) {
                    $event_group_id .= $egid.',';
                    $event_group_ids[] = $egid;
                }
                $event_group_id = substr($event_group_id, 0, -1);
            }
            $title = stripslashes($_POST['title']);
            $is_featured = stripslashes($_POST['is_featured']);
            $link = stripslashes($_POST['link']);
            $start = stripslashes($_POST['start']);
            $end = stripslashes($_POST['end']);
            $location = stripslashes($_POST['location']);
            if ($event->logo_url) {
                $logo_url = $event->logo_url;
            }
            
            $logo_name = $_FILES["logo_url"]["name"];
            $logo_type = $_FILES["logo_url"]["type"];
            $logo_error = $_FILES["logo_url"]["error"];
            $logo_size = $_FILES["logo_url"]["size"];
            $logo_tmp = $_FILES["logo_url"]["tmp_name"];

            if ($logo_error != 4) { // UPLOAD_ERR_NO_FILE: No file was uploaded.
        
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $temp = explode(".", $logo_name);
                $extension = end($temp);

                if ((($logo_type == "image/gif") || ($logo_type == "image/jpeg") || ($logo_type == "image/jpg") 
                    || ($logo_type == "image/pjpeg") || ($logo_type == "image/x-png") || ($logo_type == "image/png"))
                    && ($logo_size < 10000000) && in_array($extension, $allowedExts)) {
                    if ($logo_error > 0) {
                        $notice = "Upload Error.";
                    } else {
                        if (file_exists(plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                            $notice = $logo_name." already exists.";
                        } else {
                            if (move_uploaded_file($logo_tmp, plugin_dir_path(__FILE__)."uploads/".$logo_name)) {
                                $logo_url = "uploads/".$logo_name;
                            } else {
                                $notice = "Upload Error.";
                            }
                        }
                    }
                } else {
                    $notice = "Invalid File.";
                }
            }
            $description = stripslashes($_POST['description']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'events', 
                    array(
                        'event_group_id' => $event_group_id,
                        'is_featured' => $is_featured,
                        'event_title' => htmlspecialchars($title, ENT_QUOTES),
                        'event_id' => $link,
                        'event_start' => strtotime($start) ? date('Y-m-d H:i:s', strtotime($start)) : null,
                        'event_end' => strtotime($end) ? date('Y-m-d H:i:s', strtotime($end)) : null,
                        'event_address' => $location,
                        'logo_url' => $logo_url,
                        'content' => htmlspecialchars($description, ENT_QUOTES)
                    ), 
                    array( 'id' => $event_id ), 
                    array( 
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $event_group_ids = array();
            if ($event->event_group_id) {
                $event_group_ids = explode(',', $event->event_group_id);
            }
            $link = $event->event_id ? $event->event_id : null;
            $is_featured = $event->is_featured ? $event->is_featured : null;
            $title = $event->event_title ? htmlspecialchars_decode($event->event_title, ENT_QUOTES) : null;
            if ($event->event_start != '0000-00-00 00:00:00') {
                if (date('H:i:s', strtotime($event->event_start)) == '00:00:00') {
                    $start = date('Y-m-d', strtotime($event->event_start));
                } else {
                    $start = date('Y-m-d g:ia', strtotime($event->event_start));
                }
            } else {
                $start = null;
            }
            if ($event->event_end != '0000-00-00 00:00:00') {
                if (date('H:i:s', strtotime($event->event_end)) == '00:00:00') {
                    $end = date('Y-m-d', strtotime($event->event_end));
                } else {
                    $end = date('Y-m-d g:ia', strtotime($event->event_end));
                }
            } else {
                $end = null;
            }
            $location = $event->event_address ? $event->event_address : null;
            $logo_url = $event->logo_url ? $event->logo_url : null;
            $description = $event->content ? htmlspecialchars_decode($event->content, ENT_QUOTES) : null;
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/event-form.php')) {
    include_once('views/event-form.php');
}

?>
