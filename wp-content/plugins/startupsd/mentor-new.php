<?php

global $wpdb;
$header = '<h2>Add New Mentor</h2>';

if (isset($_POST['mentor_post'])) {
    $first_name = stripslashes($_POST['first_name']);
    $last_name = stripslashes($_POST['last_name']);
    $company = stripslashes($_POST['company']);
    $position = stripslashes($_POST['position']);
    $email = stripslashes($_POST['email']);
    $linkedin = stripslashes($_POST['linkedin']);
    $facebook = stripslashes($_POST['facebook']);
    $twitter = stripslashes($_POST['twitter']);
    
    if (!isset($notice) || empty($notice)) {       
        $result = $wpdb->insert( 
            'mentors', 
            array( 
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $company,
                'position' => $position,
                'email' => $email,
                'linkedin' => $linkedin,
                'facebook' => $facebook,
                'twitter' => $twitter
            ), 
            array( 
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            ) 
        );
        if (!$result) {
            $notice = 'The item was NOT ADDED successfully.';
        } else {
            $message = 'The item was ADDED successfully.';
            $_SESSION['message'] = $message;
            wp_redirect("admin.php?page=sd_mentors&action=edit&mentor=".$wpdb->insert_id);
            die();
        }
    }
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/mentor-form.php')) {
    include_once('views/mentor-form.php');
}

?>
