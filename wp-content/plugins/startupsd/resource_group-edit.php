<?php

global $wpdb;
$header = '<h2>Edit Resource Group <a href="?page=sd_resource_groups&action=new" class="add-new-h2">Add New</a></h2>';
$resource_group_id =  (isset($_GET['resource_group']) && is_numeric($_GET['resource_group'])) ? $_GET['resource_group'] : null;

if ($resource_group_id) {
    $resource_group = $wpdb->get_row('SELECT * FROM resource_groups WHERE id = '.$resource_group_id);
    if ($resource_group) {
        if (isset($_POST['resource_group_post']) && isset($_POST['resource_group_id']) && ($resource_group_id == $_POST['resource_group_id'])) {
            $name = stripslashes($_POST['name']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'resource_groups', 
                    array( 
                        'name' => $name
                    ), 
                    array( 'id' => $resource_group_id ), 
                    array( 
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $name = $resource_group->name ? $resource_group->name : null;
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/resource_group-form.php')) {
    include_once('views/resource_group-form.php');
}

?>