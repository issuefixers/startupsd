<?php

global $wpdb;
$header = '<h2>Edit Mentor <a href="?page=sd_mentors&action=new" class="add-new-h2">Add New</a></h2>';
$mentor_id =  (isset($_GET['mentor']) && is_numeric($_GET['mentor'])) ? $_GET['mentor'] : null;

if ($mentor_id) {
    $mentor = $wpdb->get_row('SELECT * FROM mentors WHERE id = '.$mentor_id);
    if ($mentor) {
        if (isset($_POST['mentor_post']) && isset($_POST['mentor_id']) && ($mentor_id == $_POST['mentor_id'])) {
            $first_name = stripslashes($_POST['first_name']);
            $last_name = stripslashes($_POST['last_name']);
            $company = stripslashes($_POST['company']);
            $position = stripslashes($_POST['position']);
            $email = stripslashes($_POST['email']);
            $linkedin = stripslashes($_POST['linkedin']);
            $facebook = stripslashes($_POST['facebook']);
            $twitter = stripslashes($_POST['twitter']);

            if (!isset($notice) || empty($notice)) {
                $result = $wpdb->update( 
                    'mentors', 
                    array( 
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'company' => $company,
                        'position' => $position,
                        'email' => $email,
                        'linkedin' => $linkedin,
                        'facebook' => $facebook,
                        'twitter' => $twitter
                    ), 
                    array( 'id' => $mentor_id ), 
                    array( 
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    ), 
                    array( '%d' ) 
                );
                if (!$result) {
                    $notice = 'The item was NOT UPDATED successfully.';
                } else {
                    $message = 'The item was UPDATED successfully.';
                }
            }
        } else {
            $first_name = $mentor->first_name ? $mentor->first_name : null;
            $last_name = $mentor->last_name ? $mentor->last_name : null;
            $company = $mentor->company ? $mentor->company : null;
            $position = $mentor->position ? $mentor->position : null;
            $email = $mentor->email ? $mentor->email : null;
            $linkedin = $mentor->linkedin ? $mentor->linkedin : null;
            $facebook = $mentor->facebook ? $mentor->facebook : null;
            $twitter = $mentor->twitter ? $mentor->twitter : null;
        }
    } else {
        include_once('views/404.php');
        die();
    }
} else {
    include_once('views/404.php');
    die();
}

if (isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
    $notice = $_SESSION['notice'];
    unset($_SESSION['notice']);
}

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

if (file_exists(plugin_dir_path( __FILE__ ).'views/mentor-form.php')) {
    include_once('views/mentor-form.php');
}

?>
