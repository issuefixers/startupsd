(function ($) {
	$(function () {
		$('input.date-picker').datetimepicker({
			ampm: true,
			timeFormat: 'h:mmtt',
			dateFormat: 'yy-mm-dd'
		});
	});
}(jQuery));