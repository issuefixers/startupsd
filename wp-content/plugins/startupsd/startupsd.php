<?php
/**
 * @wordpress-plugin
 * Plugin Name: StartUp San Diego
 * Description: 
 * Version:     1.0.0
 * Author:      Mindgruve
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /lang
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define(TABLE_INDUSTRIES, 'industries');
define(TABLE_STARTUPS, 'startups');
define(TABLE_EVENT_GROUPS, 'event_groups');
define(TABLE_EVENTS, 'events');
define(TABLE_RESOURCE_GROUPS, 'resource_groups');
define(TABLE_RESOURCES, 'resources');
define(TABLE_MENTORS, 'mentors');

require_once( plugin_dir_path( __FILE__ ) . 'class-startupsd.php' );

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'StartUp_San_Diego', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'StartUp_San_Diego', 'deactivate' ) );

StartUp_San_Diego::get_instance();